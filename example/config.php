<?php

/**
 * Logs
 */

# Diretório para salvar logs
\DS\Utils\Config::setValue('assests_dir', __DIR__.'/temps');
# Habilitar ou desabilitar logs
\DS\Utils\Config::setValue('disable_logs', true);

/**
 * Configurações de Email
 */
# Email From
\DS\Utils\Config::setValue("email_send", "noreply@devesharp.com.br");
# Name Email from
\DS\Utils\Config::setValue("email_send_name", "Devesharp");
# Servidor de email
\DS\Utils\Config::setValue("email_host", "smtp.umbler.com");
# Porta do Servidor
\DS\Utils\Config::setValue("email_port", 587);
# Username
\DS\Utils\Config::setValue("email_user", "noreply@devesharp.com.br");
# Senha
\DS\Utils\Config::setValue("email_pass", "I*a9.y2rY*");


/**
 * Configurações do Banco de dados
 */
# Driver
\DS\Utils\Config::setValue('RDBMS_driver', 'mysql');
# Servidor
\DS\Utils\Config::setValue('RDBMS_host', 'host');
# Database
\DS\Utils\Config::setValue('RDBMS_dbname', 'db_test');
# User
\DS\Utils\Config::setValue('RDBMS_user', 'root');
# Senha
\DS\Utils\Config::setValue('RDBMS_pass', '123456aa');
# Porta
\DS\Utils\Config::setValue('RDBMS_port', '3306');


/**
 * Configurações do MongoDB
 */
# Servidor
\DS\Utils\Config::setValue('MONGODB_host', 'host');
# Database
\DS\Utils\Config::setValue('MONGODB_dbname', 'db_test');
# User
\DS\Utils\Config::setValue('MONGODB_user', 'root');
# Senha
\DS\Utils\Config::setValue('MONGODB_pass', '123456aa');
# Porta
\DS\Utils\Config::setValue('MONGODB_port', '3306');