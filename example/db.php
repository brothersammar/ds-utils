<?php

/**
 * Database
 */

// Config
\DS\Utils\Config::setValue('RDBMS_driver', 'mysql');
\DS\Utils\Config::setValue('RDBMS_host', 'host');
\DS\Utils\Config::setValue('RDBMS_dbname', 'db_test');
\DS\Utils\Config::setValue('RDBMS_user', 'root');
\DS\Utils\Config::setValue('RDBMS_pass', '123456aa');
\DS\Utils\Config::setValue('RDBMS_port', '3306');

$db = new DS\Database\TableCRUD('login');


// SELECT * FROM `login`  WHERE (login = :login) AND ((id = :id) OR (((name = :name) AND (last = :last)) OR (middle = :middle)))
$db->where('login', ':login')
	->beginBlock()
		->where('id', ':id')
		->whereOR()
		->beginBlock()
			->beginBlock()
				->where('name', ':name')
				->where('last', ':last')
			->endBlock()
			->whereOR()
			->where('middle', ':middle')
		->endBlock()
	->endBlock()
	->bindValue(':login', 'login')
	->select();

echo $db->getString();
