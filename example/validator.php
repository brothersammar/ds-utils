<?php

$data = file_get_contents('tests/YAMLSchema/testValidate.yaml');

$var = [
	'allowed' => '41056294842'
];

try {
	DS\YAMLSchema\Validator::validateD($data, $var);
} catch (DS\YAMLSchema\Exception $e) {
	$data = $e->getBody();

	//Tratamento de erros
	foreach ($e->getBody() as $value) {
		switch ($value['code']) {
			case DS\YAMLSchema\ErrorCode::ERROR_REQUIRED:
				echo 'O campo '.$data.' é obrigatório';
				break;

			case DS\YAMLSchema\ErrorCode::ERROR_DATA:
				echo 'Erro no tratamento dos dados';
				break;

			case DS\YAMLSchema\ErrorCode::TYPE_ERROR:
				echo $value['name'].' precisa ser '.$value['body']['expect'].' mas é '.$value['body']['current'];
				break;

			case DS\YAMLSchema\ErrorCode::RANGE_MAX:
				echo $value['name'].' precisa ser menor que '.$value['body']['expect'].' mas é '.$value['body']['current'];
				break;

			case DS\YAMLSchema\ErrorCode::RANGE_MIN:
				echo $value['name'].' precisa ser maior que '.$value['body']['expect'].' mas é '.$value['body']['current'];
				break;

			case DS\YAMLSchema\ErrorCode::INVALID_CPF:
				echo $value['name'].' não é um CPF válido';
				break;

			case DS\YAMLSchema\ErrorCode::INVALID_CEP:
				echo $value['name'].' não é um CEP válido';
				break;

			case DS\YAMLSchema\ErrorCode::INVALID_EMAIL:
				echo $value['name'].' não é um email válido';
				break;

			case DS\YAMLSchema\ErrorCode::NOT_ALLOWED_DATA:
				echo $value['name'].' não é nenhum dos valores válidos: '.$value['body']['expect'];
				break;
		}
	}
}
