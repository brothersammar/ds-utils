<?php

namespace DS\APIGenerator;

/**
 * APIGeneratorTest
 */

class APIGeneratorTest
{
	public function generator($filename, $out, $data = [])
	{
		$routes = yaml_parse(file_get_contents($filename));

		$all_tests = '';
		foreach ($routes['routes'] as $routename => $route) {
			$all_requests = '';
			$method = '';
            	foreach ($route as $request_name => $request) {

            		if($request_name == 'comment') continue;

            		//Body
              		$body_content = $this->getBody($request);

              		//Header
              		$header_content = $this->getHeader($request);

              		//Params
              		$params_content = $this->getParams($request);

              		//Global
              		$global_content = $this->getGlobal($request);

              		//Testes
              		$tests_content = $this->getTest($request);

              		//Method
              		$method = $request['method'];

          		$template = $this->getTemplateTest($request['method']);
          		$template = str_replace('{{PATH}}', $request['route']."?".$params_content, $template);
          		$template = str_replace('{{RESQUEST_NAME}}', $request_name, $template);
          		$template = str_replace('{{HEADERS}}', $header_content, $template);
          		$template = str_replace('{{BODY}}', $body_content, $template);
          		$template = str_replace('{{TESTS}}', $tests_content, $template);
          		$template = str_replace('{{HEADERS}}', $header_content, $template);
          		$template = str_replace('{{GLOBAL}}', $global_content, $template);
          		$all_requests .= $template."\n\n\n";
			}

			$string_tests = str_replace('{{BODY}}', $all_requests, $this->getTemplateFunction());
			$string_tests = str_replace('{{ROUTE}}', $routename, $string_tests);
			$all_tests .= str_replace('{{NAME}}', $this->getFileName($routename), $string_tests);
        	}

        	//Class File
		$file = str_replace('{{TESTS}}', $all_tests, $this->getTemplateFile($out));
		$file = str_replace('{{HOST}}', $routes['host'], $file);
		$file = str_replace('{{setUpBeforeClass}}', $data['setUpBeforeClass'], $file);
        	file_put_contents($out, $file);
	}

	/**
	 * Resgata Body
	 */
	public function getBody($request)
	{
		$body_content = "[\n";
		if(is_array($request['body'])){
	         	foreach($request['body'] as $body_key => $body){
	         		$body['value'] = $this->setGlobal($body['value']);
	          	$body_content .= "\t\t\t\t'{$body_key}' => \"{$body['value']}\", \n";
	         	}
	    	}

		$body_content .= "\t\t\t],";

         return $body_content;
	}

	/**
	 * Resgata Header
	 */
	public function getHeader($request)
	{
		$header_content = "[\n";
		if(isset($request['headers'])){
	         	foreach($request['headers'] as $body_key => $body){
	         		$output_array = [];
	         		$body['value'] = $this->setGlobal($body['value']);
	          	$header_content .= "\t\t\t\t'{$body_key}' => \"{$body['value']}\", \n";
	         	}
		}
         	$header_content .= "\t\t\t],";

         return $header_content;
	}

	/**
	 * Resgata variaveis globais
	 */
	public function setGlobal($value)
	{
		preg_match_all('/{\$([a-zA-z0-9]+)}/i', $value, $output_array);
		foreach ($output_array[0] as $key => $hash) {
			$value = str_replace($hash, '".self::$GLOBAL[\''.$output_array[1][$key].'\']."', $value);
		}

		return $value;
	}

	/**
	 * Resgata Query
	 */
	public function getParams($request)
	{
		$params = [];
		if(isset($request['params'])){
	         	foreach($request['params'] as $body_key => $body){
	         		$body['value'] = $this->setGlobal($body['value']);
	          	$params[] = "{$body_key}={$body['value']}";
	         	}
		}
         	return implode('&', $params);
	}

	/**
	 * Resgata Globals
	 */
	public function getGlobal($request)
	{
		$global = "";
		if(isset($request['global'])){
			$global .= "// Saves\n";
	         	foreach($request['global'] as $body_key => $value){
	          	$global .= "\t\tself::\$GLOBAL['token'] = \$data->".str_replace('.', '->', $value).";";
	         	}
		}
         	return $global;
	}

	/**
	 * Resgata Testes
	 */
	public function getTest($request)
	{
		$tests_content = "";
		if(isset($request['tests'])){
			$tests_content .= "// Testes\n";
	         	foreach($request['tests'] as $test_key => $tests){
	         		foreach ($tests as $key => $test) {
	         			switch ($key) {
	         				case 'equal':
	         					if(is_string($test) && !is_numeric($test)){
	         						$test = $this->setGlobal($test);
	         						$test = "\"".$test."\"";
	         					}

	         					$tests_content .= "\t\t\$this->assertEquals($test, \$data->".str_replace('.', '->', $test_key).");\n";

	         					break;
	         				case 'type':
	         					$tests_content .= "\t\t\$this->assertInternalType('$test', \$data->".str_replace('.', '->', $test_key).");\n";
	         					break;
	         			}
	         		}
	         	}
		}
         	return $tests_content;
	}

	/**
	 * Template do Test
	 */
	public function getTemplateTest($type)
	{
		$template = '';
		$template .= "\t\t/*\n";
		$template .= "\t\t * Resquest: {{RESQUEST_NAME}}\n";
		$template .= "\t\t */\n";
		$template .= "\t\t\$res = self::\$http->request('".$type."', \"{{PATH}}\", [\n";
		//Body
		$template .= "\t\t\t'form_params' => \n";
		$template .= "\t\t\t{{BODY}}\n";
		//Header
		$template .= "\t\t\t'headers' => \n";
		$template .= "\t\t\t{{HEADERS}}\n";
		//
		$template .= "\t\t]);\n\n";
		//
		$template .= "\t\t\$data = json_decode(\$res->getBody());\n\n";
		//
		$template .= "\t\t{{TESTS}}\n";
		//
		$template .= "\t\t{{GLOBAL}}";


         	return $template;
	}

	/**
	 * Template functions
	 */
	public function getTemplateFunction()
	{
		$template = '';

		$template .= "\n";
		$template .= "\t/**\n";
		$template .= "\t * @testdox {{ROUTE}}\n";
		$template .= "\t **/\n";
		$template .= "\tpublic function test{{NAME}}()\n";
		$template .= "\t{\n";
		$template .= "{{BODY}}";
		$template .= "\t}\n";

         	return $template;
	}

	/**
	 * Template File
	 */
	public function getTemplateFile($file = '')
	{
		if(file_exists($file)){
			$template_test = '';
			$template_test .= "/** BEGIN TEST **/\n";
			$template_test .= "{{TESTS}}";
			$template_test .= "\t/** END TEST **/\n";

			$template = file_get_contents($file);
			$template = preg_replace("/\/\*\* BEGIN TEST \*\*\/(.*)\/\*\* END TEST \*\*\//s", $template_test, $template);

			return $template;
		}else{

			$template = '';
			$template .= "<?php\n";
			$template .= "\n";
			$template .= "use PHPUnit\Framework\TestCase;\n";
			$template .= "\n";
			$template .= "class RoutesTest extends TestCase\n";
			$template .= "{\n";
			$template .= "\n";
			$template .= "\tstatic \$http = false;\n";
			$template .= "\tstatic \$account = false;\n";
			$template .= "\tstatic \$faker = false;\n";
			$template .= "\tstatic \$GLOBAL = false;\n\n";
			$template .= "\t/**\n";
			$template .= "\t * Setup\n";
			$template .= "\t */\n";
			$template .= "\tstatic public function setUpBeforeClass()\n";
			$template .= "\t{\n";
			$template .= "\t\tself::\$faker = Faker\Factory::create();\n";
			$template .= "\t\tself::\$faker->addProvider(new Faker\Provider\pt_BR\Person(self::\$faker));\n";
			$template .= "\t\t\n";
			$template .= "\t\tself::\$http = new \GuzzleHttp\Client([\n";
			$template .= "\t\t\t'base_uri' => '{{HOST}}',\n";
			$template .= "\t\t\t'http_errors' => false\n";
			$template .= "\t\t]);\n";
			$template .= "\t\t{{setUpBeforeClass}}\n";
			$template .= "\t}\n";
			$template .= "\t\t\n";
			$template .= "\n";
			$template .= "\t/** BEGIN TEST **/\n";
			$template .= "{{TESTS}}";
			$template .= "\t/** END TEST **/\n";
			$template .= "}\n";
		}

         	return $template;
	}

	/**
	 * Template File
	 */
	public function getFileName($name)
	{
		$arr = preg_split("/[^A-Za-z0-9]/", $name);
		foreach ($arr as $key => &$value) {
			$value = ucfirst($value);
		}

		return implode('', $arr);
	}

}