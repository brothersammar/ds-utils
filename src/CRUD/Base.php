<?php

namespace DS\CRUD;

/**
 * CRUD Base
 * Class para gerenciamento de CRUD Simples
 */

class Base
{
    /**
     *
     * Valores que podem ser ordernados
     *
     * @var array
     *
     */
    public $sort = [];
    public $sort_default = '';

    /**
     *
     * Valores que podem ser filtrados
     *
     * @var array
     *
     */
    public $filters = [
        // 'id' => [
        // 	'filter' => 'array_string',
        // 	'column' => 'name'
        // ]
    ];

    /**
     *
     * Status padrões
     *
     * @var array
     *
     */
    public $state = [];

    /**
     *
     * Campos que devem ser retornados
     *
     * @var array
     *
     */
    public $fields = [];

    /**
     *
     * Campos que devem ser retornados
     *
     * @var array
     *
     */
    public $query = [
        'offset' => 0,
        'limit' => 20
    ];

    /**
     * @var array valor atual
     */
    public $filters_current = [];

    public function __search(\Illuminate\Database\Eloquent\Model $resource, $filters = [], $query = [], $alias = [])
    {
        $query = $query ?? [];
        $filters = $filters ?? [];
        $alias = $alias ?? [];

        //Misc Query
        $query = array_merge($this->query, $query);

        $this->__boot($resource);
        $this->__filters($resource, $filters, $this->filters);
        $this->__sorts($resource, $query);
        $this->__query($resource, $query);

        return $resource;

        // var_dump($resource->toSql());
        // var_dump($resource->toArray());
    }

    public function __getSearch($resource, $alias = [])
    {
        $this->__alias($resource, $alias);
        return $resource;
    }

    public function __alias(&$resource, $alias)
    {

        $alias_allowed = [];

        foreach ($alias as $value) {
            if (in_array($value, $this->fields)) {
                $alias_allowed[] = $value;
            }
        }

        if (empty($alias_allowed)) {
            $resource = $resource->get();
        } else {
            $resource = $resource->get($alias_allowed);
        }
    }

    public function __query(&$resource, $query)
    {
        //Offset and Limit
        $resource = $resource
            ->offset($query['offset'] ?? 0)
            ->limit($query['limit'] ?? 20);
    }

    /**
     * Resgata quantos recursos foram encontrados
     * @param  [type] $resource
     * @return
     */
    public function __count($resource)
    {
        $resource->offset(0);
        return $resource->count();
    }

    /**
     * Ordem
     * @param  &$resource [description]
     * @param  [type]                              $query          [description]
     */
    public function __sorts(&$resource, $query)
    {
        $query['sort'] = $query['sort'] ?? $this->sort_default;
        //sort
        if (isset($query['sort'])) {
            foreach (explode(",", $query['sort']) as $key => $value) {
                //DESC
                if ($value[0] == '-') {
                    if (in_array(substr($value, 1), $this->sort)) {
                        $resource = $resource->orderBy(substr($value, 1), 'desc');
                    } else {
                        foreach ($this->sort as $sort_key => $sort_value) {
                            if (is_array($sort_value) && $sort_key == substr($value, 1)) {
                                $resource = $resource->orderBy($sort_value['column'], 'desc');
                            }
                        }
                    }
                } else {
                    if (in_array($value, $this->sort)) {
                        $resource = $resource->orderBy($value, 'asc');
                    } else {
                        foreach ($this->sort as $sort_key => $sort_value) {
                            if (is_array($sort_value) && $sort_key == $value) {
                                $resource = $resource->orderBy($sort_value['column'], 'asc');
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Filtra parametros de acordo com os filtros disponiveis
     *
     * @param  array $filter Valores que precisam ser filtrados
     * @param  array $filters_allowed Valores permitidos para busca
     * @return null
     */
    public function __filters(&$resource, $filter, $filters_allowed)
    {

        foreach ($filter as $filter_key => $filter_value) {
            foreach ($filters_allowed as $fallow_key => $fallow_value) {
                //Coluna válida
                if ($fallow_key == $filter_key) {
                    //Resgata coluna que deve ser tratada
                    $column = $fallow_value['column'] ?? $fallow_key;

                    switch ($fallow_value['filter']) {
                        //Int
                        case 'int':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->where($column, $filter_value);
                            break;

                        //Maior que
                        case 'int_gt':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->where($column, '>', $filter_value);
                            break;

                        //Menor que
                        case 'int_lt':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->where($column, '<', $filter_value);
                            break;

                        //Maior que ou igual
                        case 'int_gte':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->where($column, '>=', $filter_value);
                            break;

                        //Menor que ou igual
                        case 'int_lte':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->where($column, '<=', $filter_value);
                            break;

                        //String
                        case 'string':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->where($column, $filter_value);
                            break;

                        //String Like
                        case 'string_like':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->where($column, 'LIKE', $filter_value);
                            break;

                        //String Like
                        case 'not_string_like':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->where($column, 'NOT LIKE', $filter_value);
                            break;

                        //String Like com porcentagem depois
                        case 'string_like_percent_after':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->where($column, 'LIKE', $filter_value . '%');
                            break;

                        //String Like com porcentagem antes
                        case 'string_like_percent_before':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->where($column, 'LIKE', '%' . $filter_value);
                            break;

                        //String Like antes e depois
                        case 'string_like_percent':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->where($column, 'LIKE', '%' . $filter_value . '%');
                            break;

                        //Array de Inteiros
                        case 'array_int':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->whereIn($column, $filter_value);
                            break;

                        //Not Array de Inteiros
                        case 'array_int_not':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->whereNotIn($column, $filter_value);
                            break;

                        //Array de Strings
                        case 'array_string':
//                            $this->filters_current[$key] = $filter_value;
                            if (!empty($filter_value)) {
                                $resource = $resource->whereIn($column, $filter_value);
                            }

                            break;

                        //Array de Strings
                        case 'int_in_column':
                            $filter_value = intval($filter_value);
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->whereRaw('FIND_IN_SET(' . $filter_value . ', ' . $column . ') = 0');
                            break;

                    }
                }
            }
        }
    }

    /**
     * Boot antes da busca
     * @param  [type] $resource [description]
     * @return [type]           [description]
     */
    public function __boot($resource)
    {
    }

    /**
     * tratamento de parametros
     * @param  array $data [description]
     * @return [type]       [description]
     */
    public static function getParams(array $data)
    {
        if (isset($_GET['limit'])) {
            $data['query']['limit'] = $_GET['limit'];
        }

        if (isset($_GET['offset'])) {
            $data['query']['offset'] = $_GET['offset'];
        }

        if (isset($_GET['alias'])) {
            $data['alias'] = $_GET['alias'];
        }

        if (isset($_GET['sort'])) {
            $data['query']['sort'] = $_GET['sort'];
        }

        return $data;
    }
}
