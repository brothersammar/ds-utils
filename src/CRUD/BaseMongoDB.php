<?php

namespace DS\CRUD;

/**
 * CRUD Base para MongoDB
 * Class para gerenciamento de CRUD Simples
 */

class BaseMongoDB
{
    /**
     *
     * Valores que podem ser ordernados
     *
     * @var array
     *
     */
    public $sort = [];
    public $sort_default = '';

    /**
     *
     * Valores que podem ser filtrados
     *
     * @var array
     *
     */
    public $filters = [
        'name' => [
            'filter' => 'string',
            'column' => 'name'
        ]
    ];

    /**
     *
     * Status padrões
     *
     * @var array
     *
     */
    public $state = [];

    /**
     *
     * Campos que devem ser retornados
     *
     * @var array
     *
     */
    public $fields = [];

    /**
     *
     * Campos que devem ser retornados
     *
     * @var array
     *
     */
    public $query = [
        'offset' => 0,
        'limit' => 20
    ];

    public function __search(\DS\MongoORM\Model $resource, $filters, $query, $alias)
    {
        $query = $query ?? [];

        //Misc Query
        $query = array_merge($this->query, $query);

        $this->__filters($resource, $filters ?? [], $this->filters);
        $this->__sorts($resource, $query ?? []);
        $this->__query($resource, $query ?? []);

        return $resource;

        // var_dump($resource->toSql());
        // var_dump($resource->toArray());
    }

    public function __getSearch($resource, $alias = []){
        $this->__alias($resource, $alias);
        return $resource;
    }

    public function __alias(&$resource, $alias){

        $alias_allowed = [];

        foreach ($alias as $value) {
            if(in_array($value, $this->fields)){
                $alias_allowed[] = $value;
            }
        }

        if(empty($alias_allowed)){
            $resource = $resource->get();
        }else{
            $resource = $resource->get($alias_allowed);
        }
    }

    public function __query(&$resource, $query){
        //Offset and Limit
        $resource = $resource
            ->offset($query['offset'] ?? 0)
            ->limit($query['limit'] ?? 20);
    }

    /**
     * Ordem
     * @param  &$resource       [description]
     * @param  [type]                              $query          [description]
     */
    public function __sorts(&$resource, $query){
        $query['sort'] = $query['sort'] ?? $this->sort_default;
        //sort
        if(isset($query['sort'])){
            foreach (explode(",",$query['sort']) as $key => $value) {
                //DESC
                if(strlen($value) >= 1 && $value[0] == '-'){
                    if(in_array(substr($value,1), $this->sort)){
                        $resource = $resource->orderBy(substr($value,1), 'desc');
                    }
                }else{
                    if(in_array($value, $this->sort)){
                        $resource = $resource->orderBy($value, 'asc');
                    }
                }
            }
        }
    }
    /**
     * Filtra parametros de acordo com os filtros disponiveis
     *
     * @param  array $filter          	Valores que precisam ser filtrados
     * @param  array $filters_allowed 	Valores permitidos para busca
     * @return null
     */
    public function __filters(&$resource, $filter, $filters_allowed){

        foreach ($filter as $filter_key => $filter_value) {
            foreach ($filters_allowed as $fallow_key => $fallow_value) {
                //Coluna válida
                if($fallow_key == $filter_key){
                    //Resgata coluna que deve ser tratada
                    $column = $fallow_value['column'] ?? $fallow_key;
                    $value = 5;

                    switch ($fallow_value['filter']) {
                        //Int
                        case 'int':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->eq($column, intval($filter_value));
                            break;

                        //Maior que
                        case 'int_gt':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->gt($column, intval($filter_value));
                            break;

                        //Menor que
                        case 'int_lt':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->lt($column, intval($filter_value));
                            break;

                        //Maior que ou igual
                        case 'int_gte':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->gte($column, intval($filter_value));
                            break;

                        //Menor que ou igual
                        case 'int_lte':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->lte($column, intval($filter_value));
                            break;

                        //Date
                        case 'date':
//                            $this->filters_current[$key] = $filter_value;
                            // Deve Manter o Horário sem fuso para poder utilizar no mongoDB
                            $time = new \DateTime($filter_value);
                            $time->setTimezone(new \DateTimeZone('UTC'));
                            $time = new \MongoDB\BSON\UTCDateTime($time->format('Uv'));
                            $resource = $resource->eq($column, $time);
						break;

                        //Maior que
                        case 'date_gt':
//                            $this->filters_current[$key] = $filter_value;
                            // Deve Manter o Horário sem fuso para poder utilizar no mongoDB
                            $time = new \DateTime($filter_value);
                            $time->setTimezone(new \DateTimeZone('UTC'));
                            $time = new \MongoDB\BSON\UTCDateTime($time->format('Uv'));
                            $resource = $resource->gt($column, $time);
						break;

                        //Menor que
                        case 'date_lt':
//                            $this->filters_current[$key] = $filter_value;
                            // Deve Manter o Horário sem fuso para poder utilizar no mongoDB
                            $time = new \DateTime($filter_value);
                            $time->setTimezone(new \DateTimeZone('UTC'));
                            $time = new \MongoDB\BSON\UTCDateTime($time->format('Uv'));
                            $resource = $resource->lt($column, $time);
						break;

                        //Maior que ou igual
                        case 'date_gte':
//                            $this->filters_current[$key] = $filter_value;
                            // Deve Manter o Horário sem fuso para poder utilizar no mongoDB
                            $time = new \DateTime($filter_value);
                            $time->setTimezone(new \DateTimeZone('UTC'));
                            $time = new \MongoDB\BSON\UTCDateTime($time->format('Uv'));
                            $resource = $resource->gte($column, $time);
						break;

                        //Menor que ou igual
                        case 'date_lte':
//                            $this->filters_current[$key] = $filter_value;
                            // Deve Manter o Horário sem fuso para poder utilizar no mongoDB
                            $time = new \DateTime($filter_value);
                            $time->setTimezone(new \DateTimeZone('UTC'));
                            $time = new \MongoDB\BSON\UTCDateTime($time->format('Uv'));
                            $resource = $resource->lte($column, $time);
						break;

                        //String
                        case 'string':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->eq($column, $filter_value);
                            break;

                        case 'string_normalized':
//                            $this->filters_current[$key] = \DS\Helper::normalizeString($filter_value);
                            $resource = $resource->eq($column, \DS\Helper::normalizeString($filter_value));
                            break;

                        //String Like
                        case 'string_like':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->like($column, $filter_value);
                            break;

                        //String Like divide strings
                        case 'string_like_percent':
                            $filter_value = str_replace(' ', '.*', $filter_value);
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->like($column, $filter_value);
                            break;

                        //Array de Inteiros
                        case 'array_int':
//                            $this->filters_current[$key] = array_map('intval', $filter_value);
                            $resource = $resource->in($column, array_map('intval', $filter_value));
                            break;

                        //Not Array de Inteiros
                        case 'array_not':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->nin($column, $filter_value);
                            break;

                        case 'array_not':
//                            $this->filters_current[$key] = array_map('intval', $filter_value);
                            $resource = $resource->nin($column, array_map('intval', $filter_value));
                            break;

                        //Array de Strings
                        case 'array_string':
//                            $this->filters_current[$key] = $filter_value;
                            $resource = $resource->in($column, $filter_value);

                            break;

                        // Array de Strings normalizada
                        case 'array_string_normalized':
//                            $this->filters_current[$key] = $filter_value;

                            $resource = $resource->or(function($q) use ($column, $filter_value)
                            {
                                foreach ($filter_value as $value) {
                                    $q->eq($column, \DS\Helper::normalizeString($value));
                                }
                            });

                            break;
                        //Array de Strings Like
                        case 'array_string_like':
//                            $this->filters_current[$key] = $filter_value;

                            $resource = $resource->or(function($q) use ($column, $filter_value)
                            {
                                foreach ($filter_value as $value) {
                                    $q->like($column, $value);
                                }
                            });

                            break;

                    }
                }
            }
        }
    }

    public static function getParams(array $data)
    {
        if (isset($_GET['limit'])) {
            $data['query']['limit'] = $_GET['limit'];
        }

        if (isset($_GET['offset'])) {
            $data['query']['offset'] = $_GET['offset'];
        }

        if (isset($_GET['alias'])) {
            $data['alias'] = $_GET['alias'];
        }

        if (isset($_GET['sort'])) {
            $data['query']['sort'] = $_GET['sort'];
        }

        return $data;
    }
}
