<?php

namespace DS\CRUD;

/**
 * Classe que gerencia banco de dados relacionais
 */
class DBRDS implements \DS\CRUD\ManagerDB {

	protected $db = null;
	protected $alias = [];

	//Filtros atual
	protected $filters_current = [];

	function __construct($dbname){
		$this->db = new \DS\Database\TableCRUD($dbname);
	}

	/**
	 * Faz uma busca no banco de dados com os filtros estipulados
	 *
	 * @param  array $filter          	Valores que precisam ser filtrados
	 * @param  array $filters_allowed 	Valores permitidos para busca
	 * @param  array $options         	Opções da busca (limit, offset, order)
	 * @return array return			Retorna array com resultados da busca
	 */
	public function findAll($filter, $filters_allowed, $options = []){

		$this->db->clear();

		//Adiciona filtros nos resultados
		$this->filters($filter, $filters_allowed);

		//Adiciona as opções
		$this->options($options);

		return $this->db
				->select($this->alias)
				->run()
				->getArray();
	}

	/**
	 * Resgata um elemento do banco de dados
	 * Se vários documentos satisfizerem a consulta, esse método retornará o primeiro documento de acordo com a
	 * ordem natural que reflete a ordem dos documentos no disco.
	 *
	 * @param  array $filter          	Valores que precisam ser filtrados
	 * @param  array $filters_allowed 	Valores permitidos para busca
	 * @param  array $options         	Opções da busca (limit, offset, order)
	 * @return array return			Retorna primeiro resultado da busca
	 */
	public function findOne($filter, $options = []){
		$this->db->clear();

		foreach ($filter as $key => $value) {
			$this->db->where($key, ':'.\DS\Helper::normalizeString($key))
				->bindValue(':'.\DS\Helper::normalizeString($key), $value, is_int($value) ? \PDO::PARAM_INT : \PDO::PARAM_STR);
		}

		return $this->db
				->select($this->alias)
				->run()
				->getArray(0);
	}


	/**
	 * Atualiza um documento no banco de dados
	 * Se vários documentos satisfizerem a consulta, esse método atualizará apenas o primeiro documento de acordo com a
	 * ordem natural que reflete a ordem dos documentos no disco.
	 *
	 * @param  array $update          	Valores que precisa ser atualizadas
	 * @param  array $filter          	Valores que precisam ser filtrados
	 * @param  array $filters_allowed 	Valores permitidos para busca
	 * @return boolean				Retorna se atualização foi feita com sucesso
	 */
	public function updateOne($update, $filter, $filters_allowed){
		$this->db->clear();

		//Adiciona filtros nos resultados
		$this->filters($filter, $filters_allowed);

		//Adiciona as opções
		$this->options(['limit' => 1]);

		return $this->db
				->update($update)
				->run()
				->getArray();
	}

	/**
	 * Atualiza vários documentos no banco de dados
	 *
	 * @param  array $update          	Valores que precisa ser atualizadas
	 * @param  array $filter          	Valores que precisam ser filtrados
	 * @param  array $filters_allowed 	Valores permitidos para busca
	 * @return boolean				Retorna se as atualizações foram concluidas com sucesso
	 */
	public function updateAll($update, $filter, $filters_allowed){
		$this->db->clear();

		//Adiciona filtros nos resultados
		$this->filters($filter, $filters_allowed);

		return $this->db
				->update($update)
				->run()
				->getArray();
	}

	/**
	 * Insere um documento no banco de dados
	 *
	 * @param  array $insert 	documento a ser inserido
	 * @return bool         		Se documento foi inserido com sucesso
	 */
	public function insert($insert){
		$this->db->clear();

		return $this->db
				->insert($insert)
				->run()
				->getArray();
	}

	/**
	 * Inserir vários documentos de uma vez
	 *
	 * @param  array  $documents       	Array de documentos
	 * @param  boolean $use_transaction 	Se deve usar transação para inserir documentos
	 * @return boolean                   	Se todos os documentos foram adicionados
	 */

	public function insertMany($documents, $use_transaction = false){
		$this->db->clear();
		if($use_transaction) $this->db->beginTransaction();

		//Insert
		foreach ($documents as $document) {
			$this->insert($document);
		}

		if($use_transaction) $this->db->commit();
	}

	/**
	 * Deleta um documento no banco de dados
	 * Se vários documentos satisfizerem a consulta, esse método deletará apenas o primeiro documento de acordo com a
	 * ordem natural que reflete a ordem dos documentos no disco.
	 *
	 * @param  array $filter          	Valores que precisam ser filtrados
	 * @param  array $filters_allowed 	Valores permitidos para busca
	 * @return boolean				Retorna se documento foi deletado com sucesso
	 */
	public function deleteOne($filter, $filters_allowed){
		$this->db->clear();

		//Adiciona as opções
		$this->options(['limit' => 1]);

		//Adiciona filtros nos resultados
		$this->filters($filter, $filters_allowed);

		//Delete
		$this->db->delete()->run();
	}

	/**
	 * Deleta vários documentos no banco de dados
	 * Caso
	 *
	 * @param  array $filter          	Valores que precisam ser filtrados
	 * @param  array $filters_allowed 	Valores permitidos para busca
	 * @param  boolean $protect_mode 	Se protect_mode estiver ligado, nunca vai deixar você deletar todos os documentos do banco de dados
	 * @return boolean				Retorna se documentos foram deletados com sucesso
	 */
	public function deleteAll($filter, $filters_allowed, $protect_mode = true){
		$this->db->clear();

		//Adiciona filtros nos resultados
		$this->filters($filter, $filters_allowed);

		if($protect_mode && empty($this->filters_current) && !empty($filter)){
			throw new \Exception("Atenção seu filtro não retornou resultados, todos os documentos serão deletados, desative a varivel protect_mode para rodar o código mesmo assim.");
		}

		//Delete
		$this->db->delete()->run();
	}

	/**
	 * Filtra parametros de acordo com os filtros disponiveis
	 *
	 * @param  array $filter          	Valores que precisam ser filtrados
	 * @param  array $filters_allowed 	Valores permitidos para busca
	 * @return null
	 */
	public function filters($filter, $filters_allowed){

		foreach ($filter as $filter_key => $filter_value) {
			foreach ($filters_allowed as $fallow_key => $fallow_value) {
				//Coluna válida
				if($fallow_key == $filter_key){
					//Resgata coluna que deve ser tratada
					$column = $fallow_value['column'] ?? $fallow_key;

					switch ($fallow_value['filter']) {
						//Int
						case 'int':
							$this->filters_current[$key] = $filter_value;
							$this->db->where($column, ':'.\DS\Helper::normalizeString($key))
								->bindValue(':'.\DS\Helper::normalizeString($key), $filter_value, \PDO::PARAM_INT);
						break;

						//Maior que
						case 'int_gt':
							$this->filters_current[$key] = $filter_value;
							$this->db->where($column, '>',':'.\DS\Helper::normalizeString($key))
								->bindValue(':'.\DS\Helper::normalizeString($key), $filter_value, \PDO::PARAM_INT);
						break;

						//Menor que
						case 'int_lt':
							$this->filters_current[$key] = $filter_value;
							$this->db->where($column, '<',':'.\DS\Helper::normalizeString($key))
									->bindValue(':'.\DS\Helper::normalizeString($key), $filter_value, \PDO::PARAM_INT);
						break;

						//Maior que ou igual
						case 'int_gte':
							$this->filters_current[$key] = $filter_value;
							$this->db->where($column, '>=',':'.\DS\Helper::normalizeString($key))
									->bindValue(':'.\DS\Helper::normalizeString($key), $filter_value, \PDO::PARAM_INT);
						break;

						//Menor que ou igual
						case 'int_lte':
							$this->filters_current[$key] = $filter_value;
							$this->db->where($column, '<=',':'.\DS\Helper::normalizeString($key))
									->bindValue(':'.\DS\Helper::normalizeString($key), $filter_value, \PDO::PARAM_INT);
						break;

						//String
						case 'string':
							$this->filters_current[$key] = $filter_value;
							$this->db->where($column, ':'.\DS\Helper::normalizeString($key))
								->bindValue(':'.\DS\Helper::normalizeString($key), $filter_value, \PDO::PARAM_STR);
						break;

						//String Like
						case 'string_like':
							$this->filters_current[$key] = $filter_value;
							$this->db->where($column, 'LIKE', ':'.\DS\Helper::normalizeString($key))
								->bindValue(':'.\DS\Helper::normalizeString($key), $filter_value, \PDO::PARAM_STR);
						break;

						//Array de Inteiros
						case 'array_int':
							$this->filters_current[$key] = $filter_value;
							$inQuery = [];

							foreach ($filter_value as $_key => $_value) {
								$inQuery[] = ':'.\DS\Helper::normalizeString($key).$_key;
							}

							$inQuery = implode(',', $inQuery);

							$this->db->where($fallow_key, 'IN', '('.$inQuery.')');

							foreach ($filter_value as $_key => $_value){
								$this->db->bindValue(':'.\DS\Helper::normalizeString($key).$_key, $_value, \PDO::PARAM_INT);
							}

						break;

						//Array de Strings
						case 'array_string':
							$this->filters_current[$key] = $filter_value;

							$inQuery = [];

							foreach ($filter_value as $_key => $_value) {
								$inQuery[] = ':'.\DS\Helper::normalizeString($key).$_key;
							}

							$inQuery = implode(',', $inQuery);

							$this->db->where($fallow_key, 'IN', '('.$inQuery.')');

							foreach ($filter_value as $_key => $_value){
								$this->db->bindValue(':'.\DS\Helper::normalizeString($key).$_key, $_value, \PDO::PARAM_STR);
							}

						break;

					}
				}
			}
		}
	}

	/**
	 * Configurar Opções do banco de dados
	 *
	 * @param  array $options 	Limit,Order,offset
	 * @return null
	 */
	public function options($options){
		foreach ($options as $key => $value) {
			switch ($key) {
				case 'limit':
					$this->db->limit(':__limitOpt__')
						->bindValue(':__limitOpt__', $value, \PDO::PARAM_INT);
					break;

				case 'offset':
					$this->db->offset(':offsetOpt__')
						->bindValue(':offsetOpt__', $value, \PDO::PARAM_INT);
					break;

				case 'order':
					foreach ($value as $column => $order) {
						$this->db->order($column, $order);
					}
					break;

				case 'project':

					foreach ($value as $column => $alias) {
						if($alias === 1){
							$this->alias[] = $column;
						}else if($alias !== -1){
							$this->alias[] = [$columnm, $alias];
						}

					}
					break;
			}
		}
	}

}