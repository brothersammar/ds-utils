<?php

namespace DS\CRUD;

/**
 * Gerenciador de Databases
 * Mysql
 * MongoDB, ElasticSearch - produção ainda
 */
interface ManagerDB {
	public function findAll($filter, $filters_allowed, $options = []);
	public function findOne($filter, $options = []);
	public function updateOne($update, $filter, $filters_allowed);
	public function updateAll($update, $filter, $filters_allowed);
	public function insert($insert);
	public function insertMany($documents, $use_transaction = false);
	public function deleteOne($filter, $filters_allowed);
	public function deleteAll($filter, $filters_allowed, $protect_mode = true);
	public function filters($filter, $filters_allowed);
	public function options($options);
}