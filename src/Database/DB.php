<?php

namespace DS\Database;

use \PDO;

/**
 * DB
 */
class DB {

    public static function Table($table_name = "", $aspas = true){
        return new TableCRUD($table_name, $aspas);
    }

    public static function commit(){
        return (new TableCRUD())->commit();
    }

    public static function rollBack(){
        return (new TableCRUD())->rollBack();
    }

    public static function beginTransaction(){
        return (new TableCRUD())->beginTransaction();
    }


    public static function status()
    {
        $status = [];
        $status['mariadb'] = [];
        $status['mariadb']['info'] = $GLOBALS['database']->getAttribute(PDO::ATTR_SERVER_INFO) ?? '';
        $status['mariadb']['heath'] = DB::isConnected() ? 'up' : 'down';

        return $status;
    }

    public static function isConnected(){

         try {
            return (bool) $GLOBALS['database']->prepare('SELECT 1+1')->execute();
        } catch (PDOException $e) {
            return false;
        }

    }


    public static function Mongo($db = ''){
        if(empty($db)){
            return new \MongoDB\Client('mongodb://'.MONGO_USER.':'.MONGO_PASS.'@'.MONGO_IP.':'.MONGO_PORT);
        }else{
            return (new \MongoDB\Client('mongodb://'.MONGO_USER.':'.MONGO_PASS.'@'.MONGO_IP.':'.MONGO_PORT))->bdi->{$db};
        }
    }

    public static function checkConnection(){
        try{
            $dbh = new DBConnection();
        }catch(\PDOException $ex){
            return false;
        }

        return true;
    }
}