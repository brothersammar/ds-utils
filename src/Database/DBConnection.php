<?php

namespace DS\Database;

use \PDO;

/**
 *
 * Database
 *
 */
class DBConnection extends \PDO
{
        //Conexão com banco de dados
    function __construct($config = [])
    {
        // Resgata configurações globais
        // Caso não haja, resgata configurações enviadas
        if (empty($config)) {
                if(\DS\Utils\Config::getValue("RDBMS_driver") === false) throw new \Exception('Variable \'RDBMS_driver\' not defined.');
                $this->engine = \DS\Utils\Config::getValue('RDBMS_driver');

                if(\DS\Utils\Config::getValue("RDBMS_host") === false) throw new \Exception('Variable \'RDBMS_host\' not defined.');
                $this->host = \DS\Utils\Config::getValue('RDBMS_host');

                if(\DS\Utils\Config::getValue("RDBMS_dbname") === false) throw new \Exception('Variable \'RDBMS_dbname\' not defined.');
                $this->dbname = \DS\Utils\Config::getValue('RDBMS_dbname');

                if(\DS\Utils\Config::getValue("RDBMS_user") === false) throw new \Exception('Variable \'RDBMS_user\' not defined.');
                $this->user = \DS\Utils\Config::getValue('RDBMS_user');

                if(\DS\Utils\Config::getValue("RDBMS_pass") === false) throw new \Exception('Variable \'RDBMS_pass\' not defined.');
                $this->pass = \DS\Utils\Config::getValue('RDBMS_pass');

                if(\DS\Utils\Config::getValue("RDBMS_port") === false) throw new \Exception('Variable \'RDBMS_port\' not defined.');
                $this->port = \DS\Utils\Config::getValue('RDBMS_port');

            }else{
                if(!isset($config["RDBMS_driver"])) throw new \Exception('Variable \'RDBMS_driver\' not defined.');
                $this->engine = $config['RDBMS_driver'];

                if(!isset($config["RDBMS_host"])) throw new \Exception('Variable \'RDBMS_host\' not defined.');
                $this->host = $config['RDBMS_host'];

                if(!isset($config["RDBMS_dbname"])) throw new \Exception('Variable \'RDBMS_dbname\' not defined.');
                $this->dbname = $config['RDBMS_dbname'];

                if(!isset($config["RDBMS_user"])) throw new \Exception('Variable \'RDBMS_user\' not defined.');
                $this->user = $config['RDBMS_user'];

                if(!isset($config["RDBMS_pass"])) throw new \Exception('Variable \'RDBMS_pass\' not defined.');
                $this->pass = $config['RDBMS_pass'];

                if(!isset($config["RDBMS_port"])) throw new \Exception('Variable \'RDBMS_port\' not defined.');
                $this->port = $config['RDBMS_port'];
            }

            $dns = $this->engine.':dbname='.$this->dbname.";host=".$this->host.";port=".$this->port;
            parent::__construct( $dns, $this->user, $this->pass );
            $this->exec("SET CHARACTER SET utf8");
            $this->setAttribute( \PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC );
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            $this->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

            ini_set("default_socket_timeout", 10);
            $this->setAttribute(\PDO::ATTR_TIMEOUT, 10);
        }
    }