<?php

namespace DS\Database;
use \PDO;

/**
* TableCRUD
* Classe para auxiliar o desenvolvimento agil de requisições ao banco de dados
*/
class TableCRUD
{

    public static $database = "";

    private $rowCount = 0;
    private $result_query = [];
    private $table_name = "";

	//Syntax
    private $BINDPARAM = [];
    private $WHERE = [];
    private $GROUP = [];
    private $HAVING = "";
    private $ORDER = [];
    private $OFFSET = "";
    private $LIMIT = "";

	//Associantions
	private $JOIN = [];
	private $INNER_JOIN = [];
	private $LEFT_JOIN = [];
	private $RIGHT_JOIN = [];
	private $FULL_JOIN = [];

   private $lastInsertId = 0;

	/**
	 * Outros
	 */
	private $query = "";
	private $typeQuery = "";
	private $success = null;
    private $success_execute = null;

    private $aspas = null;


	function __construct($table = '', $aspas = true)
	{
        $this->table_name = $table;
        $this->aspas = $aspas;

        if(empty(self::$database)){
            self::$database = new DBConnection();
        }
	}

    function tableName($table = '')
    {
        $this->table_name = $table;
    }


	/**
	 * Prepara a classe para fazer o comando Select
	 *
	 * $values => são os parametros da comando SELECT
	 * EXEMPLO:
	 * [
	 * "__id", => id
	 * ["__id","id"] => "__id as id"
	 * ]
	 */
	public function select($values = null){
		$this->typeQuery = "select";
		/**
		 * Resgata parametros
		 */
		$selects = [];

        if(!empty($values)){
            foreach($values as $key=>$value){
                if(is_array($value)){
                    $selects[] = $value[0]." as ".$value[1]."";
                }else{
                    $selects[] = $value;
                }
            }
            //Converte parametros
        }
        $selects = implode(",",$selects);

        if(empty($selects))
            $selects = "*";


        /**
         * Base da Query
         */
        if($this->aspas){
            $this->query = "SELECT {$selects} FROM `{$this->table_name}` ";
        }else{
            $this->query = "SELECT {$selects} FROM {$this->table_name} ";
        }

        /**
         * Inner Join
         */
        if(!empty($this->INNER_JOIN)){
            $this->query .= " INNER JOIN ".implode(" JOIN ",$this->INNER_JOIN);
        }

        /**
         * Left Join
         */
        if(!empty($this->LEFT_JOIN)){
            $this->query .= " LEFT JOIN ".implode(" LEFT JOIN ",$this->LEFT_JOIN);
        }

        /**
         * Right Join
         */
        if(!empty($this->RIGHT_JOIN)){
            $this->query .= " RIGHT JOIN ".implode(" RIGHT JOIN ",$this->RIGHT_JOIN);
        }

        /**
         * Full Join
         */
        if(!empty($this->FULL_JOIN)){
            $this->query .= " FULL JOIN ".implode(" FULL JOIN ",$this->FULL_JOIN);
        }

        /**
         * WHERE CONDIÇÃO
         */
        if(!empty($this->WHERE)){
            $this->query .= " WHERE " . implode(" AND ",$this->WHERE);
        }

        /**
         * GROUP CONDIÇÃO
         */
        if(!empty($this->GROUP)) {
            $this->query .= " GROUP BY ".implode(" , ",$this->GROUP);
        }

        /**
         * HAVING CONDIÇÃO
         */
        if($this->HAVING != ""){
            $this->query .= " HAVING ".$this->HAVING;
        }

        /**
         * ORDER CONDIÇÃO
         */
        if(!empty($this->ORDER)) {
            $this->query .= " ORDER BY ".implode(" , ",$this->ORDER);
        }

        /**
         * LIMIT CONDIÇÃO
         */
        if($this->LIMIT != "") {
            $this->query .= " LIMIT ".$this->LIMIT;
        }

        /**
         * OFFSET CONDIÇÃO
         */
        if($this->OFFSET != "") {
            $this->query .= " OFFSET ".$this->OFFSET;
        }

        return $this;
	}


	/**
	 * Prepara a classe para fazer o comando Delete
	 */
	function delete(){
		$this->typeQuery = "delete";

        /**
         * Base da Query
         */
        $this->query = "DELETE FROM `{$this->table_name}` ";

        /**
         * WHERE CONDIÇÃO
         */
        if(!empty($this->WHERE)){
            $this->query .= " WHERE " . implode(" AND ",$this->WHERE);
        }

        /**
         * ORDER CONDIÇÃO
         */
        if(!empty($this->ORDER)) {
            $this->query .= " ORDER BY ".implode(" , ",$this->ORDER);
        }

        /**
         * LIMIT CONDIÇÃO
         */
        if($this->LIMIT != "") {
            $this->query .= " LIMIT ".$this->LIMIT;
        }

        return $this;
	}

	/**
	 * Prepara a classe para fazer o comando Insert
	 */
	function insert($array){
		$this->typeQuery = "insert";

		$keys_names = "";
        $value_names = "";

        foreach((array) $array as $key=>$value){

            // $keys_names .= $key.",";

            // if($value == null){
            //     $value_names .= "NULL,";
            // }else{
            //     $value_names .= "'".$value."',";
            // }

             if($value !== NULL){
                $keys_names .= $key.",";

                if($value[0] == ':'){
                    $value_names .= $value.",";
                }else{
                    $value_names .= "'".$value."',";
                }
            }

            // $keys_names .= $key.",";

            // if($value == null){
            //     $value_names .= "NULL,";
            // }else{
            //     $value_names .= "'".$value."',";
            // }
        }

        $value_names = substr($value_names,0,-1);
        $keys_names = substr($keys_names,0,-1);

        $this->query = "INSERT INTO `{$this->table_name}` ({$keys_names}) VALUES ({$value_names})";

        return $this;
	}

	/**
	 * Prepara a classe para fazer o comando Update
	 */
	public function update($array = null){
		$this->typeQuery = "update";

        $update = " ";

        foreach((array) $array as $key=>$value){
            if($value !== NULL){
                if(is_int($key)){
                    $update .= (string) $value.",";
                }else if($value[0] == ':'){
                    $update .= $key."= ".(string) $value.",";
                }else if(is_int($value)){
                    $update .= $key."= ".(string) $value.",";
                }else{
                    $update .= $key."= '".(string) $value."',";
                }
            }

        }

        $update = substr($update,0,-1);

        $this->query = "UPDATE `{$this->table_name}` ";

        /**
         * Inner Join
         */
        if(!empty($this->INNER_JOIN)){
            $this->query .= " INNER JOIN ".implode(" JOIN ",$this->INNER_JOIN);
        }

        $this->query .= "SET {$update}";

        /**
         * WHERE CONDIÇÃO
         */
        if(!empty($this->WHERE)){
            $this->query .= " WHERE " . implode(" AND ",$this->WHERE);
        }

        /**
         * ORDER CONDIÇÃO
         */
        if(!empty($this->ORDER)) {
            $this->query .= " ORDER BY ".implode(" , ",$this->ORDER);
        }

        /**
         * LIMIT CONDIÇÃO
         */
        if($this->LIMIT != "") {
            $this->query .= " LIMIT ".$this->LIMIT;
        }

        return $this;
    }

    /**
     * Retorna Query em String
     * @return string
     */
    function getString(){
        return $this->query;
    }

    /**
     * Une todas os Selections enviados
     * Deve ser enviado como array
     */

    function union($seletects){
        $this->typeQuery = "select";

        $this->query = implode(" UNION ",$seletects);
         /**
         * WHERE CONDIÇÃO
         */
        if(!empty($this->WHERE)){
            $this->query .= " WHERE " . implode(" AND ",$this->WHERE);
        }

        /**
         * GROUP CONDIÇÃO
         */
        if(!empty($this->GROUP)) {
            $this->query .= " GROUP BY ".implode(" , ",$this->GROUP);
        }

        /**
         * HAVING CONDIÇÃO
         */
        if($this->HAVING != ""){
            $this->query .= " HAVING ".$this->HAVING;
        }

        /**
         * ORDER CONDIÇÃO
         */
        if(!empty($this->ORDER)) {
            $this->query .= " ORDER BY ".implode(" , ",$this->ORDER);
        }

        /**
         * LIMIT CONDIÇÃO
         */
        if($this->LIMIT != "") {
            $this->query .= " LIMIT ".$this->LIMIT;
        }

        /**
         * OFFSET CONDIÇÃO
         */
        if($this->OFFSET != "") {
            $this->query .= " OFFSET ".$this->OFFSET;
        }

        return $this;
    }


    /**
     * Executa um query qualquer estipulada pela string
     */
    public function query($string){
        $this->query = $string;
        return $this;
    }

    public function beginTransaction(){
        return self::$database->beginTransaction();
    }

    public function commit(){
        return self::$database->commit();
    }

    public function rollBack(){
        return self::$database->rollBack();
    }

    /**
     * Executa a Query e retorna o resultado dela
     */
    public function run(){
    	$query = self::$database->prepare($this->query);

        //Bind Values
        foreach ($this->BINDPARAM as $bindValue) {
            $query->bindValue($bindValue['$parameter'], $bindValue['$variable'], $bindValue['$data_type']);
        }

        $this->success = $query->execute();
        $this->success_execute = $this->success;

        $this->lastInsertId = self::$database->lastInsertId();


        switch ($this->typeQuery) {
        	case 'select':
        		//Quantidade de retorno
        		$this->rowCount = $query->rowCount();
        		$this->result_query = $query->fetchAll();
        		break;
            case 'update':
                $this->success = $this->success_execute;
                break;
        }

        return $this;
    }

    /**
     * Retorna quantidade da busca
     */
    public function rowCount(){
        return $this->rowCount;
    }

    /**
     * Retorna Ultimo ID da tabela
     */
    public function lastInsertId(){
        return $this->lastInsertId;
    }

    /**
     * Retorna se comando no MYSQL foi executado com sucesso e retornou algo
     */
    function success(){
    	return $this->success;
    }

     /**
     * Retorna se comando no MYSQL foi executado com sucesso
     */
    function successRun(){
        return $this->success_execute;
    }

    /**
     * Return resultado em Json
     */
    public function getJSON($print_string = false){
        if($print_string){
            echo json_encode($this->result_query);
            return;
        }

        return json_encode($this->result_query);
    }

    /**
     * Returna Array
     */

    public function getArray($key = null){
        if ($key === null) {
            return $this->result_query;
        } else {
            if(empty($this->result_query[$key]))
                return [];
            else
                return $this->result_query[$key];
        }
    }

	/**
	 * adiciona innerJoin
	 */
	public function innerJoin($table, $param1 = "", $param2 = "", $cond = "="){

        if(empty($param1)){
            $this->INNER_JOIN[] = $table." ";
        }else{
            $this->INNER_JOIN[] = $table." on ".$param1." ".$cond." ".$param2." ";
        }



        return $this;
    }

    /**
	 * adiciona innerJoin
	 */
	public function leftJoin($table, $param1, $param2, $cond = "="){

		$this->LEFT_JOIN[] = $table." on ".$param1." ".$cond." ".$param2." ";

        return $this;
    }

    /**
	 * adiciona rightJoin
	 */
	public function rightJoin($table, $param1, $param2, $cond = "="){

		$this->RIGHT_JOIN[] = $table." on ".$param1." ".$cond." ".$param2." ";

        return $this;
    }

    /**
	 * adiciona fullJoin
	 */
	public function fullJoin($table, $param1, $param2, $cond = "="){

		$this->FULL_JOIN[] = $table." on ".$param1." ".$cond." ".$param2." ";

        return $this;
    }

    /**
     * adiciona Where
     */
    public function bindValue($parameter, $variable, $data_type = PDO::PARAM_STR){

        $this->BINDPARAM[] = [
            '$parameter' => $parameter,
            '$variable' => $variable,
            '$data_type' => $data_type,
        ];

        return $this;
    }

    public $whereBlock = [];
    public $whereDeep = 0;
    public $whereTemp = [];

    public function beginBlock(){
        $this->whereDeep++;
        $this->whereBlock[$this->whereDeep] = [];

        return $this;

    }

    public function endBlock(){

        $whereCurrent = implode(' ', $this->whereBlock[$this->whereDeep]);

        $this->whereDeep--;

        $this->where($whereCurrent);

        return $this;
    }

    public function whereOR(){
        $this->whereBlock[$this->whereDeep][] = 'OR';
        return $this;
    }

    /**
	 * adiciona Where
	 */
    public function where(){

        switch(func_num_args()){
            case 1:

                if($this->whereDeep > 0){
                    $lastWhere = $this->whereBlock[$this->whereDeep][count($this->whereBlock[$this->whereDeep])-1] ?? '';
                    if(!empty($lastWhere) && $lastWhere != 'AND' && $lastWhere != 'OR'){
                        $this->whereBlock[$this->whereDeep][] = 'AND';
                    }
                    $this->whereBlock[$this->whereDeep][] = '('.func_get_arg(0).')';
                }else
                    $this->WHERE[] = '('.func_get_arg(0).')';

                break;

            case 2:
                //Caso esteja vazio
                if(func_get_arg(1) === NULL)
                    break;

                //Se tiver : no início então é um bindParam
                if(func_get_arg(1)[0] == ':')
                    if($this->whereDeep > 0){

                        $lastWhere = $this->whereBlock[$this->whereDeep][count($this->whereBlock[$this->whereDeep])-1] ?? '';
                        if(!empty($lastWhere) && $lastWhere != 'AND' && $lastWhere != 'OR'){
                            $this->whereBlock[$this->whereDeep][] = 'AND';
                        }

                        $this->whereBlock[$this->whereDeep][] = '('.func_get_arg(0)." = ".func_get_arg(1)."".')';
                    }else
                        $this->WHERE[] = '('.func_get_arg(0)." = ".func_get_arg(1)."".')';
                else
                    if($this->whereDeep > 0){
                        $lastWhere = $this->whereBlock[$this->whereDeep][count($this->whereBlock[$this->whereDeep])-1] ?? '';

                        if(!empty($lastWhere) && $lastWhere != 'AND' && $lastWhere != 'OR'){
                            $this->whereBlock[$this->whereDeep][] = 'AND';
                        }

                        $this->whereBlock[$this->whereDeep][] = '('.func_get_arg(0)." = '".func_get_arg(1)."'".')';
                    }else
                        $this->WHERE[] = '('.func_get_arg(0)." = '".func_get_arg(1)."'".')';

                break;
            case 3:
                //Caso esteja vazio
                if(func_get_arg(2) === NULL)
                    break;

                if(func_get_arg(1) == "IN" || func_get_arg(2)[0] == ':')
                    if($this->whereDeep > 0){
                        $lastWhere = $this->whereBlock[$this->whereDeep][count($this->whereBlock[$this->whereDeep])-1] ?? '';
                        if(!empty($lastWhere) && $lastWhere != 'AND' && $lastWhere != 'OR'){
                        $this->whereBlock[$this->whereDeep][] = 'AND';
                    }
                        $this->whereBlock[$this->whereDeep][] = '('.func_get_arg(0)." ".func_get_arg(1)." ".func_get_arg(2)."".')';
                    }else
                        $this->WHERE[] = '('.func_get_arg(0)." ".func_get_arg(1)." ".func_get_arg(2)."".')';
                else
                    if($this->whereDeep > 0){
                        $lastWhere = $this->whereBlock[$this->whereDeep][count($this->whereBlock[$this->whereDeep])-1] ?? '';
                        if(!empty($lastWhere) && $lastWhere != 'AND' && $lastWhere != 'OR'){
                        $this->whereBlock[$this->whereDeep][] = 'AND';
                    }
                        $this->whereBlock[$this->whereDeep][] = '('.func_get_arg(0)." ".func_get_arg(1)." '".func_get_arg(2)."'".')';
                    }else
                        $this->WHERE[] = '('.func_get_arg(0)." ".func_get_arg(1)." '".func_get_arg(2)."'".')';

                break;
        }

        return $this;
    }

    /**
     * adiciona Where
     */
    public function having(){

        switch(func_num_args()){
            case 1:
                if($this->HAVING != ""){
                    $this->HAVING .= " AND ";
                }

                $this->HAVING .= func_get_arg(0);
                break;

            case 2:
                //Caso esteja vazio
                if(func_get_arg(1) === NULL)
                    break;

                if($this->HAVING != ""){
                    $this->HAVING .= " AND ";
                }

                $this->HAVING .= func_get_arg(0)." = '".func_get_arg(1)."'";
                break;
            case 3:
                //Caso esteja vazio
                if(func_get_arg(2) === NULL)
                    break;

                if($this->HAVING != ""){
                    $this->HAVING .= " AND ";
                }


                if(func_get_arg(1) == "IN")
                    $this->HAVING .= func_get_arg(0)." ".func_get_arg(1)." ".func_get_arg(2)."";
                else
                    $this->HAVING .= func_get_arg(0)." ".func_get_arg(1)." '".func_get_arg(2)."'";

                break;
        }

        return $this;
    }

    /**
	 * adiciona limit
	 */
    public function limit($value = 0){
        if(empty($value))
            $this->LIMIT = "";
        else
            $this->LIMIT = $value;

        return $this;
    }


    /**
	 * adiciona offset
	 */
    public function offset($value = 0){
        if(empty($value))
            $this->OFFSET = "";
        else
            $this->OFFSET = $value;

        return $this;
    }

     /**
	 * adiciona order
	 */
    public function order(){
        switch(func_num_args()){
            case 2:
                $this->ORDER[] = func_get_arg(0)." ".func_get_arg(1);
                break;
            case 3:
                $this->ORDER[] = func_get_arg(0)." ".func_get_arg(1)." ".func_get_arg(2);
                break;
            default:
                $this->ORDER = [];
                break;
        }

        return $this;
    }

     /**
	 * adiciona group
	 */
    public function group($value = null){
        if(empty($value))
            $this->GROUP = [];
        else
            $this->GROUP[] = $value;

        return $this;
    }

    /**
    * Limpa todas as várias da classe, deixando igual a sua construção
    */
    public function clear(){
        $this->rowCount = 0;
        $this->result_query = [];

        //Syntax
        $this->BINDPARAM = [];
        $this->WHERE = [];
        $this->GROUP = [];
        $this->HAVING = "";
        $this->ORDER = [];
        $this->OFFSET = "";
        $this->LIMIT = "";

        //Associantions
        $this->JOIN = [];
        $this->INNER_JOIN = [];
        $this->LEFT_JOIN = [];
        $this->RIGHT_JOIN = [];
        $this->FULL_JOIN = [];

        $this->lastInsertId = 0;

        /**
         * Outros
         */
        $this->query = "";
        $this->typeQuery = "";
        $this->success = null;
        $this->success_execute = null;
    }
}