<?php

namespace DS;

/**
*
*/
class Helper
{
    /**
     * Verifica se a array é associativa ou sequencial
     * @param  array  $arr
     * @return boolean
     */
    static function is_array_assoc($arr)
    {
        if(!is_array($arr)) return false;

        if (array() === $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
    }

    /**
     * Converte objeto em array
     * @param  object $obj
     * @return array
     */
    static function object_to_array($obj) {
        if(is_object($obj)) $obj = (array) $obj;
        if(is_array($obj)) {
            $new = array();
            foreach($obj as $key => $val) {
                $new[$key] = \DS\Helper::object_to_array($val);
            }
        }
        else $new = $obj;
        return $new;
    }

    /**
     * Convert String em preço
     * @param  string $value
     * @return string
     */
    static public function convertPrice($value, $centavos = true){
      if(empty($value)) return 0;

      if($centavos){
        $value = $value/100;
        return number_format($value, 2, ',', '.');
      }

      return number_format($value, 0, ',', '.');
    }

    /**
     * Cria Máscara
     * @param  string $mask
     * @param  string $str
     * @return string
     */
    function mask($mask, $str){
      $str = str_replace(" ","",$str);

      for($i=0;$i<strlen($str);$i++){
          $mask[strpos($mask,"#")] = $str[$i];
      }

      return $mask;
    }

    /**
     * Criar Máscara para CPF
     * @param  string $mask
     * @param  string $str
     * @return string
     */
    function maskCPF($cpf){
      if(strlen($cpf) != 11) return $cpf;
      return \DS\Helper::mask("###.###.###-##", $cpf);
    }

    /**
     * Criar Máscara para Telefone
     * @param  string $mask
     * @param  string $str
     * @return string
     */
    function maskPhone($phone){

      if(strlen($phone) == 8){
        return \DS\Helper::mask("####-####", $phone);
      }else if(strlen($phone) == 10){
        return \DS\Helper::mask("(##) ####-####", $phone);
      }else if(strlen($phone) == 11){
        return \DS\Helper::mask("(##) #####-####", $phone);
      }

      return $phone;
    }

    /**
     * Converte array para object
     */
    static public function array_to_object($array) {
        return json_decode(json_encode($array));
    }

    /**
     * Converte array para object
     */
    static public function organizeKey($array, $order) {

        if(is_array($array)){
            $new_array = [];
            foreach ($order as $value) {
                if(isset($array[$value])) $new_array[$value] = $array[$value];
            }

            foreach ($array as $key => $value) {
                if(!isset($new_array[$key])) $new_array[$key] = $array[$key];
            }

            return $new_array;

        }else if(is_object($array)){
            $new_array = (object) [];
            foreach ($order as $value) {
                if(isset($array->$value)) $new_array->$value = $array->$value;
            }

            foreach ($array as $key => $value) {
                if(!isset($new_array->{$key})) $new_array->{$key} = $array->{$key};
            }

            return $new_array;
        }

        return $array;
    }

    /**
     * Embaralha Array
     */
    static public function fisherYatesShuffle(&$items, $seed)
    {
        @mt_srand($seed);
        for ($i = count($items) - 1; $i > 0; $i--)
        {
            $j = @mt_rand(0, $i);
            $tmp = $items[$i];
            $items[$i] = $items[$j];
            $items[$j] = $tmp;
        }

        @mt_srand();
    }

    /**
     * Normaliza String
     */
    public static function normalizeString($str){
        $str = Helper::removeAccents($str);
        $str = str_replace("-", " ", $str);
        $str = preg_replace('/[^a-zA-Z0-9 ]+/', '', $str);
        $str = trim($str);
        $str = strtolower($str);

        return $str;
    }

     /**
     * Remove acentos
     * @param  [type] $str [description]
     * @return [type]      [description]
     */
    static public function removeAccents($str){
        if(empty($str)) return $str;
        return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

     /**
      * Converte Array em valores para o bindValues do PDO e já adiciona na query os valores bind
      * @param  PDO &$DB
      * @param  array $data data a ser convertido
      * @return array       retorna valor convertido
      */
    static public function convertBindValues(&$DB, $data){
        $insert = [];
        foreach ($data as $key => $value) {
            $insert[$key] = ':'.\DS\Helper::normalizeString($key);
            $DB->bindValue(':'.\DS\Helper::normalizeString($key),$value);
        }

        return $insert;
    }

    /**
     * Gera token para acesso do usuário
     * @return string token
     */
    static public function generateToken(){
        return base64_encode(md5(random_bytes(32)));
    }

    /**
     * Gera token unico
     * @return string token
     */
    static public function uniqueToken(){
        return base64_encode(uniqid(rand(), true).'-'.\DS\Helper::getDate());
    }

    /**
     * Gera token com timestamp Unico, alterando em um mapa randomico
     * definido pelo primeiro caracter seguidos de 2 caracters randomicos
     *
     * @return string token
     */
    static public function uniqueTokenMin(){
        $seed = rand() % 10000000;
        $seed_hash = \DS\Helper::convertNumbertoHash($seed);

        $now = intval(microtime(true)*10000);
        $dateinit = intval(strtotime('1 March 2018')*10000);

        $ss = \DS\Helper::convertNumbertoHash($now-$dateinit);

        return $seed_hash.\DS\Helper::convertNumbertoHash(rand() % 100000);
    }

    /**
     * Converte numero para hash
     * @return string token
     */
    static public function convertNumbertoHash($value, $seed = 0, $prefix = ''){

        $map = [
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'm',
            'n',
            'p',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'M',
            'N',
            'P',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9'
        ];

        // \DS\Helper::fisherYatesShuffle($map, 0);

        $rest = $value % count($map);
        $div = intval($value/count($map));
        $prefix .= $map[$rest];

        if($div != 0){
            $prefix = \DS\Helper::convertNumbertoHash($div, $seed, $prefix);
        }

        return $prefix;
    }

    /**
     * Retorna Data
     * @param  string $value
     * @return string
     */
    static public function getDate()
    {
        return date('YmdHis');
    }

    /**
     * Remove Todas as keys que não estiverem na array
     * @return Object
     */
    static public function RemoveAllKeysNoExist($object, $object_remove)
    {
        if(is_object($object)){
            $new_object = (object) [];
            foreach ($object as $key => $value) {
                if(in_array($key,$object_remove)){
                    $new_object->{$key} = $value;
                }
            }

            return $new_object;
        }else if(is_array($object)){
            $new_object = [];
            foreach ($object as $key => $value) {
                if(in_array($key,$object_remove)){
                    $new_object[$key] = $value;
                }
            }

            return $new_object;
        }
    }

    /**
     * array_search_multi
     * @param  [type] $array     [description]
     * @param  [type] $key_name  [description]
     * @param  [type] $value_key [description]
     * @return [type]            [description]
     */
    function array_search_multi($array, $key_name, $value_key){
        $array = (array) $array;

        foreach ($array as $value) {
            $value = (array) $value;
            if($value[$key_name] == $value_key){
                return $value;
            }
        }

        return NULL;
    }
}