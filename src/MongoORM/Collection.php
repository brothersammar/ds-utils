<?php

/**
*
* Model para implementação de documento no MONGODB
*
*/

namespace DS\MongoORM;

class Collection
{
	/**
	 * Resultado
	 * @var array
	 */
	protected $array = [];

	/**
	 * Count
	 * @var integer
	 */
	protected $count = 0;

	public function __construct($array, $count)
	{
		$this->array = $array;
		$this->count = $count;
	}

	public function toArray()
	{
		return $this->array;
	}

	public function count()
	{
		return $this->count ?? 0;
	}

	public function first()
	{
		return $this->array[0] ?? NULL;
	}

	public function last()
	{
		return end($this->array) ?? NULL;
	}

}