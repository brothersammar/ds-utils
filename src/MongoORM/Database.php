<?php

/**
*
* Configuração do banco de dados
*
*/

namespace DS\MongoORM;

class Database
{

    /**
     * Conexões registradas
     *
     * @var array
     */

    static $connections = [];

    static $connectionsInit = [];

    /**
     *
     * Adicionar conexão
     *
     * @param  array   $config
     * @param  string  $name
     * @return void
     */

    public function addConnection($config, $name = 'default')
    {
        self::$connections[$name] = $config;
    }

    /**
     *
     *  Resgata uma conexão com banco de dados
     *
     * @param  string  $connection
     * @return
     */
    public static function getConnection($name = 'default')
    {

        if(isset(self::$connectionsInit[$name])) {
            return self::$connectionsInit[$name];
        }

        $host = 'mongodb://';

        if(isset(self::$connections[$name]['srv'])) {
            return new \MongoDB\Client(self::$connections[$name]['srv']);
        }

        if(isset(self::$connections[$name]['user'])){
            $host .= self::$connections[$name]['user'] . ':' . self::$connections[$name]['password'] . '@';
        }

        $host .= self::$connections[$name]['host'];

        $host .= ':' . self::$connections[$name]['port'];

        if(isset(self::$connections[$name]['user'])){
            $host .= ':' . self::$connections[$name]['port'];
        }

        self::$connectionsInit[$name] = new \MongoDB\Client($host);

        return self::$connectionsInit;
    }
}