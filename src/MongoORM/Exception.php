<?php

/**
*
* Model para implementação de documento no MONGODB
*
*/
namespace DS\MongoORM;

class Exception extends \Exception
{
	const DUPLICATE_KEY = 1000;

	const ERROR_UPDATE = 2000;
	const ERROR_SAVE = 2001;
}