<?php

/**
 *
 * Model para implementação de documento no MONGODB
 *
 */

namespace DS\MongoORM;

class Model extends QueryBuilderModel implements \JsonSerializable
{
    /**
     * Nome da conexão
     *
     * @var string
     */
    protected $connection_name = 'default';

    /**
     * Class Mongo para conexão
     *
     * @var string
     */
    public $connection;

    /**
     * Database Nome
     *
     * @var string
     */
    public $database;

    /**
     * Coleção Nome
     *
     * @var string
     */
    public $collection;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table;

    static $globalScope = [];

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'created_at';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updated_at';

    /**
     * Ativa CREATED_AT e UPDATED_AT
     *
     * @var string
     */
    public $timestamps = true;

    /**
     * Formato da data
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s.u';

    /**
     * Se já foi salvo
     *
     * @var string
     */
    public $saved = false;

    /**
     *
     * Atributos do Model
     *
     * @var array
     */
    protected $attributes = [];

    /**
     *
     * Keys que devem ser removidas no Update
     *
     * @var array
     */
    protected $removed = [];

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [];

    /**
     * Variáveis que devem ficar escondidas
     * @var array
     */
    protected $hidden = [];

    /**
     *
     * Variaveis que precisam de tratamento
     *
     * @var array
     *
     */
    protected $casts = [];

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->database = $this->database ?? \DS\MongoORM\Database::$connections[$this->connection_name]['database'];

        //Conexão
        $this->connection = \DS\MongoORM\Database::getConnection($this->connection_name);
        $this->connection = $this->connection->{$this->database}->{$this->collection};

        //Atributos iniciais
        if (!empty($this->attributes)) {
            foreach ($this->attributes as $key => $value) {
                $this->__set($key, $value);
            }
        }

        $this->attributes = array_merge($this->attributes, $this->fillable($attributes));

//	   	$this->connection->createIndex([ 'id' => -1 ], [ 'unique' => -1 ]);
    }

    /**
     * Criar Primary Key
     * @param string $column
     */
    public function createIndex(string $column)
    {
        $this->connection->createIndex([$column => -1], ['unique' => -1]);
    }

    /**
     * Adicionar mais valores nos attrs
     *
     * @param  array $attributes
     * @return void
     */
    public function add(array $data = [])
    {
        //Atributos iniciais
        $this->attributes = array_merge($this->attributes, $this->fillable($data));
    }

    /**
     * Create a new Eloquent model instance.
     *
     * @param  array $attributes
     * @return void
     */
    static function find($id)
    {
        static::boot();

        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        if (is_array($id)) {

            $builder->orderBy($instance->primaryKey, 'desc');

            $_document = $builder->in($instance->primaryKey, $id)->get()->toArray();

            $documents = [];

            foreach ($_document as $document) {
                $documents[] = self::createByAttrs($document);
            }

            return $documents;
        } else {
            $result = $builder->eq($instance->primaryKey, $id)->get()->first();

            if (empty($result)) {
                return NULL;
            }

            return self::createByAttrs($result);
        }
    }

    /**
     * Procura um ID
     *
     * @param  array $attributes
     * @return void
     */
    static function createByAttrs($attributes)
    {
        if (empty($attributes)) return false;

        self::convertMongoDBObject($attributes);

        $document = new static();
        $document->saved = true;

        foreach ($attributes as $key => $value) {
            $document->{$key} = $value;
        }

        return $document;
    }

    static function convertMongoDBObject(&$attributes)
    {
        foreach ($attributes as $key => &$value) {
            if ($value instanceof \MongoDB\Model\BSONDocument) {
                $value = json_decode(json_encode($value));
            } else if ($value instanceof \MongoDB\Model\BSONArray) {
                $value = json_decode(json_encode($value));
            }
        }
    }

    /**
     * Os atributos que são atribuíveis em massa
     *
     * @param  array $data
     * @return array
     */
    public function fillable(array $data)
    {

        $fillable = [];

        foreach ($data as $key => $value) {
            if (in_array($key, $this->fillable)) {
                $fillable[$key] = $value;
            }
        }

        return $fillable;
    }

    /**
     * Os atributos que podem ser visualizados
     *
     * @param  array $data
     * @return array
     */
    public function getVisible()
    {

        $visible = [];

        foreach ($this->attributes as $key => $value) {
            if (!in_array($key, $this->hidden)) {
                $visible[$key] = $this->processVarOut($this->{$key});
            }
        }

        return $visible;
    }

    /**
     * Processa variavel
     *
     * @return array
     *
     */
    public function processVarOut($var)
    {
        if ($var instanceof \MongoDB\BSON\UTCDateTime) {
            $var = $var->toDateTime()->format($this->dateFormat);
        }

        return $var;
    }

    protected static function boot()
    {
    }

    protected static function addGlobalScope(string $name, \Closure $function)
    {
        if (empty(static::$globalScope[static::class])) {
            static::$globalScope[static::class] = [];
        }

        static::$globalScope[static::class][$name] = $function;
    }

    public function getGlobalScope($builder)
    {
        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }
    }

    /**
     * Converte em Array
     *
     * @return array
     *
     */
    public function toArray()
    {
        return $this->getVisible();
    }

    /**
     * Actions
     */

    /**
     * Cria documento de incremento
     * @return int
     */
    public function getIncrement()
    {

        $increment_number = 1;

        $increment = (new static())->connection->findOne(['_id' => 'increment']);

        if (empty($increment)) {
            $increment = (new static())->connection->insertOne([
                '_id' => 'increment',
                'increment' => 1
            ]);

            // if($increment->getInsertedCount() != 1) //error
            $increment_number = 1;

        } else {
            $increment_number = $increment->increment;
        }

        $incrementUpdate = (new static())->connection->updateOne(
            ['_id' => 'increment'],
            ['$inc' => ['increment' => 1]]
        );

        // $this->attributes[$this->primaryKey] = $document->getInsertedId();
        // $this->attributes['_id'] = $document->getInsertedId();

        // if($increment->getInsertedCount() != 1) //error

        return $increment_number;

    }

    /**
     * Cria novo documento, caso o ID não tenha sido definido
     * @param int $id
     * @return bool
     * @throws Exception
     */
    public function save($id = 0)
    {
        try {
            if ($this->saved) return false;

            // Criado em e atualizado
            if ($this->timestamps) {
                $this->attributes[self::CREATED_AT] = new \MongoDB\BSON\UTCDateTime(new \DateTime);
                $this->attributes[self::UPDATED_AT] = $this->attributes[self::CREATED_AT];
            }

            // Adiciona ID
            if (!empty($id) && !isset($this->attributes[$this->primaryKey])) {
                $this->attributes = [$this->primaryKey => $id] + $this->attributes;
            }

            $document = (new static())->connection->insertOne($this->attributes);

            if ($document->getInsertedCount() != 1) {
                throw new Exception("Erro ao salvar documento.", Exception::ERROR_SAVE);
            }

            if ($document->getInsertedId() instanceof \MongoDB\BSON\ObjectId) {
                if (!isset($this->attributes[$this->primaryKey])) {
                    if ($id) {
                        $this->attributes[$this->primaryKey] = $id;
                    } else {
                        $this->attributes[$this->primaryKey] = (string)$document->getInsertedId();
                    }
                }

                $this->attributes['_id'] = (string)$document->getInsertedId();
            }

            $this->saved = true;
        } catch (\Exception $e) {
            if ($e instanceof \MongoDB\Driver\Exception\BulkWriteException) {
                $writeResult = $e->getWriteResult();
                switch ($writeResult->getWriteErrors()[0]->getCode()) {
                    case 11000:
                        throw new \DS\MongoORM\Exception("Chave Primário duplicada", \DS\MongoORM\Exception::DUPLICATE_KEY);
                        break;
                }
            }

            throw $e;
        }


        return true;
    }

    /**
     * Atualiza documento, caso tenha ID
     * @param array $array
     * @return bool
     * @throws Exception
     */
    public function update(array $array = [])
    {
        $incrementing = false;

        if ($this->incrementing) {
            $incrementing = $this->getIncrement();
        }

        $body = [];

        //Atualizado em
        if ($this->timestamps) {
            $this->attributes[self::UPDATED_AT] = new \MongoDB\BSON\UTCDateTime(new \DateTime);
        }

        $body['$set'] = $this->attributes;
        if (isset($body['$set']['_id'])) unset($body['$set']['_id']);

        //Keys que tem que ser removidas
        if (!empty($this->removed)) {
            $body['$unset'] = $this->removed;
        }

        $document = (new static())->connection->updateOne(
            ['_id' => new \MongoDB\BSON\ObjectId((string)$this->attributes['_id'])],
            $body
        );

        if ($document->getMatchedCount() != 1) {
            throw new Exception("Erro ao atualizar documento.", Exception::ERROR_UPDATE);
        }

        return true;
    }

    /**
     * VARIAVEIS
     */

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string $key
     * @return mixed
     */
    public function __get($key)
    {

        $function_name = str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));

        if (method_exists($this, 'get' . $function_name)) {
            return call_user_func(array($this, 'get' . $function_name), $this->attributes[$key]);
        }

        if (array_key_exists($key, $this->attributes)) {
            return $this->processVarOut($this->attributes[$key]);
        }

        return $this->$key ?? NULL;
    }

    /**
     * Dynamically set attributes on the model.
     *
     * @param  string $key
     * @param  mixed $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->attributes[$key] = $value;

        $function_name = str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));

        if (method_exists($this, 'set' . $function_name)) {
            return call_user_func(array($this, 'set' . $function_name), $value);
        }

        //Tratamento
        if (array_key_exists($key, $this->casts)) {
            switch ($this->casts[$key]) {
                case 'time':
                    if ($value instanceof \MongoDB\BSON\UTCDateTime) {
                        $time = $value;
                    } else {
                        $time = new \DateTime($value, new \DateTimeZone('UTC'));
                        $time = new \MongoDB\BSON\UTCDateTime($time->format('Uv'));
                    }

                    $this->attributes[$key] = $time;
                    break;

                case 'object':
                    if (!is_object($value)) {
                        $value = (object)json_decode($value);
                    }

                    $this->attributes[$key] = $value;
                    break;

                case 'array':
                    if (!is_array($value)) {
                        $value = json_decode($value, true);
                    }

                    $this->attributes[$key] = $value;
                    break;

                case 'boolean':
                    $this->attributes[$key] = (!!$value);

                    break;
            }
        }

        return $this;
    }

    /**
     * Unset an attribute on the model.
     *
     * @param  string $key
     * @return void
     */
    public function __unset($key)
    {
        if (isset($this->attributes[$key])) {
            $this->removed[$key] = 1;
            unset($this->attributes[$key]);
        }
    }

    /**
     * Isset an attribute on the model.
     * @param $key
     * @return bool
     */
    public function __isset($key)
    {
        return isset($this->attributes[$key]);
    }

    /**
     *
     * JsonSerializable
     * Retorna valores JSON quando solicitado
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->getVisible();
    }
}
