<?php

/**
*
* Model para implementação de documento no MONGODB
*
*/

namespace DS\MongoORM;

class QueryBuilder
{

	/**
	* conexão
	*
	* @var string
	*/
	static $connection;

	/**
	* Nome da Coleção
	*
	* @var string
	*/
	protected $colletion;

	/**
	* Instancia filha para voltar os dados da busca
	*
	* @var string
	*/
	public $instance_child;

	/**
	* Nome da Database
	*
	* @var string
	*/
	protected $database;

	/**
	* Variavel que encapsula o match
	* @var string
	*/
	public $container = '$and';

	/**
	 * Variavel responsável pela busca
	 * @var array
	 */
	protected $match = [];

	/**
	 * Variavel que contem as agregações
	 * @var array
	 */
	protected $aggregates = [];

	/**
	 * Options
	 * @var array
	 */
	protected $options = [];

	/**
	 * Habilita/Desabilita Aggregate
	 * @var array
	 */
	protected $enabled_aggregate = false;

    //Variaveis temporarias
	protected $__find = [];
	protected $__count = 0;
	protected $__setGlobalScope = false;

	/**
	* Create a new Eloquent model instance.
	*
	* @param  array  $attributes
	* @return void
	*/
	public function __construct($database, $colletion, $instance_child = false)
	{
		$this->database = $database;
		$this->colletion = $colletion;
		$this->instance_child = $instance_child;

		return $this;
	}

	/**
	* Create a new Eloquent model instance.
	*
	* @param  array  $attributes
	* @return void
	*/
    static public function collection($database, $colletion, $instance_child = false)
	{
		return new self($database, $colletion, $instance_child);
	}

	/**
	 * Habilita Aggregate
	 * @var array
	 */
	public function setAggregate(array $aggregate){
		$this->aggregates = $aggregate;
		$this->enabled_aggregate = true;
	}

	/**
	 * Igual ao valor
	 *
	 * @param  array  $attributes
	 * @return void
	*/
	public function equals($key, $value, $addLast = false)
	{
		$this->addAnd([ $key => $value ], $addLast);
		return $this;
	}

	/**
	 * Or
	 * @param  [type]  $function [description]
	 * @param  boolean $addLast  [description]
	 * @return [type]            [description]
	 */
	public function or($function, $addLast = false)
	{
		$q = new self($this->database, $this->colletion);
		$q->container = '$or';

		$function($q);

		if(!empty($q->toArray())){
			$this->addAnd($q->toArray(), $addLast);
		}

		return $this;
	}

	/**
	 * AND
	 * @param  [type]  $function [description]
	 * @param  boolean $addLast  [description]
	 * @return [type]            [description]
	 */
	public function and($function, $addLast = false)
	{
		$q = new self($this->database, $this->colletion);
		$q->container = '$and';

		$function($q);

		if(!empty($q->toArray())){
			$this->addAnd($q->toArray(), $addLast);
		}

		return $this;
	}

	/**
	 * Não igual
	 * @param  array  $attributes
	 * @return void
	*/
	public function notEquals($key, $value, $addLast = false)
	{
		$this->addAnd([ $key => [ '$ne' => $value ] ], $addLast);
		return $this;
	}

	/**
	 * Não igual alias notEquals
	 * @param  array  $attributes
	 * @return void
	*/
	public function ne($key, $value, $addLast = false)
	{
		return $this->notEquals($key, $value, $addLast);
	}

	/**
	 * Não igual
	 * @param  array  $attributes
	 * @return void
	*/
	public function like($key, $value, $addLast = false)
	{
        $value = explode('.*', $value);
        foreach ($value as &$item) {
            $item = \DS\Helper::normalizeString($item);
        }
        $value = implode($value, '.*');

		$this->addAnd([ $key => new \MongoDB\BSON\Regex($value, 'i') ], $addLast);
		return $this;
	}

	/**
	 * Igual ao valor (number)
	 * @param  array  $attributes
	 * @return void
	*/
	public function eq($key, $value, $addLast = false)
	{
		$this->addAnd([ $key => [ '$eq' => $value ] ], $addLast);
		return $this;
	}

	/**
	 * Maior que (number)
	 * @param  array  $attributes
	 * @return void
	*/
	public function gt($key, $value, $addLast = false)
	{
		$this->addAnd([ $key => [ '$gt' => $value ] ], $addLast);
		return $this;
	}

	/**
	 * Maior que ou igual (number)
	 * @param  array  $attributes
	 * @return void
	*/
	public function gte($key, $value, $addLast = false)
	{
		$this->addAnd([ $key => [ '$gte' => $value ] ], $addLast);
		return $this;
	}

	/**
	 * Menor que (number)
	 * @param  array  $attributes
	 * @return void
	*/
	public function lt($key, $value, $addLast = false)
	{
		$this->addAnd([ $key => [ '$lt' => $value ] ], $addLast);
		return $this;
	}

	/**
	 * Menor que ou igual (number)
	 * @param  array  $attributes
	 * @return void
	*/
	public function lte($key, $value, $addLast = false)
	{
		$this->addAnd([ $key => [ '$lte' => $value ] ], $addLast);
		return $this;
	}

	/**
	 * Contem
	 * @param  array  $attributes
	 * @return void
	*/
	public function in($key, array $value, $addLast = false)
	{
		$this->addAnd([ $key => [ '$in' => $value ] ], $addLast);
		return $this;
	}

	/**
	 * Contem
	 * @param  array  $attributes
	 * @return void
	*/
	public function nin($key, array $value, $addLast = false)
	{
		$this->addAnd([ $key => [ '$nin' => $value ] ], $addLast);
		return $this;
	}

	/**
	 * Remove valor da array
	*/
	public function remove($key)
	{
		foreach ($this->match as $k => &$value) {
			if(isset($value[$key])){
				unset($this->match[$k]);
			}
		}

		$this->match = array_values($this->match);
	}

	/**
	 * Adicionar valor na array princial [ $and => [] ]
	*/
	protected function addAnd($value, $addLastArray = false)
	{
		if($addLastArray && count($this->match)){
			$this->match[count($this->match)-1] = array_merge($this->match[count($this->match)-1], $value);
		}else{
			$this->match[] = $value;
		}
	}

	/**
	* Adicionar mais valores nos attrs
	*
	* @param  array  $attributes
	* @return void
	*/
	public function queryOr(array $data = [])
	{
		//Atributos iniciais
		$this->attributes = array_merge($this->attributes, $this->fillable($data));
	}

	/**
	* Ordenar resultados
	*
	* @param  array  $attributes
	* @return void
	*/
	public function orderBy($key = false, $order = false)
	{
		if(empty($this->options['sort'])) $this->options['sort'] = [];

		if($key == false || $order == false){
			$this->options['sort'] = [];
			return $this;
		}

		if($order == 'desc'){
			$this->options['sort'][$key] = -1;
		}else if($order == 'asc'){
			$this->options['sort'][$key] = 1;
		}

		return $this;
	}

	/**
	* Limite
	*
	* @param  array  $attributes
	* @return void
	*/
	public function limit(int $limit)
	{
		$this->options['limit'] = $limit;

		return $this;
	}


	/**
	* offset
	*
	* @param  array  $attributes
	* @return void
	*/
	public function offset(int $offset)
	{
		$this->options['skip'] = $offset;

		return $this;
	}

	/**
	 * Cria a query para busca no MongoDB
	*/
	protected function builder()
	{
		//Aggregate
		if($this->enabled_aggregate){
			return $this->aggregates;
		}

		if(empty($this->container)){
			return $this->match;
		}else{
			if(empty($this->match)) return (object) [];

			$query = [
				$this->container => $this->match
			];

			return $query;
		}
	}

	/**
	 * Cria a query para busca no MongoDB
	*/
	protected function builderOptions()
	{
		return $this->options;
	}

    /**
     * Faz o tratamento da Query
     */
    protected function processQuery()
    {
        if(!$this->__setGlobalScope){
            $this->__setGlobalScope = true;
            (new $this->instance_child)->getGlobalScope($this);
        }

        if(!$this->enabled_aggregate){
            $this->__find = \DS\MongoORM\Database::getConnection()
                ->{$this->database}
                ->{$this->colletion}
                ->find($this->builder(), $this->options);

            $this->__count = \DS\MongoORM\Database::getConnection()
                ->{$this->database}
                ->{$this->colletion}
                ->count($this->builder());
        }else{
            $query = $this->builder();

            if(!empty($this->options['sort']))
                $query[] = ['$sort' => $this->options['sort'] ];

            if(!empty($this->options['skip']))
                $query[] = ['$skip' => $this->options['skip'] ];

            if(!empty($this->options['limit']))
                $query[] = ['$limit' => $this->options['limit'] ];

            $this->__find = \DS\MongoORM\Database::getConnection()
                ->{$this->database}
                ->{$this->colletion}
                ->aggregate($query, $this->options);

            //Count
            $query = $this->builder();
            $query[] = ['$count' => 'count'];

            $this->__count = \DS\MongoORM\Database::getConnection()
                ->{$this->database}
                ->{$this->colletion}
                ->aggregate($query)->toArray()[0]->count ?? 0;
        }

    }


    /**
	 * Buscar
	*/
	public function get()
	{
		if(!$this->enabled_aggregate){
			$find = \DS\MongoORM\Database::getConnection()
					->{$this->database}
					->{$this->colletion}
					->find($this->builder(), $this->options);

			$count = \DS\MongoORM\Database::getConnection()
					->{$this->database}
					->{$this->colletion}
					->count($this->builder());
		}else{
			$query = $this->builder();

			if(!empty($this->options['sort']))
				$query[] = ['$sort' => $this->options['sort'] ];

			if(!empty($this->options['skip']))
				$query[] = ['$skip' => $this->options['skip'] ];

			if(!empty($this->options['limit']))
				$query[] = ['$limit' => $this->options['limit'] ];

			$find = \DS\MongoORM\Database::getConnection()
					->{$this->database}
					->{$this->colletion}
					->aggregate($query, $this->options);

			//Count
			$query = $this->builder();
			$query[] = ['$count' => 'count'];

            $count = \DS\MongoORM\Database::getConnection()
                ->{$this->database}
                ->{$this->colletion}
                ->aggregate($query)->toArray();

            if(empty($count)) {
                $count = 0;
            } else {
                $count = $count[0]->count;
            }
		}

		$result = [];
		foreach ($find as $key => $value) {
			if(!empty($this->instance_child)){
				$instance = $this->instance_child::createByAttrs($value);
				$result[] = $instance;
			}else{
				$result[] = $value;
			}
		}

		$colletion = new Collection($result, $count);

		return $colletion;
	}

    /**
     * Buscar
     * @return int
     */
    public function count()
    {
        if(!$this->enabled_aggregate){
            return \DS\MongoORM\Database::getConnection()
                ->{$this->database}
                ->{$this->colletion}
                ->count($this->builder());
        }else{
            $query = $this->builder();

            if(!empty($this->options['sort']))
                $query[] = ['$sort' => $this->options['sort'] ];

            if(!empty($this->options['skip']))
                $query[] = ['$skip' => $this->options['skip'] ];

            if(!empty($this->options['limit']))
                $query[] = ['$limit' => $this->options['limit'] ];

            // Count
            $query = $this->builder();
            $query[] = ['$count' => 'count'];

            $count = \DS\MongoORM\Database::getConnection()
                ->{$this->database}
                ->{$this->colletion}
                ->aggregate($query)->toArray();

            if(empty($count)) {
                return 0;
            } else {
                return $count[0]->count;
            }
        }
    }

    /**
     *
     * @return [type] [description]
     */
    function chunk($parts, \Closure $call){

        $offset = $this->options['skip'] ?? 0;

        while(1) {
            $this->limit($parts);
            $this->offset($offset);
            // Processa Query
            $this->processQuery();

            $it = new \IteratorIterator($this->__find);
            $it->rewind();

            // Se não tiver mais documentos
            if(empty($it->current())){
                break;
            }

            // Passa para array
            $result = [];
            foreach ($it as $item) {
                if(!empty($this->instance_child)){
                    $instance = $this->instance_child::createByAttrs($item);
                    $result[] = $instance;
                }else{
                    $result[] = $item;
                }
            }

            // Chama função
            $result = $call($result);

            if($result !== 'redo') {
                $offset += $parts;
            }
        }


    }

	/**
	 * Atualizar Vários documetnos
	*/
	public function updateMany(array $data = [], array $options = [])
	{
		$updateResult = \DS\MongoORM\Database::getConnection()
				->{$this->database}
				->{$this->colletion}
				->updateMany($this->builder(), $data, $options);

		return !!$updateResult->getMatchedCount();
	}

	/**
	 * Atualizar um ou o primeiro elemento
	*/
	public function updateOne(array $data = [], array $options = [])
	{
		$updateResult = \DS\MongoORM\Database::getConnection()
				->{$this->database}
				->{$this->colletion}
				->updateOne($this->builder(), $data, $options);

		return !!$updateResult->getMatchedCount();
	}

	/**
	 * First
	*/
	public function first()
	{
		$find = \DS\MongoORM\Database::getConnection()
				->{$this->database}
				->{$this->colletion}
				->findOne($this->builder());

		if(!empty($this->instance_child)){
			return $this->instance_child::createByAttrs($find);
		}else{
			return $find;
		}
	}

	/**
	 * Cria a query para busca no MongoDB
	*/
	public function toString()
	{
		if($this->enabled_aggregate){
			$query = $this->builder();

			if(!empty($this->options['sort']))
				$query[] = ['$sort' => $this->options['sort'] ];

			if(!empty($this->options['skip']))
				$query[] = ['$skip' => $this->options['skip'] ];

			if(!empty($this->options['limit']))
				$query[] = ['$limit' => $this->options['limit'] ];

			return json_encode($query);
		}else{
			return json_encode($this->builder());
		}
	}

	/**
	 * Cria a query para com opções
	*/
	public function toStringOptions()
	{
		return json_encode($this->builderOptions());
	}

	/**
	 * Array
	*/
	public function toArray()
	{
		return $this->builder();
	}
}
