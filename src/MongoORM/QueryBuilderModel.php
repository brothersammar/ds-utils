<?php

/**
 *
 * Model para implementação de documento no MONGODB
 *
 */

namespace DS\MongoORM;

class QueryBuilderModel
{
    /**
     * Igual ao valor
     *
     * @param  array $attributes
     * @return void
     */
    public function equals($key, $value, $addLast = false)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->equals($key, $value);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    public function or($function)
    {
        static::boot();
        $instance = new static();
        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->OR($function);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    public function and($function)
    {
        static::boot();
        $instance = new static();
        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->AND($function);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Não igual
     * @param  array $attributes
     * @return void
     */
    public function notEquals($key, $value, $addLast = false)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->notEquals($key, $value, $addLast);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Não igual
     * @param  array $attributes
     * @return void
     */
    public function like($key, $value, $addLast = false)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->like($key, $value, $addLast);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Igual ao valor (number)
     * @param  array $attributes
     * @return void
     */
    public function eq($key, $value, $addLast = false)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->eq($key, $value, $addLast);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Diferente ao valor (number)
     * @param  array $attributes
     * @return void
     */
    public function ne($key, $value, $addLast = false)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->ne($key, $value, $addLast);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Maior que (number)
     * @param  array $attributes
     * @return void
     */
    public function gt($key, $value, $addLast = false)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->gt($key, $value, $addLast);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Maior que ou igual (number)
     * @param  array $attributes
     * @return void
     */
    public function gte($key, $value, $addLast = false)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->gte($key, $value, $addLast);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Menor que (number)
     * @param  array $attributes
     * @return void
     */
    public function lt($key, $value, $addLast = false)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->lt($key, $value, $addLast);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Menor que ou igual (number)
     * @param  array $attributes
     * @return void
     */
    public function lte($key, $value, $addLast = false)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->lte($key, $value, $addLast);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Contem
     * @param  array $attributes
     * @return void
     */
    public function in($key, array $value, $addLast = false)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->in($key, $value, $addLast);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Não Contem
     * @param  array $attributes
     * @return void
     */
    public function nin($key, array $value, $addLast = false)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->nin($key, $value, $addLast);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Não Contem
     * @param  array $attributes
     * @return void
     */
    public function orderBy($key, $order)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->orderBy($key, $order);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Limite
     *
     * @param  array $attributes
     * @return void
     */
    public function limit(int $limit)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->limit($limit);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }


    /**
     * offset
     *
     * @param  array $attributes
     * @return void
     */
    public function offset(int $offset)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->offset($offset);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

    /**
     * Chunk
     * @param  array $attributes
     * @return void
     */
    public function chunk(int $parts, \Closure $callable)
    {
        static::boot();
        $instance = new static();

        $builder = \DS\MongoORM\QueryBuilder::collection($instance->database, $instance->collection, static::class)
            ->chunk($parts, $callable);

        if (isset(static::$globalScope[static::class])) {
            foreach (static::$globalScope[static::class] as $scope) {
                $scope($builder);
            }
        }

        return $builder;
    }

}
