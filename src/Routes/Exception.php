<?php

namespace DS\Routes;

class Exception extends \Exception
{

    private $_body;

    public function __construct($message,
                                $code = 0,
                                Exception $previous = null,
                                $body = array())
    {
        parent::__construct($message, $code, null);

        $this->_body = $body;
    }

    public function getBody() { return $this->_body; }
}