<?php

namespace DS\Tratament;

/**
 * Gerenciador de Resposta para o Servidor
 */
class ResponseManager {

	static public function successJSON($data = ''){

		$response = [
			'success' => true,
			'body' => $data
		];

		return $response;
	}

	public static function failedJSON($error = '', $code = 0, $hash = ''){

		$response = [
			'error' => true,
			'code' => $code,
			'message' => $error
		];

		if(!empty($hash)) $response['hash'] = $hash;

		return $response;
	}
}