<?php

namespace DS\Utils;

/**
* Configurações da plataforma
* Pode ser adiciona qualquer tipo de váriavel nessa classe
* desde strings, boolean até funções e classes
*/

class Config
{
	static public $configs = [
		'extra_logs' => [] # Informações Extra que pode ser adicionadas nos logs
	];

	static public function setValue($key, $value)
	{
		Config::$configs[$key] = $value;
	}

	static public function getValue($key)
	{
		if(isset(Config::$configs[$key])){
			if(is_callable(Config::$configs[$key])){
				return Config::$configs[$key]();
			}else{
				return Config::$configs[$key];
			}
		}else{
			return false;
		}
	}
}