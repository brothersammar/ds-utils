<?php

namespace DS\Utils;

use PHPMailer\PHPMailer\PHPMailer;

/**
 * Classe para o envio e resgate de emails
 *
 * Example:
 *
 * $email_sender = new \DS\Utils\EmailManager;
 * $email_sender->addAddress('albertoammarw@gmail.com');
 * $email_sender->msgHTML('kkk');
 * $email_sender->send();
 */
class EmailManager
{
	private $mail = null;

	function __construct($config = [])
	{
		$this->sender($config);
	}

	private function sender($config = [])
	{
		date_default_timezone_set('Etc/UTC');

		//Create a new PHPMailer instance
		$this->mail = new PHPMailer;

		//Tell PHPMailer to use SMTP
		$this->mail->isSMTP();
		$this->mail->CharSet = 'UTF-8';

		//$this->mail->SMTPDebug = 3;                               // Enable verbose debug output

		$this->mail->SMTPDebug = 0;
		$this->mail->Debugoutput = 'html';

		$this->mail->IsSMTP();
		$this->mail->SMTPAuth = true;
		$this->mail->SMTPSecure = false;

		if(empty($config)){

			if(\DS\Utils\Config::getValue("email_host") === false) throw new \Exception('Variable \'email_host\' not defined.');
			$this->mail->Host = \DS\Utils\Config::getValue("email_host");

			if(\DS\Utils\Config::getValue("email_port") === false) throw new \Exception('Variable \'email_port\' not defined.');
			$this->mail->Port = \DS\Utils\Config::getValue("email_port");

			if(\DS\Utils\Config::getValue("email_user") === false) throw new \Exception('Variable \'email_user\' not defined.');
			$this->mail->Username = \DS\Utils\Config::getValue("email_user");

			if(\DS\Utils\Config::getValue("email_pass") === false) throw new \Exception('Variable \'email_pass\' not defined.');
			$this->mail->Password = \DS\Utils\Config::getValue("email_pass");

			//Remetente
			if(\DS\Utils\Config::getValue("email_send_name") === false) throw new \Exception('Variable \'email_send_name\' not defined.');
			if(\DS\Utils\Config::getValue("email_send") === false) throw new \Exception('Variable \'email_pass\' not defined.');
			$this->mail->setFrom(\DS\Utils\Config::getValue("email_send"), \DS\Utils\Config::getValue("email_send_name"));

		}else{
			if(!isset($config["email_host"])) throw new \Exception('Variable \'email_host\' not defined.');
			$this->mail->Host = $config["email_host"];

			if(!isset($config["email_port"])) throw new \Exception('Variable \'email_port\' not defined.');
			$this->mail->Port = $config["email_port"];

			if(!isset($config["email_user"])) throw new \Exception('Variable \'email_user\' not defined.');
			$this->mail->Username = $config["email_user"];

			if(!isset($config["email_pass"])) throw new \Exception('Variable \'email_pass\' not defined.');
			$this->mail->Password = $config["email_pass"];

			//Remetente
			if(!isset($config["email_send_name"])) throw new \Exception('Variable \'email_send_name\' not defined.');
			if(!isset($config["email_send"])) throw new \Exception('Variable \'email_pass\' not defined.');
			$this->mail->setFrom($config["email_send"], $config["email_send_name"]);

		}

		$this->mail->smtpConnect(
			    array(
			        "ssl" => array(
			            "verify_peer" => false,
			            "verify_peer_name" => false,
			            "allow_self_signed" => true
			        )
			    )
			);
	}

	//Destinatários
	function addAddress($email,$name = ""){
		$this->mail->addAddress($email, $name);
		return $this;
	}

	function addReplyTo($email,$name = ""){
		$this->mail->addReplyTo($email,$name);
		return $this;
	}

	/**
	 * Titulo
	 */
	function subject($subject){
		$this->mail->Subject = $subject;
		return $this;
	}

	function msgHTML($message){
		$this->mail->msgHTML($message);
		return $this;
	}

	function msg($message){
		$this->mail->AltBody = $message;
		return $this;
	}

	function send(){
		if (!$this->mail->send()) {
		    return false;
		} else {
			return true;
		}
	}
}

?>