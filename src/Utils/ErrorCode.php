<?php

namespace DS\Utils;

/**
* Tratamento das exceções para mostrar corretamento na API
*/

class ErrorCode
{
	const DUPLICATE_ENTRY = 1;
	const NOT_FOUND = 2;
	const UNAUTHORIZED = 3;

	// Erro no servidor
	const SERVER_ERROR = 3;
}