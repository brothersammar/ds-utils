<?php

namespace DS\Utils;

/**
* Tratamento das exceções para mostrar corretamento na API
*/

class ExceptionManager
{
	/**
	 * Retorna getCode e StatusCode HTTP
	 * @param  [type] $e       [description]
	 * @param  string $message [description]
	 * @return [type]          [description]
	 */
	static public function getCode($e, $message = '')
	{
		if ($e instanceof \PDOException) {
			if(@$e->errorInfo[1] == 1062){
				return [
					'message' => $message ?? 'Coluna duplicada.',
					'code_response' => 400,
				];
			}
		}else if ($e instanceof \DS\Utils\DefaultException) {

			switch (variable) {
				case \DS\Utils\ErrorCode::DUPLICATE_ENTRY:
					return [
						'message' => $message ?? 'Entrada duplicada.',
						'code_response' => 400,
					];
					break;

				case \DS\Utils\ErrorCode::NOT_FOUND:
					return [
						'message' => $message ?? 'Objeto não encontrado.',
						'code_response' => 404,
					];
					break;

				case \DS\Utils\ErrorCode::UNAUTHORIZED:
					return [
						'message' => $message ?? 'Não autorizado.',
						'code_response' => 401,
					];
					break;

				case \DS\Utils\ErrorCode::SERVER_ERROR:
					return [
						'message' => $message ?? 'Erro no servidor, tente novamente mais tarde.',
						'code_response' => 500,
					];
					break;
			}
		}
	}
}