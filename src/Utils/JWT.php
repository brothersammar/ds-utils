<?php

namespace DS\Utils;

/**
 * JWT encode e decode
 */
class JWT
{
	static public function encode($payload, $key)
	{
		return \Firebase\JWT\JWT::encode($payload, $key);
	}

	static public function decode($jwt, $key)
	{
		return \Firebase\JWT\JWT::decode($jwt, $key, array('HS256'));
	}
}