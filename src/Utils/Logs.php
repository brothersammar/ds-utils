<?php

namespace DS\Utils;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\FirePHPHandler;

/**
 *
 */
class Logs
{
    const DEBUG = \Monolog\Logger::DEBUG; # Detailed debug information.
    const INFO = \Monolog\Logger::INFO; # Interesting events. Examples: User logs in, SQL logs.
    const NOTICE = \Monolog\Logger::NOTICE; # Normal but significant events.
    const WARNING = \Monolog\Logger::WARNING; # Exceptional occurrences that are not errors. Examples: Use of deprecated APIs, poor use of an API, undesirable things that are not necessarily wrong.
    const ERROR = \Monolog\Logger::ERROR; # Runtime errors that do not require immediate action but should typically be logged and monitored.
    const CRITICAL = \Monolog\Logger::CRITICAL; # Critical conditions. Example: Application component unavailable, unexpected exception.
    const ALERT = \Monolog\Logger::ALERT; # Action must be taken immediately. Example: Entire website down, database unavailable, etc. This should trigger the SMS alerts and wake you up.
    const EMERGENCY = \Monolog\Logger::EMERGENCY; # Emergency: system is unusable.

    /**
     * @param $type
     * @param $message
     * @param array $body
     * @param int $level
     * @param array $extra
     * @param string $name_log
     * @param null $hash
     * @return bool
     * @throws \Exception
     */
    static public function addLog(
        $type,
        $message,
        $body = [],
        $level = Logs::INFO,
        $extra = [],
        $name_log = '',
        $hash = NULL)
    {
        //Verifica se pode ser usado os logs
        if (\DS\Utils\Config::getValue('disable_logs') === true) return false;

        //Verifica se path está definido
        if (empty(\DS\Utils\Config::getValue('assests_dir'))) {
            throw new \Exception('Variable \'assests_dir\' not defined.');
        }

        $formatter = new JsonFormatter();

        if (empty($name_log)) {
            $stream = new StreamHandler(\DS\Utils\Config::getValue('assests_dir') . '/logs-' . $type . '-' . date('Y-m-d') . '.json', \Monolog\Logger::DEBUG);
        } else {
            $stream = new StreamHandler(\DS\Utils\Config::getValue('assests_dir') . '/' . $name_log . '.json', \Monolog\Logger::DEBUG);
        }

        $stream->getUrl();
        $stream->setFormatter($formatter);

        $logger = new Logger($type);
        $logger->pushHandler($stream);

        $logger->pushProcessor(function ($record) {

            //Para definir um grupo para o logo, normalmente a mensagem para aquela ação é a mesma,
            //mas sempre com dados diferentes
            $record['log_group'] = md5(serialize($record['message'] . $record['level']));
            //Serve para saber quais logs são repetidos
            $record['log_code'] = md5(serialize($record['context']) . $record['message'] . $record['level']);
            $record['hash'] = $hash ?? \DS\Helper::uniqueTokenMin();

            if (!empty(\DS\Utils\Config::getValue('extra_logs'))) {
                $record['extra'] = \DS\Utils\Config::getValue('extra_logs');
            }

            return $record;
        });

        $logger->addRecord($level, $message, $body);

        return true;
    }

    /**
     * @param $level
     * @return int
     */
    static public function getLevel($level)
    {
        switch ($level) {
            case Logs::DEBUG:
                return \Monolog\Logger::DEBUG;
                break;
            case Logs::INFO:
                return \Monolog\Logger::INFO;
                break;
            case Logs::NOTICE:
                return \Monolog\Logger::NOTICE;
                break;
            case Logs::WARNING:
                return \Monolog\Logger::WARNING;
                break;
            case Logs::ERROR:
                return \Monolog\Logger::ERROR;
                break;
            case Logs::CRITICAL:
                return \Monolog\Logger::CRITICAL;
                break;
            case Logs::ALERT:
                return \Monolog\Logger::ALERT;
                break;
            case Logs::EMERGENCY:
                return \Monolog\Logger::EMERGENCY;
                break;
        }
    }
}

?>
