<?php
namespace App\Utils;
/**
* 
*/
class Response
{
	
	static function success($body)
	{
		return json_encode([
				'success' => true,
				'body' => $body
		]);
	}

	static function error($body)
	{
		return json_encode([
				'success' => false,
				'body' => $body
		]);
	}
}