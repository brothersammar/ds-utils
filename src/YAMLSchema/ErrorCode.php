<?php

namespace DS\YAMLSchema;

class ErrorCode
{
	public const ERROR_REQUIRED = 10;
	public const ERROR_DATA = 20;
	public const TYPE_ERROR = 100;
	public const RANGE_MAX = 110;
	public const RANGE_MIN = 111;

	public const INVALID_CPF = 112;
	public const INVALID_CEP = 113;
	public const INVALID_EMAIL = 114;
	public const INVALID_SAME = 115;

	public const NOT_ALLOWED_DATA = 200;
}