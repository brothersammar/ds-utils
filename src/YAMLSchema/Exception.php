<?php

namespace DS\YAMLSchema;

class Exception extends \Exception
{

    private $_body;

    public function __construct($message,
                                $code = 0,
                                $body = array(),
                                \Exception $previous = null)
    {
        parent::__construct($message, $code, null);
        $this->_body = $body;
    }

    public function getBody() {
        return $this->_body;
    }
}