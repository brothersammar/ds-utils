<?php

namespace DS\YAMLSchema;

/**
 * Validator
 */

class Validator
{

    static $additionalProperties = [];

    public static function fastValidator(array &$data, array $contain)
    {
        $data = array_filter($data);
        $required = [];
        foreach ($contain as $key) {
            if (!array_key_exists($key, $data)) {
                $required[] = $key;
            }
        }

        if (!empty($required)) {
            throw new Exception('Valores obrigatórios faltando: ' . implode(',', $required), ErrorCode::ERROR_REQUIRED);
        }
    }

    /**
     * [validateF description]
     * @param  [type] $file       [description]
     * @param  [type] &$array     [description]
     * @param  array $options [description]
     * @param  array &$rest_data [description]
     * @return [type]             [description]
     */
    public static function validateF($data, &$array, $options = [], &$rest_data = [])
    {

        $validator = new \DS\YAMLSchema\ValidatorSchema($data, $array, $options, $rest_data);
        $array = $validator->validate();

        return;

        //Load YAML
        $YAML = \yaml_parse(file_get_contents($file));

        //Empty
        self::$additionalProperties = [];

        //Normalized
        $array_normalized = Validator::normalizeArray($array);
        $template_normalized = Validator::normalizeTemplate($YAML);
        $template_required = Validator::getRequired($YAML);

        return Validator::validate($array_normalized, $template_normalized, $template_required, $array, $options, $rest_data);
    }

    public static function validateD($data, &$array, $options = [], &$rest_data = [])
    {

        $validator = new \DS\YAMLSchema\ValidatorSchema($data, $array, $options, $rest_data);
        $array = $validator->validate();

        return;

        //Load YAML
        $YAML = \yaml_parse($data);

        //Empty
        self::$additionalProperties = [];

        //Normalized
        $array_normalized = Validator::normalizeArray($array);
        $template_normalized = Validator::normalizeTemplate($YAML);
        $template_required = Validator::getRequired($YAML);

        return Validator::validate($array_normalized, $template_normalized, $template_required, $array, $options, $rest_data);
    }

    /**
     * Valida Entrada
     * @param  [type] $array    [description]
     * @param  [type] $template [description]
     * @param  [type] $required [description]
     * @return [type]           [description]
     */
    public static function validate($array, $template, $required, &$o_array, $options = [], &$rest_data)
    {
        $errors = [];
        $newArray = [];
        $rest_data = [];

        /**
         * Valida Valores
         */
        foreach ($array as $key => $value) {
            if (isset($value['ignore']) || $value['ignore']) continue;

            if (isset($template[$key])) {
                try {
                    Validator::validateItem($template[$key], $value['value'], $array, $key, $template);
                    //Adiciona valor a nova array
                    $newArray[$key] = $value['value'];
                } catch (Exception $e) {
                    $errors[] = [
                        'name' => $template[$key]['name'] ?? $key,
                        'path' => $key,
                        'code' => $e->getCode(),
                        'message' => $e->getMessage(),
                        'body' => $e->getBody(),
                    ];
                }
            } else {
                //additionalProperties
                //Verifica se deve excluir ou adicionar variveis que não foram definidas
                if (self::$additionalProperties[$value['__parent__']]) {
                    $newArray[$key] = $value['value'];
                } else {
                    $rest_data[$key] = $value['value'];
                }
            }
        }

        /**
         * Valores Defaults
         */
        foreach ($template as $key => $value) {
            if (!isset($newArray[$key])) {
                if (isset($value['default'])) {
                    $newArray[$key] = $value['default'];
                }
            }
        }


        /**
         * Caso exista erro no formato dos dados
         */
        if (!empty($errors)) {
            throw new Exception($errors[0]['message'], $errors[0]['code'], $errors);
            // throw new Exception.php('YAML ERRORS', ErrorCode::ERROR_DATA, $errors);
        }

        /**
         * Valores obrigatórios
         */
        $values_missing = [];
        $values_missing_names = [];

        /**
         * Definir todos como required
         * Para testes e afins
         */
        if (isset($options['set_all_required']) && $options['set_all_required']) {
            foreach ($template as $key => $value) {
                if (!isset($newArray[$key])) {

                    if ($value['ignore']) continue;

                    $values_missing[] = [
                        'name' => $template[$key]['name'] ?? $key,
                        'path' => $key,
                        'code' => ErrorCode::ERROR_REQUIRED,
                        'message' => $key . ' é obrigatórido.'
                    ];

                    $values_missing_names[] = $template[$key]['name'] ?? $key;
                }
            }
        }

        //Ignore obrigatórios
        if (!(isset($options['ignore_required']) && $options['ignore_required'])) {
            foreach ($required as $value) {
                if (!isset($array[$value])) {
                    $values_missing[] = [
                        'name' => $template[$value]['name'] ?? $value,
                        'path' => $value,
                        'code' => ErrorCode::ERROR_REQUIRED,
                        'message' => $value . ' é obrigatórido.'
                    ];

                    $values_missing_names[] = $template[$value]['name'] ?? $value;
                }
            }
        }

        /**
         * Caso falte algum valor obrigatório
         */
        if (!empty($values_missing)) {
            throw new Exception('Valores obrigatórios faltando: ' . implode(',', $values_missing_names), ErrorCode::ERROR_REQUIRED, $values_missing);
        }

        $o_array = Validator::returnArray($newArray);

        return !empty($errors) ? $errors : true;
    }

    /**
     * Valida Todos Items
     * @param  [type] $reqs  [description]
     * @param  [type] $value [description]
     * @param  [type] $array [description]
     * @param  [type] $path  [description]
     * @return [type]        [description]
     */
    public static function validateItem($reqs, &$value, $array, $path, $template = [])
    {

        $name_template = $reqs['name'] ?? $path;

        $info_error = [
            'path' => $path
        ];

        // Tipo
        if (!isset($reqs['type'])) {
            throw new Exception('Não foi definido um tipo para ' . $name_template);
        } else {
            switch ($reqs['type']) {
                // Para caso de CEP, Numero de casas, onde é necessário ser uma string porem
                case 'string-number':
                    if (!is_numeric($value) && !empty($value)) {
                        //Convert em String
                        $info_error['expect'] = gettype($value);
                        $info_error['current'] = 'string-number';
                        throw new Exception($name_template . ' precisa ser string-number porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                    }

                    $value = (string)$value;

                    break;
                case 'string':
                    if (!is_string($value) && !is_numeric($value)) {
                        //Convert em String
                        $info_error['expect'] = gettype($value);
                        $info_error['current'] = 'string';
                        throw new Exception($name_template . ' precisa ser string porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                    }

                    if (is_numeric($value)) {
                        $value = (string)$value;
                    }

                    break;
                case 'number':

                    //Caso seja vazia coloca valor default
                    if (empty($value) && isset($reqs['default'])) {
                        $value = $reqs['default'];
                        return true;
                    }


                    if (!is_numeric($value)) {
                        $info_error['expect'] = gettype($value);
                        $info_error['current'] = 'number';
                        throw new Exception($name_template . ' precisa ser number porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                    } else {
                        $value = ($value == (int)$value) ? (int)$value : (float)$value;
                    }

                    break;
                case 'int':

                    //Caso seja vazia coloca valor default
                    if (empty($value) && isset($reqs['default'])) {
                        $value = $reqs['default'];
                        return true;
                    }

                    if (!is_numeric($value)) {
                        $info_error['expect'] = gettype($value);
                        $info_error['current'] = 'int';
                        throw new Exception($name_template . ' precisa ser int porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                    } else {
                        $value = ($value == (int)$value) ? (int)$value : (float)$value;

                        if (!is_int($value)) {
                            $info_error['expect'] = gettype($value);
                            $info_error['current'] = 'int';
                            throw new Exception($name_template . ' precisa ser int porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                        }
                    }
                    break;
                case 'float':
                    //Caso seja vazia coloca valor default
                    if (empty($value) && isset($reqs['default'])) {
                        $value = $reqs['default'];
                        return true;
                    }

                    if (!is_numeric($value)) {
                        $info_error['expect'] = gettype($value);
                        $info_error['current'] = 'float';
                        throw new Exception($name_template . ' precisa ser float porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                    } else {
                        $value = ($value == (int)$value) ? (int)$value : (float)$value;

                        if (!is_numeric($value)) {
                            $info_error['expect'] = gettype($value);
                            $info_error['current'] = 'float';
                            throw new Exception($name_template . ' precisa ser float porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                        }
                    }

                    break;
                case 'mixed':
                    if (!is_numeric($value) && !is_string($value)) {
                        $info_error['expect'] = gettype($value);
                        $info_error['current'] = 'number or string';
                        throw new Exception($name_template . ' precisa ser mixed porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                    }
                    break;
                case 'array':

                    //Caso seja vazia coloca valor default
                    if (empty($value) && isset($reqs['default'])) {
                        $value = json_decode($reqs['default']);
                        return true;
                    }

                    if (!is_array($value)) {
                        $info_error['expect'] = gettype($value);
                        $info_error['current'] = 'array';
                        throw new Exception($name_template . ' precisa ser array porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                    }
                    break;
                case 'object':

                    //Caso seja vazia coloca valor default
                    if (empty($value) && isset($reqs['default'])) {
                        $value = json_decode($reqs['default']);
                    } else {
                        if (!is_object($value) && !\DS\Helper::is_array_assoc($value)) {
                            throw new Exception($name_template . ' precisa ser object porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                        }
                    }

                    if (\DS\Helper::is_array_assoc($value)) {
                        $value = json_decode(json_encode($value));
                    }

                    //Objetos não precisam de tratamento, por enquanto
                    return;
                    break;
            }
        }

        //Verifica dados de uma array
        if ($reqs['type'] == 'array') {
            foreach ($value as $element_key => &$element_value) {

                switch ($reqs['items']['type']) {
                    case 'string':
                        if (!is_string($element_value) && !is_numeric($element_value)) {
                            $info_error['expect'] = gettype($value);
                            $info_error['current'] = 'string';
                            throw new Exception('DATA ERRO: É requerido string porém é ' . gettype($element_value), ErrorCode::TYPE_ERROR, $info_error);
                        }

                        if (is_numeric($element_value)) {
                            $element_value = (string)$element_value;
                        }

                        break;

                    case 'number':
                        if (!is_numeric($element_value)) {
                            $info_error['expect'] = gettype($value);
                            $info_error['current'] = 'number';
                            throw new Exception('DATA ERRO: É requerido number porém é ' . gettype($element_value), ErrorCode::TYPE_ERROR, $info_error);
                        } else {
                            $element_value = ($element_value == (int)$element_value) ? (int)$element_value : (float)$element_value;
                        }
                        break;

                    case 'int':
                        if (!is_numeric($element_value)) {
                            $info_error['expect'] = gettype($value);
                            $info_error['current'] = 'int';
                            throw new Exception('DATA ERRO: É requerido int porém é ' . gettype($element_value), ErrorCode::TYPE_ERROR, $info_error);
                        } else {

                            $element_value = ($element_value == (int)$element_value) ? (int)$element_value : (float)$element_value;

                            if (!is_int($element_value)) {
                                $info_error['expect'] = gettype($value);
                                $info_error['current'] = 'int';
                                throw new Exception('DATA ERRO: É requerido int porém é ' . gettype($element_value), ErrorCode::TYPE_ERROR, $info_error);
                            }

                        }

                        break;

                    case 'float':

                        if (!is_numeric($element_value)) {
                            $info_error['expect'] = gettype($value);
                            $info_error['current'] = 'float';
                            throw new Exception('DATA ERRO: É requerido float porém é ' . gettype($element_value), ErrorCode::TYPE_ERROR, $info_error);
                        } else {

                            $element_value = ($element_value == (int)$element_value) ? (int)$element_value : (float)$element_value;

                            if (!is_float($element_value)) {
                                $info_error['expect'] = gettype($value);
                                $info_error['current'] = 'float';
                                throw new Exception('DATA ERRO: É requerido float porém é ' . gettype($element_value), ErrorCode::TYPE_ERROR, $info_error);
                            }
                        }

                        break;

                    case 'mixed':
                        if (!is_numeric($element_value) && !is_string($element_value)) {
                            $info_error['expect'] = gettype($value);
                            $info_error['current'] = 'string/number';
                            throw new Exception('DATA ERRO: É requerido mixed porém é ' . gettype($element_value), ErrorCode::TYPE_ERROR, $info_error);
                        }
                        break;

                    case 'array':
                        if (!is_array($element_value)) {
                            $info_error['expect'] = gettype($value);
                            $info_error['current'] = 'array';
                            throw new Exception('DATA ERRO: É requerido Array porém é ' . gettype($element_value), ErrorCode::TYPE_ERROR, $info_error);
                        }
                        break;
                }

                if ($reqs['items']['type'] == 'object') {
                    foreach ($element_value as $sub_key => $sub_item) {
                        if (isset($template[$path . '.' . $sub_key])) {
                            $sub_path = $path . '[' . $element_key . '].' . $sub_key;
                            Validator::validateItem($reqs['items']['items'][$sub_key], $sub_item, $array, $sub_path);
                        } else {
                            if (!self::$additionalProperties['.' . $path]) {
                                unset($element_value->{$sub_key});
                            }
                        }
                    }

                } else {
                    $path = $path . '[' . $element_key . ']';
                    Validator::validateItem($reqs['items'], $element_value, $array, $path);
                }
            }
        }


        //Tamanho Máximo
        if (isset($reqs['validate']['max-size'])) {
            switch ($reqs['type']) {
                case 'string':
                    if (strlen($value) > $reqs['validate']['max-size']) {
                        $info_error['expect'] = $reqs['validate']['max-size'];
                        $info_error['current'] = strlen($value);
                        throw new Exception('DATA ERRO: Excedeu o valor máximo - ' . strlen($value) . '/' . $reqs['validate']['max-size'], ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;

                case 'number':
                case 'float':
                case 'int':

                    if ($value > $reqs['validate']['max-size']) {
                        $info_error['expect'] = $reqs['validate']['max-size'];
                        $info_error['current'] = $value;
                        throw new Exception('DATA ERRO: Excedeu o valor máximo - ' . $value . '/' . $reqs['validate']['max-size'], ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;

                case 'mixed':
                    if (strlen($value) > $reqs['validate']['max-size']) {
                        $info_error['expect'] = $reqs['validate']['max-size'];
                        $info_error['current'] = strlen($value);

                        throw new Exception('DATA ERRO: Excedeu o valor máximo - ' . strlen($value) . '/' . $reqs['validate']['max-size'], ErrorCode::RANGE_MAX, $info_error);
                    }

                    if ($value > $reqs['validate']['max-size']) {
                        $info_error['expect'] = $reqs['validate']['max-size'];
                        $info_error['current'] = $value;
                        throw new Exception('DATA ERRO: Excedeu o valor máximo - ' . $value . '/' . $reqs['validate']['max-size'], ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;

                case 'array':
                    if (count($value) > $reqs['validate']['max-size']) {
                        $info_error['expect'] = $reqs['validate']['max-size'];
                        $info_error['current'] = count($value);

                        throw new Exception('DATA ERRO: Excedeu o valor máximo - ' . count($value) . '/' . $reqs['validate']['max-size'], ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;
            }
        }

        //Tamanho Máximo de caracters apenas para string e numbers
        if (isset($reqs['validate']['max-length'])) {
            switch ($reqs['type']) {
                case 'number':
                case 'mixed':
                case 'float':
                case 'int':
                case 'string':
                    if (strlen((string)$value) > $reqs['validate']['max-length']) {
                        $info_error['expect'] = $reqs['validate']['max-length'];
                        $info_error['current'] = strlen($value);

                        throw new Exception($name_template . ' só pode ter ' . $reqs['validate']['max-length'] . ' caracteres, porém tem ' . strlen($value), ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;
            }
        }

        //Tamanho Minimo
        if (isset($reqs['validate']['min-size'])) {
            switch ($reqs['type']) {
                case 'string':
                    if (strlen($value) < $reqs['validate']['min-size']) {
                        $info_error['expect'] = $reqs['validate']['min-size'];
                        $info_error['current'] = strlen($value);
                        throw new Exception('DATA ERRO: Menor que o valor mínimo - ' . strlen($value) . '/' . $reqs['validate']['min-size'], ErrorCode::RANGE_MIN, $info_error);
                    }
                    break;

                case 'number':
                case 'float':
                case 'int':
                    if ($value < $reqs['validate']['min-size']) {
                        $info_error['expect'] = $reqs['validate']['min-size'];
                        $info_error['current'] = $value;
                        throw new Exception('DATA ERRO: Menor que o valor mínimo - ' . $value . '/' . $reqs['validate']['min-size'], ErrorCode::RANGE_MIN, $info_error);
                    }
                    break;

                case 'mixed':
                    if (strlen($value) < $reqs['validate']['min-size']) {
                        $info_error['expect'] = $reqs['validate']['min-size'];
                        $info_error['current'] = strlen($value);

                        throw new Exception('DATA ERRO: Menor que o valor mínimo - ' . strlen($value) . '/' . $reqs['validate']['min-size'], ErrorCode::RANGE_MIN, $info_error);
                    }

                    if ($value > $reqs['validate']['min-size']) {
                        $info_error['expect'] = $reqs['validate']['min-size'];
                        $info_error['current'] = $value;
                        throw new Exception('DATA ERRO: Menor que o valor mínimo - ' . $value . '/' . $reqs['validate']['min-size'], ErrorCode::RANGE_MIN, $info_error);
                    }
                    break;

                case 'array':
                    if (count($value) < $reqs['validate']['min-size']) {
                        $info_error['expect'] = $reqs['validate']['min-size'];
                        $info_error['current'] = count($value);
                        throw new Exception('DATA ERRO: Menor que o valor mínimo - ' . count($value) . '/' . $reqs['validate']['min-size'], ErrorCode::RANGE_MIN, $info_error);
                    }
                    break;
            }
        }

        //Tamanho Minimo de caracters apenas para string e numbers
        if (isset($reqs['validate']['min-length'])) {
            switch ($reqs['type']) {
                case 'number':
                case 'mixed':
                case 'float':
                case 'int':
                case 'string':
                    if (strlen((string)$value) < $reqs['validate']['min-length']) {
                        $info_error['expect'] = $reqs['validate']['min-length'];
                        $info_error['current'] = strlen($value);

                        throw new Exception($name_template . ' só pode ter ' . $reqs['validate']['min-length'] . ' caracteres, porém tem ' . strlen($value), ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;
            }
        }

        //Email
        if (isset($reqs['validate']['email'])) {
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                throw new Exception('DATA ERRO: Email Inválido', ErrorCode::INVALID_EMAIL, $info_error);
            }
        }

        //CPF
        if (isset($reqs['validate']['cpf'])) {
            if (!Validator::valideCPF($value)) {
                throw new Exception('DATA ERRO: CPF Inválido', ErrorCode::INVALID_CPF, $info_error);
            }
        }

        //CPF
        if (isset($reqs['validate']['cnjp'])) {
            if (!Validator::valideCNPJ($value)) {
                throw new Exception('DATA ERRO: CPF Inválido', ErrorCode::INVALID_CPF, $info_error);
            }
        }

        //CEP
        if (isset($reqs['validate']['cep'])) {
            if (!Validator::valideCEP($value)) {
                throw new Exception('DATA ERRO: CEP Inválido', ErrorCode::INVALID_CEP, $info_error);
            }
        }

        //Igual outro campo
        if (isset($reqs['validate']['same'])) {
            if ($array[$reqs['validate']['same']]['value'] !== $value) {
                throw new Exception('DATA ERRO: Não é igual a key ' . $reqs['validate']['same'], ErrorCode::INVALID_SAME, $info_error);
            }
        }

        /**
         * Tratamento da Variável
         */
        if (!empty($reqs['tratament'])) {
            foreach ($reqs['tratament'] as $tratament) {
                switch ($tratament) {
                    //UpperCase
                    case 'uppercase':
                        $value = strtoupper($value);
                        break;

                    //LowerCase
                    case 'lowercase':
                        $value = strtolower($value);
                        break;

                    //Remove qualquer coisa que não seja número
                    case 'lowercase':
                        $value = preg_replace('/[^0-9]/', '', (string)$value);
                        break;

                    //Remove qualquer coisa que não seja número
                    case 'number':
                        $value = preg_replace('/[^0-9]/', '', (string)$value);
                        break;

                    //Mascara de CEP
                    case 'cep':
                        if (isset($reqs['validate']['cep'])) {
                            $value = \DS\Helper::mask("#####-###", $value);
                        }
                        break;

                    //Mascara de CEP
                    case 'cpf':
                        if (isset($reqs['validate']['cep'])) {
                            $value = \DS\Helper::maskCPF($value);
                        }
                        break;

                    //Remove qualquer coisa que não seja número
                    case 'number':
                        $value = preg_replace('/[^0-9]/', '', (string)$value);
                        break;

                    //Shae1
                    case 'sha1':
                        $value = sha1($value);
                        break;

                    //MD5
                    case 'md5':
                        $value = md5($value);
                        break;
                }
            }
        }

        /**
         * Allowed Data
         * Valores permitidos
         */
        if (!empty($reqs['allowed'])) {
            $allow = false;
            foreach ($reqs['allowed'] as $allowed) {
                if ($allowed == $value) {
                    $allow = true;
                }
            }

            if (!$allow) {
                $info_error['expect'] = implode(', ', $reqs['allowed']);
                $info_error['current'] = $value;
                throw new Exception('DATA ERRO: Valor não permito, valores permito: ' . implode(', ', $reqs['allowed']), ErrorCode::NOT_ALLOWED_DATA, $info_error);
            }
        }

    }

    public static function returnArray($element)
    {
        $final_array = [];

        foreach ($element as $key => $value) {
            $route = explode('.', $key);
            Validator::getItemArray($key, $value, $final_array);
        }
        return $final_array;
    }

    public static function getItemArray($key, $value, &$element)
    {
        $route = explode('.', $key);

        $c_element = &$element[$route[0]];

        if (!isset($element[$route[0]])) {
            $c_element = [];
        }

        array_splice($route, 0, 1);

        foreach ($route as $name) {
            if (!isset($c_element[$name])) {
                $c_element[$name] = [];
            }

            $c_element = &$c_element[$name];
        }

        $c_element = $value;
    }

    public static function normalizeArray($element, $parent = [], $ignore = false)
    {
        $final_array = [];

        $validate = (array)$element;
        foreach ($validate as $key => $value) {
            $c_parent = $parent;
            $c_parent[] = $key;

            if (is_object($value) || \DS\Helper::is_array_assoc($value)) {
                $final_array = array_merge($final_array, Validator::normalizeArray($value, $c_parent));
            } else {
                $final_array[implode('.', $c_parent)] = [
                    'value' => $value,
                    '__parent__' => '.' . implode('.', $parent)
                ];

                //Ignorar na array
                if ($ignore) {
                    $final_array[implode('.', $c_parent)]['ignore'] = true;
                }

                if (is_array($value)) {
                    if (is_object($value[0])) {
                        $final_array = array_merge($final_array, Validator::normalizeArray($value[0], $c_parent, true));
                    }
                }
            }
        }
        return $final_array;
    }

    /**
     * Verifica quais valores são obrigatórios
     * @param  [type] $element [description]
     * @param  array $parent [description]
     * @return [type]          [description]
     */
    public static function getRequired($element, $parent = [])
    {
        $final_array = [];

        foreach ($element as $key => $value) {

            $c_parent = $parent;
            //Ignore tag $root
            if ($key !== '$root') {
                $c_parent[] = $key;
            }

            switch (@$value['type']) {
                case 'object':
                    if (isset($value['required'])) {
                        foreach ($value['required'] as $requireds) {
                            $required = implode('.', $c_parent);
                            $final_array[] = $required ? $required . '.' . $requireds : $requireds;
                        }
                    }

                    $final_array = array_merge($final_array, Validator::getRequired($value['items'], $c_parent));
                    break;
            }
        }

        return $final_array;
    }

    /**
     * Configura template para verifação das variáveis
     * @param  [type] $element [description]
     * @param  array $parent [description]
     * @return [type]          [description]
     */
    public static function normalizeTemplate($element, $parent = [], $ignore = false)
    {
        $final_array = [];

        foreach ($element as $key => $value) {

            $c_parent = $parent;
            //Ignore tag $root
            if ($key !== '$root') {
                $c_parent[] = $key;
            }

            switch (@$value['type']) {
                case 'object':
                    //Guarda additionalProperties de objetos
                    self::$additionalProperties['.' . implode('.', $c_parent)] = $value['additionalProperties'] ?? false;

                    //Adiciona Object no template, mas remove items
                    if (!empty($c_parent)) {
                        $final_array[implode('.', $c_parent)] = $value;
                        if (isset($final_array[implode('.', $c_parent)]['items'])) {
                            unset($final_array[implode('.', $c_parent)]['items']);
                        }
                    }

                    $final_array = array_merge($final_array, Validator::normalizeTemplate($value['items'], $c_parent));

                    break;

                case 'array':
                    if ($value['items']['type'] == 'object') {
                        $final_array[implode('.', $c_parent)] = $value;

                        self::$additionalProperties['.' . implode('.', $c_parent)] = $value['items']['additionalProperties'] ?? false;

                        $final_array = array_merge($final_array, Validator::normalizeTemplate($value['items']['items'], $c_parent, true));
                        break;
                    }

                case 'string':
                case 'number':
                case 'int':
                case 'float':
                case 'mixed':


                    if ($ignore) $value['ignore'] = true;

                    $final_array[implode('.', $c_parent)] = $value;
                    break;
            }
        }

        return $final_array;
    }

    public static function valideCEP($cep)
    {
        $cep = preg_replace('/[^0-9]/is', '', $cep);

        if (strlen($cep) == 8) {
            return true;
        } else {
            return false;
        }
    }

    public static function valideCPF($cpf)
    {
        $cpf = preg_replace('/[^0-9]/is', '', $cpf);

        if (strlen($cpf) != 11) {
            return false;
        }

        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }

    public static function valideCNPJ($cnpj)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string)$cnpj);
        // Valida tamanho
        if (strlen($cnpj) != 14)
            return false;

        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
            return false;

        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }

    public static function normalizeTemplate2($element, $parent = [])
    {

    }
}
