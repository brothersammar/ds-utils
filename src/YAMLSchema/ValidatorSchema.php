<?php

namespace DS\YAMLSchema;

/**
 * ValidatorSchema
 */

class ValidatorSchema
{
    /**
     * Propriedades Adicionais
     * @var array
     */
    protected $additionalProperties = [];

    /**
     * Template do Schema
     * @var array
     */
    protected $template = [];
    protected $template_normalized = [];

    /**
     * Valores obrigatórios
     * @var array
     */
    protected $required = [];
    protected $values_missing = [];

    /**
     * Dados que precisam ser tratados
     * @var array
     */
    protected $data = [];

    /**
     * Dados tradados
     * @var array
     */
    protected $new_array = [];

    /**
     * Dados tradados - normalizados
     * @var array
     */
    protected $new_array_normalized = [];

    /**
     * Opções
     * @var array
     */
    protected $options = [];

    /**
     *
     */
    function __construct($schema, &$array, $options = [], &$rest_data = [])
    {
        /**
         * Load Schema
         */
        if ($file = @file_get_contents($schema)) {
            $YAML = \yaml_parse($file);
        } else {
            $YAML = \yaml_parse($schema);
        }

        $this->options = $options;

        //Objects que aceitam valores adicionais
        $this->additionalProperties = $this->normalizeAP($YAML['$root']);

        //Template do Schema normalizado
        $this->template_normalized = $this->normalizeTemplate($YAML['$root']);
        // $this->template_normalized = $this->getTemplate($YAML['$root']);

        //Valores obrigatórios
        $this->required = $this->getRequired($YAML['$root']);
        $this->required_all = $this->getRequiredAll($YAML['$root']);

        //Dados Tratados para serem faceis de serem verificados
        $this->array = $this->getArray($array);
    }

    /**
     * Valida Entrada
     * @param  [type] $array    [description]
     * @param  [type] $template [description]
     * @param  [type] $required [description]
     * @return [type]           [description]
     */
    public function validate()
    {

        $errors = [];

        foreach ($this->array as $key => $value) {
            try {
                $_value = $this->validateItem($value);
                if($_value === NULL) continue;
                $this->new_array[$key] = $_value;
            } catch (Exception $e) {
                $errors[] = [
                    'name' => $template[$key]['name'] ?? $key,
                    'path' => $key,
                    'code' => $e->getCode(),
                    'message' => $e->getMessage(),
                    'body' => $e->getBody(),
                ];
            }
        }

        /**
         * Caso exista erro no formato dos dados
         */
        if (!empty($errors)) {
            throw new Exception($errors[0]['message'], $errors[0]['code'], $errors);
        }

        $this->normalizeArray($this->new_array);

        //Ignora requireds
        $this->options['ignore_required'] = $this->options['ignore_required'] ?? false;

        if (!$this->options['ignore_required']) {
            $this->checkRequired($this->required);

            if (isset($this->options['set_all_required']) && $this->options['set_all_required']) {
                $this->checkRequired($this->required_all);
            }
        }

        //Verifica se existem valores faltando
        if (!empty($this->values_missing)) {
            $this->values_missing = array_unique($this->values_missing);

            //Primeiro Erro
            $errors[] = [
                'name' => $this->values_missing[0],
                'path' => $this->values_missing[0],
                'code' => ErrorCode::ERROR_REQUIRED,
                'message' => $this->values_missing[0] . ' é obrigatórido.'
            ];

            throw new Exception('Valores obrigatórios faltando: ' . implode(',', $this->values_missing), ErrorCode::ERROR_REQUIRED, $errors);
        }


        return $this->new_array;

    }

    public function checkRequired($values, $_key = '', $template_path = '')
    {

        if (isset($values['$type'])) {
            if ($values['$type'] == '$array') {

                if ($this->checkValueExist($_key, $_key, 'array')) {
                    $this->checkRequired($values['items'], $_key . '[]', $template_path . '[]');
                }

            }

            return false;
        }

        foreach ($values as $key => $value) {
            switch ($value['$type']) {
                case '$object':
                    if ($this->checkValueExist($_key . '.' . $key, $_key . '.' . $key, 'object')) {
                        $this->checkRequired($value['items'], $_key . '.' . $key, $template_path . '.' . $key);
                    }
                    break;
                case '$array':
                    if ($this->checkValueExist($_key . '.' . $key, $_key . '.' . $key, 'array')) {
                        $this->checkRequired($value['items'], $_key . '.' . $key . '[]', $template_path . '[]');
                    }

                    break;
                case '$item':
                    $object = $_key;
                    // $_template_path = '.'.$template_path.'.'.$key;

                    if ($key == '$item') {
                        $item = $_key;
                        $this->checkValueExist($object, $item, 'array');
                    } else {
                        $item = $_key . '.' . $key;
                        $this->checkValueExist($object, $item, '', $key);
                    }

                    // echo '<pre>'; print_r($this->new_array_normalized); echo '</pre>';

                    break;
            }
        }
    }

    public function checkValueExist($object, $item, $type = '', $key = '')
    {
        $find = true;
        switch ($type) {
            case 'object':
            case 'array':
                //Adiciona valores que estão faltando na array
                if (!isset($this->new_array_normalized[$object])) {
                    $this->values_missing[] = $this->template_normalized[$object]['name'] ?? $item;
                    $find = false;
                }
                break;

            default:

                if (isset($this->new_array_normalized[$object]) && !empty($object)) {
                    if (is_array($this->new_array_normalized[$object])) {
                        //Busca valores dentro do object para ver se estão faltando
                        foreach ($this->new_array_normalized[$object] as $_objects) {
                            if (!isset($_objects->{$key})) {
                                $find = false;
                                //Adiciona valores que estão faltando na array
                                $this->values_missing[] = $this->template_normalized[$object]['name'] ?? $item;
                            }
                        }
                    } else {
                        if (!isset($this->new_array_normalized[$object]->{$key})) {
                            $find = false;
                            //Adiciona valores que estão faltando na array
                            $this->values_missing[] = $this->template_normalized[$object]['name'] ?? $item;
                        }
                    }
                } else {

                    if (!isset($this->new_array_normalized[$item])) {
                        $find = false;
                        //Adiciona valores que estão faltando na array
                        $this->values_missing[] = $this->template_normalized[$item]['name'] ?? $item;
                    }
                }

                break;
        }

        return $find;
    }

    /**
     * Valida Todos Items
     * @param  [type] $reqs  [description]
     * @param  [type] $value [description]
     * @param  [type] $array [description]
     * @param  [type] $path  [description]
     * @return [type]        [description]
     */
    public function validateItem($item)
    {
        $value = $this->checkType($item);

        if($value === NULL) {
            return $value;
        }

        //Loop
        switch ($item['template']['type']) {

            // Para caso de CEP, Numero de casas, onde é necessário ser uma string porem, não deve ser removidos os zeros iniciais
            case 'string-number':
            case 'string':
            case 'number':
            case 'bool':
            case 'boolean':
            case 'int':
            case 'float':
            case 'mixed':
                $item['value'] = $value;
                $value = $this->checkItem($item);

                break;

            case 'array':
                $item['value'] = $value;
                $value = $this->checkItem($item);

                $array = [];
                foreach ($value as $item_key => $item_value) {
                    $_value = $this->validateItem($item_value);
                    if($_value === NULL) continue;
                    // echo '<pre>'; print_r($item_value); echo '</pre>';
                    $array[] = $_value;
                }
                $value = $array;

                break;

            case 'object':
                // $value = $this->checkItem($item);

                $object = [];

                foreach ($value as $item_key => $item_value) {
                    $_value = $this->validateItem($item_value);
                    if($_value === NULL) continue;
                    $object[$item_key] = $_value;
                }

                //Transforma em object
                return (object)json_decode(json_encode($object));

                break;
        }


        return $value;
    }

    public function checkType($item)
    {
        $value = $item['value'];
        $name_template = $item['name'];

        //Erros
        $info_error = [
            'name' => $name_template
        ];

        switch ($item['template']['type']) {
            case 'string-number':

                if($value === NULL) {
                    return NULL;
                }

                if (!is_numeric($value) && !empty($value)) {
                    //Convert em String
                    $info_error['expect'] = gettype($value);
                    $info_error['current'] = 'string-number';
                    throw new Exception($name_template . ' precisa ser number porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                }

                $value = (string)$value;

                break;

            case 'string':

                if($value === NULL) {
                    return NULL;
                }

                if (!is_string($value) && !is_numeric($value)) {
                    //Convert em String
                    $info_error['expect'] = gettype($value);
                    $info_error['current'] = 'string';
                    throw new Exception($name_template . ' precisa ser string porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                }

                if (is_numeric($value)) {
                    $value = (string)$value;
                }

                break;

            case 'number':

                //Caso seja vazia coloca valor default
                if (empty($value) && isset($item['template']['default'])) {
                    $value = $item['template']['default'];
                    return $value;
                }

                if($value === NULL) {
                    return NULL;
                }

                if (!is_numeric($value)) {
                    $info_error['expect'] = gettype($value);
                    $info_error['current'] = 'number';
                    throw new Exception($name_template . ' precisa ser number porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                } else {
                    $value = ($value == (int)$value) ? (int)$value : (float)$value;
                }

                break;

            case 'int':

                //Caso seja vazia coloca valor default
                if (empty($value) && isset($item['template']['default'])) {
                    $value = $item['template']['default'];
                    return $value;
                }

                if($value === NULL) {
                    return NULL;
                }

                if (!is_numeric($value)) {
                    $info_error['expect'] = gettype($value);
                    $info_error['current'] = 'int';
                    throw new Exception($name_template . ' precisa ser int porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                } else {
                    $value = ($value == (int)$value) ? (int)$value : (float)$value;

                    if (!is_int($value)) {
                        $info_error['expect'] = gettype($value);
                        $info_error['current'] = 'int';
                        throw new Exception($name_template . ' precisa ser int porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                    }
                }
                break;

            case 'float':
                //Caso seja vazia coloca valor default
                if (empty($value) && isset($item['template']['default'])) {
                    $value = $item['template']['default'];
                    return $value;
                }

                if($value === NULL) {
                    return NULL;
                }

                if (!is_numeric($value)) {
                    $info_error['expect'] = gettype($value);
                    $info_error['current'] = 'float';
                    throw new Exception($name_template . ' precisa ser float porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                } else {
                    $value = ($value == (int)$value) ? (int)$value : (float)$value;

                    if (!is_numeric($value)) {
                        $info_error['expect'] = gettype($value);
                        $info_error['current'] = 'float';
                        throw new Exception($name_template . ' precisa ser float porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                    }
                }

                break;

            case 'mixed':

                if($value === NULL) {
                    return NULL;
                }

                if (!is_numeric($value) && !is_string($value)) {
                    $info_error['expect'] = gettype($value);
                    $info_error['current'] = 'number or string';
                    throw new Exception($name_template . ' precisa ser mixed porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                }
                break;

            case 'bool':
            case 'boolean':
                // Caso seja vazia coloca valor default
                if (empty($value) && isset($item['template']['default'])) {
                    $value = boolval($item['template']['default']);
                    return $value;
                }

                if($value === NULL) {
                    return NULL;
                }

                if (!is_numeric($value) && !is_bool($value)) {
                    $info_error['expect'] = gettype($value);
                    $info_error['current'] = 'boolean';
                    throw new Exception($name_template . ' precisa ser boolean porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                } else {
                    $value = boolval($value);
                }

                break;

            case 'array':

                //Caso seja vazia coloca valor default
                if (empty($value) && isset($item['template']['default'])) {
                    if(is_string($item['template']['default'])) {
                        $value = json_decode($item['template']['default'], TRUE);
                    } else {
                        $value = $item['template']['default'];
                    }
                    return $value;
                }

                if($value === NULL) {
                    return NULL;
                }

                if (!is_array($value)) {
                    $info_error['expect'] = gettype($value);
                    $info_error['current'] = 'array';
                    throw new Exception($name_template . ' precisa ser array porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                }
                break;

            case 'object':

                if($value === NULL) {
                    return NULL;
                }

                //Caso seja vazia coloca valor default
                if (empty($value) && isset($item['template']['default'])) {
                    $value = json_decode($item['template']['default']);
                } else {
                    if (!is_object($value) && !\DS\Helper::is_array_assoc($value)) {
                        throw new Exception($name_template . ' precisa ser object porém é ' . gettype($value), ErrorCode::TYPE_ERROR, $info_error);
                    }
                }

                // if(\DS\Helper::is_array_assoc($value)){
                // 	$value = json_decode(json_encode($value));
                // }

                break;
        }


        return $value;
    }

    public function checkItem($item)
    {
        $value = $item['value'];
        $name_template = $item['name'];

        //Erros
        $info_error = [
            'name' => $name_template
        ];

        //Tamanho Máximo
        if (isset($item['template']['validate']['max-size'])) {
            switch ($item['template']['type']) {
                case 'string':
                    if (strlen($value) > $item['template']['validate']['max-size']) {
                        $info_error['expect'] = $item['template']['validate']['max-size'];
                        $info_error['current'] = strlen($value);
                        throw new Exception('DATA ERRO: Excedeu o valor máximo - ' . strlen($value) . '/' . $item['template']['validate']['max-size'], ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;

                case 'number':
                case 'float':
                case 'int':

                    if ($value > $item['template']['validate']['max-size']) {
                        $info_error['expect'] = $item['template']['validate']['max-size'];
                        $info_error['current'] = $value;
                        throw new Exception('DATA ERRO: Excedeu o valor máximo - ' . $value . '/' . $item['template']['validate']['max-size'], ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;

                case 'mixed':
                    if (strlen($value) > $item['template']['validate']['max-size']) {
                        $info_error['expect'] = $item['template']['validate']['max-size'];
                        $info_error['current'] = strlen($value);

                        throw new Exception('DATA ERRO: Excedeu o valor máximo - ' . strlen($value) . '/' . $item['template']['validate']['max-size'], ErrorCode::RANGE_MAX, $info_error);
                    }

                    if ($value > $item['template']['validate']['max-size']) {
                        $info_error['expect'] = $item['template']['validate']['max-size'];
                        $info_error['current'] = $value;
                        throw new Exception('DATA ERRO: Excedeu o valor máximo - ' . $value . '/' . $item['template']['validate']['max-size'], ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;

                case 'array':
                    if (count($value) > $item['template']['validate']['max-size']) {
                        $info_error['expect'] = $item['template']['validate']['max-size'];
                        $info_error['current'] = count($value);

                        throw new Exception('DATA ERRO: Excedeu o valor máximo - ' . count($value) . '/' . $item['template']['validate']['max-size'], ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;
            }
        }

        //Tamanho Máximo de caracters apenas para string e numbers
        if (isset($item['template']['validate']['max-length'])) {
            switch ($item['template']['type']) {
                case 'number':
                case 'mixed':
                case 'float':
                case 'int':
                case 'string':
                    if (strlen((string)$value) > $item['template']['validate']['max-length']) {
                        $info_error['expect'] = $item['template']['validate']['max-length'];
                        $info_error['current'] = strlen($value);

                        throw new Exception($name_template . ' só pode ter ' . $item['template']['validate']['max-length'] . ' caracteres, porém há ' . strlen($value), ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;
            }
        }

        //Tamanho Minimo
        if (isset($item['template']['validate']['min-size'])) {
            switch ($item['template']['type']) {
                case 'string':
                    if (strlen($value) < $item['template']['validate']['min-size']) {
                        $info_error['expect'] = $item['template']['validate']['min-size'];
                        $info_error['current'] = strlen($value);

                        throw new Exception($name_template . ' precisa ser maior que ' . $item['template']['validate']['min-size'] . ', porém é ' . strlen($value), ErrorCode::RANGE_MIN, $info_error);
                    }
                    break;

                case 'number':
                case 'float':
                case 'int':
                    if ($value < $item['template']['validate']['min-size']) {
                        $info_error['expect'] = $item['template']['validate']['min-size'];
                        $info_error['current'] = $value;
                        throw new Exception($name_template . ' precisa ser maior que ' . $item['template']['validate']['min-size'] . ', porém é ' . $value, ErrorCode::RANGE_MIN, $info_error);
                    }
                    break;

                case 'mixed':
                    if (strlen($value) < $item['template']['validate']['min-size']) {
                        $info_error['expect'] = $item['template']['validate']['min-size'];
                        $info_error['current'] = strlen($value);

                        throw new Exception($name_template . ' precisa ser maior que ' . $item['template']['validate']['min-size'] . ', porém é ' . strlen($value), ErrorCode::RANGE_MIN, $info_error);
                    }

                    if ($value > $item['template']['validate']['min-size']) {
                        $info_error['expect'] = $item['template']['validate']['min-size'];
                        $info_error['current'] = $value;
                        throw new Exception($name_template . ' precisa ser maior que ' . $item['template']['validate']['min-size'] . ', porém é ' . $value, ErrorCode::RANGE_MIN, $info_error);
                    }
                    break;

                case 'array':
                    if (count($value) < $item['template']['validate']['min-size']) {
                        $info_error['expect'] = $item['template']['validate']['min-size'];
                        $info_error['current'] = count($value);
                        throw new Exception($name_template . ' precisa ser maior que ' . $item['template']['validate']['min-size'] . ', porém é ' . count($value), ErrorCode::RANGE_MIN, $info_error);
                    }
                    break;
            }
        }

        //Tamanho Minimo de caracters apenas para string e numbers
        if (isset($item['template']['validate']['min-length'])) {
            switch ($item['template']['type']) {
                case 'number':
                case 'mixed':
                case 'float':
                case 'int':
                case 'string':
                    if (strlen((string)$value) < $item['template']['validate']['min-length']) {
                        $info_error['expect'] = $item['template']['validate']['min-length'];
                        $info_error['current'] = strlen($value);

                        throw new Exception($name_template . ' precisa ter ' . $item['template']['validate']['min-length'] . ' caracteres, porém tem ' . strlen($value), ErrorCode::RANGE_MAX, $info_error);
                    }
                    break;
            }
        }


        //Email
        if (isset($item['template']['validate']['email'])) {
            if (!filter_var($value, FILTER_VALIDATE_EMAIL) && !empty($value)) {
                throw new Exception($name_template . ' não é um email válido', ErrorCode::INVALID_EMAIL, $info_error);
            }
        }

        //CPF
        if (isset($item['template']['validate']['cpf'])) {
            if (!$this->valideCPF($value) && !empty($value)) {
                throw new Exception($name_template . ' não é um CPF válido', ErrorCode::INVALID_CPF, $info_error);
            }
        }

        //CPF
        if (isset($item['template']['validate']['cnjp'])) {
            if (!$this->valideCNPJ($value) && !empty($value)) {
                throw new Exception($name_template . ' não é um CNPJ válido', ErrorCode::INVALID_CPF, $info_error);
            }
        }

        //CEP
        if (isset($item['template']['validate']['cep'])) {
            if (!$this->valideCEP($value) && !empty($value)) {
                throw new Exception($name_template . ' não é um CEP válido', ErrorCode::INVALID_CEP, $info_error);
            }
        }

        //PHONE
        if (isset($item['template']['validate']['phone'])) {
            if (!$this->validePhone($value) && !empty($value)) {
                throw new Exception($name_template . ' não é um Telefone válido', ErrorCode::INVALID_CEP, $info_error);
            }
        }

        //Igual outro campo
        // if(isset($item['template']['validate']['same'])){
        // 	if($array[$reqs['validate']['same']]['value'] !== $value){
        // 		throw new Exception.php($name_template.' não é um é igual a key '.$reqs['validate']['same'], ErrorCode::INVALID_SAME, $info_error);
        // 	}
        // }


        /**
         * Tratamento da Variável
         */
        if (!empty($item['template']['tratament'])) {
            foreach ($item['template']['tratament'] as $tratament) {
                switch ($tratament) {
                    //UpperCase
                    case 'uppercase':
                        $value = strtoupper($value);
                        break;

                    //LowerCase
                    case 'lowercase':
                        $value = strtolower($value);
                        break;

                    //Remove qualquer coisa que não seja número
                    case 'number':
                        if (is_bool($value)) {
                            $value = $value ? 1 : 0;
                        } else {
                            $value = preg_replace('/[^0-9]/', '', (string)$value);
                        }
                        break;

                    //Mascara de CEP
                    case 'cep':
                        if (isset($item['template']['validate']['cep'])) {
                            $value = \DS\Helper::mask("#####-###", $value);
                        }
                        break;

                    //Mascara de CEP
                    case 'cpf':
                        if (isset($item['template']['validate']['cep'])) {
                            $value = \DS\Helper::maskCPF($value);
                        }
                        break;

                    //Shae1
                    case 'sha1':
                        $value = sha1($value);
                        break;

                    //MD5
                    case 'md5':
                        $value = md5($value);
                        break;
                }
            }
        }

        if (!empty($item['template']['allowed'])) {
            $allow = false;
            foreach ($item['template']['allowed'] as $allowed) {
                if ($allowed == $value) {
                    $allow = true;
                }
            }

            if (!$allow) {
                $info_error['expect'] = implode(', ', $item['template']['allowed']);
                $info_error['current'] = $value;
                throw new Exception('DATA ERRO: Valor não permito, valores permito: ' . implode(', ', $item['template']['allowed']), ErrorCode::NOT_ALLOWED_DATA, $info_error);
            }
        }

        return $value;
    }

    /**
     * Verifica quais valores são obrigatórios
     */
    public function getRequired($item, $key = '')
    {
        $required = [];

        //Tipo do Item
        switch ($item['type']) {
            //
            case 'object':

                if (isset($item['required'])) {
                    foreach ($item['required'] as $_required) {
                        $required[$_required] = [
                            '$type' => '$item'
                        ];
                    }
                }

                foreach ($item['items'] as $item_key => $item_value) {
                    $_required = $this->getRequired($item_value);

                    if (!empty($_required)) {
                        if (isset($item_value['type']) && $item_value['type'] == 'object') {
                            $required[$item_key] = [
                                '$type' => '$object',
                                'items' => $_required
                            ];

                        } else {
                            $required[$item_key] = $_required;
                        }
                    }
                }

                break;

            case 'array':

                $_required = $this->getRequired($item['items']);

                if (!empty($_required)) {
                    $required = [
                        '$type' => '$array',
                        'items' => $_required
                    ];
                }

                break;
        }

        return $required;
    }

    /**
     * Verifica quais valores são obrigatórios
     */
    public function getRequiredAll($item, $key = '')
    {
        $required = [];

        //Tipo do Item
        switch ($item['type']) {
            //
            case 'object':

                foreach ($item['items'] as $item_key => $item_value) {
                    $_required = $this->getRequiredAll($item_value, $item_key);

                    if (!empty($_required)) {
                        if (isset($item_value['type']) && $item_value['type'] == 'object') {
                            // echo '<pre>'; print_r($item_value); echo '</pre>';
                            $required[$item_key] = [
                                '$type' => '$object',
                                'items' => $_required
                            ];

                        } else {
                            $required[$item_key] = $_required;
                        }
                    }
                }

                break;

            case 'array':

                $_required = $this->getRequiredAll($item['items']);

                if (!empty($_required)) {
                    //Array simples
                    if ($_required == ['$type' => '$item']) {
                        $required = [
                            '$type' => '$array',
                            'items' => [
                                '$item' => [
                                    '$type' => '$item'
                                ]
                            ]
                        ];
                    } else {
                        $required = [
                            '$type' => '$array',
                            'items' => $_required
                        ];
                    }
                }

                break;

            default:

                return [
                    '$type' => '$item'
                ];

                break;
        }

        return $required;
    }

    /**
     * Configura template para verifação das variáveis
     * @param  [type] $element [description]
     * @param  array $parent [description]
     * @return [type]          [description]
     */
    public function getTemplate($item, $key = '')
    {

        $template = [];
        //Tipo do Item
        switch ($item['type']) {
            //
            case 'object':

                if ($key != '') {
                    $template[$key] = $item;
                }

                foreach ($item['items'] as $item_key => $item_value) {
                    $template[$item_key] = $this->normalizeTemplate($item_value, $key . '.' . $item_key);
                }

                break;

            //
            case 'array':

                $template_array = $this->normalizeTemplate($item['items'], $key . '[]');
                $template[$key . '[]'] = $item;
                $template = array_merge($template, $template_array);

                break;

            //
            case 'string':
            case 'number':
            case 'int':
            case 'float':
            case 'mixed':
            case 'bool':
            case 'boolean':
                return $item;
                break;
        }

        return $template;
    }

    /**
     * Normaliza Template
     * @param  [type] $item [description]
     * @param  string $key [description]
     * @return [type]       [description]
     */
    public function normalizeTemplate($item, $key = '')
    {

        $template = [];
        //Tipo do Item
        switch ($item['type']) {
            //
            case 'object':

                if ($key != '') {
                    $template[$key] = $item;
                }

                foreach ($item['items'] as $item_key => $item_value) {
                    $_template = $this->normalizeTemplate($item_value, $key . '.' . $item_key);
                    $template = array_merge($template, $_template);
                }

                break;

            //
            case 'array':

                $template[$key] = $item;
                // $this->normalizeTemplate($item['items'], $key.'[]')
                // echo '<pre>'; print_r($this->normalizeTemplate($item['items'], $key.'[]')); echo '</pre>';

                $_template = $this->normalizeTemplate($item['items'], $key . '[]');

                $template = array_merge($template, $_template);

                break;

            //
            case 'string':
            case 'number':
            case 'int':
            case 'float':
            case 'mixed':
            case 'bool':
            case 'boolean':
                $template[$key] = $item;
                break;
        }

        return $template;
    }

    /**
     * Normaliza additionalProperties
     * @param  [type] $item [description]
     * @param  string $key [description]
     * @return [type]       [description]
     */
    public function normalizeAP($item, $key = '')
    {

        $additionalProperties = [];
        //Tipo do Item
        switch ($item['type']) {
            //
            case 'object':

                $additionalProperties[$key] = $item['additionalProperties'] ?? false;

                foreach ($item['items'] as $item_key => $item_value) {
                    $_additionalProperties = $this->normalizeAP($item_value, $key . '.' . $item_key);
                    $additionalProperties = array_merge($additionalProperties, $_additionalProperties);
                }

                break;

            //
            case 'array':

                $_additionalProperties = $this->normalizeAP($item['items'], $key . '[]');
                $additionalProperties = array_merge($additionalProperties, $_additionalProperties);

                break;
        }

        return $additionalProperties;
    }


    public function getArray($data, $key = '', $_full_path = '')
    {
        $_data = [];

        foreach ($data as $_key => $_value) {

            if (!is_numeric($_key)) {
                $full_path = $_full_path . '.' . $_key;
            } else {
                $full_path = $_full_path;
            }

            //Se não tiver no schema só adiciona se additionalProperties estiver ativado
            if (isset($this->template_normalized[$full_path]) || !empty($this->additionalProperties[$_full_path])) {

                if (is_object($_value) || \DS\Helper::is_array_assoc($_value)) {
                    $_data[$_key] = [
                        'name' => $this->template_normalized[$full_path]['name'] ?? $full_path,
                        'type' => 'object',
                        'path' => $key . '.' . $_key,
                        'full_path' => $full_path,
                        'template' => $this->template_normalized[$full_path] ?? false,
                        'value' => (object)$this->getArray($_value, $key . '.' . $_key, $full_path),
                    ];
                } else if (is_array($_value)) {
                    $_data[$_key] = [
                        'name' => $this->template_normalized[$full_path]['name'] ?? $full_path,
                        'type' => 'array',
                        'path' => $key . '.' . $_key,
                        'full_path' => $full_path,
                        'template' => $this->template_normalized[$full_path] ?? false,
                        'value' => $this->getArray($_value, $key . '.' . $_key, $full_path . '[]'),
                    ];
                } else {
                    $_data[$_key] = [
                        'name' => $this->template_normalized[$full_path]['name'] ?? $full_path,
                        'type' => 'item',
                        'path' => $key . '.' . $_key,
                        'full_path' => $full_path,
                        'value' => $_value,
                        'template' => $this->template_normalized[$full_path] ?? false,
                    ];
                }
            }
        }

        return $_data;
    }

    public function normalizeArray($data, $_key = '')
    {

        if (is_object($data) || \DS\Helper::is_array_assoc($data)) {

            if (isset($this->new_array_normalized[$_key])) {
                if (!is_array($this->new_array_normalized[$_key])) {
                    $this->new_array_normalized[$_key] = [$this->new_array_normalized[$_key]];
                }

                if (is_array($data)) {
                    $this->new_array_normalized[$_key] = array_merge($data, $this->new_array_normalized[$_key]);
                } else {
                    $this->new_array_normalized[$_key][] = $data;
                }

            } else {
                $this->new_array_normalized[$_key] = $data;
            }

            foreach ($data as $key => $value) {
                $this->normalizeArray($value, $_key . '.' . $key);

                if (isset($this->new_array_normalized[$_key . '.' . $key])) {

                    $this->new_array_normalized[$_key . '.' . $key] = (object)array_merge((array)$value, (array)$this->new_array_normalized[$_key . '.' . $key]);
                } else {
                    $this->new_array_normalized[$_key . '.' . $key] = $value;
                }
            }
        } else if (is_array($data)) {

            $array = [];

            foreach ($data as $value) {
                $array[] = $this->normalizeArray($value, $_key . '[]');
            }

            if (isset($this->new_array_normalized[$_key . '[]'])) {

                if (!is_array($this->new_array_normalized[$_key . '[]'])) {
                    $this->new_array_normalized[$_key . '[]'] = [$this->new_array_normalized[$_key . '[]']];
                }
                // $this->new_array_normalized[$_key.'[]'][] = $array;
                $this->new_array_normalized[$_key . '[]'] = array_merge($data, $this->new_array_normalized[$_key . '[]']);
            } else {
                $this->new_array_normalized[$_key . '[]'] = $data;
            }

            // $this->new_array_normalized[$_key] = $array;
        } else {
            return $data;
        }
    }

    public function valideCEP($cep)
    {
        $cep = preg_replace('/[^0-9]/is', '', $cep);

        if (strlen($cep) == 8) {
            return true;
        } else {
            return false;
        }
    }

    public function validePhone($phone)
    {
        $cep = preg_replace('/[^0-9]/is', '', $phone);

        if (strlen($cep) == 10 || strlen($cep) == 11) {
            return true;
        } else {
            return false;
        }
    }

    public function valideCPF($cpf)
    {
        $cpf = preg_replace('/[^0-9]/is', '', $cpf);

        if (strlen($cpf) != 11) {
            return false;
        }

        if (preg_match('/(\d)\1{10}/', $cpf)) {
            return false;
        }

        for ($t = 9; $t < 11; $t++) {
            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {
                return false;
            }
        }
        return true;
    }

    public function valideCNPJ($cnpj)
    {
        $cnpj = preg_replace('/[^0-9]/', '', (string)$cnpj);
        // Valida tamanho
        if (strlen($cnpj) != 14)
            return false;

        // Valida primeiro dígito verificador
        for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;
        if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
            return false;

        // Valida segundo dígito verificador
        for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++) {
            $soma += $cnpj{$i} * $j;
            $j = ($j == 2) ? 9 : $j - 1;
        }

        $resto = $soma % 11;

        return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
    }
}
