
# Estilo de código
/Users/albertoammar/.composer/vendor/bin/phpcs ./src

docker exec -ti apache bash -c "echo extension=yaml.so > /etc/php/7.1/apache2/php.ini"

Bagunça no código (pontos de refatoração, codigo não utilizado, código está legível)
/Users/albertoammar/.composer/vendor/bin/phpmd ./src html cleancode,unusedcode --reportfile phpmd-test.html

#PHPUnit Test
vendor/bin/phpunit --whitelist src/ --coverage-html cover/
vendor/bin/phpunit --configuration phpunit.xml

docker exec -ti apache /var/www/html/ds-projects/ds-utils/vendor/bin/phpunit --configuration /var/www/html/ds-projects/ds-utils/phpunit.xml

docker exec -ti apache phpunit --configuration /var/www/html/ds-projects/ds-utils/phpunit.xml --testdox-xml /var/www/html/ds-projects/ds-utils/cover/phpunitLog.xml

./convert.sh ./cover/phpunitLog.xml ./cover/phpunitLog.html

//Teste Direto no docker
docker exec -ti apache phpunit --configuration /var/www/html/ds-projects/ds-utils/phpunit.xml

# PHPunit Watcher
docker exec -ti apache bash -c 'cd /var/www/html/ds-projects/ds-utils/ && /var/www/html/ds-projects/ds-utils/vendor/bin/phpunit-watcher watch'