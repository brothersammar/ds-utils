<?php

include "vendor/autoload.php";

\DS\Utils\Config::setValue('disable_logs', false);
\DS\Utils\Config::setValue('assests_dir', __DIR__.'/temps');

$s = \DS\Utils\Logs::addLog('PHP_ERROR', 'sss', ['ss' => 'sss'], \DS\Utils\Logs::ERROR, [], 'SSS');
var_dump($s);


die();

define("ES_HOST", "host");
define("ES_PORT", "9200");
define("ES_USER", "elastic");
define("ES_PASS", "changeme");

$hosts = [
	'http://'.ES_USER.':'.ES_PASS.'@'.ES_HOST.':'.ES_PORT
];

/**
*
*/
class ElasticsearchConn
{
	static public $connection = false;

	static public function client()
	{
		if(empty($connection)){
			$hosts = [
				'http://'.ES_USER.':'.ES_PASS.'@'.ES_HOST.':'.ES_PORT
			];

			self::$connection = \Elasticsearch\ClientBuilder::create()
								->setHosts($hosts)
								->build();
		}

		return self::$connection;
	}
}

// // Create
// $params = [
//     'index' => 'teste',
//     'type' => 'teste',
//     'id' => '5',
//     'body' => ['testField' => 'abc']
// ];

// // $response = $client->index($params);
// // print_r($response);

// //

// $params = [
//     'index' => 'teste',
//     'type' => 'teste',
//     'id' => '2'
// ];
// $response = $client->get($params);
// echo '<pre>'; print_r($response); echo '</pre>';

// die();

class Model
{
	public $id = false;
	public $query;
	public $data = [];
	public $size;

	public function __construct()
	{
		$this->query = (object) [];
	}

	static public function find($id)
	{
		$self = new self;
		$self->id = $id;

		$params = [
		    'index' => 'teste',
		    'type' => 'teste',
		    'id' => $id
		];

		$find = ElasticsearchConn::client()
				->get($params);

		$self->updateValues($find['_source']);

		return $self;
	}

	public function getParams($id = false, $overhide = false){
		return [
		    	'index' => 'teste',
		    	'type' => 'teste',
		    	'client' => [
		    		'ignore' => 404
		    	]
		];
	}

	public function save($id = false, $overhide = false){
		$params = $this->getParams();

		if($this->id !== false){
			$params['id'] = $this->id;
			$params['body'] = $this->data;

			$find = ElasticsearchConn::client()
					->index($params);
		}else {
			if(!empty($id)){
				$params['id'] = $id;
				$find = ElasticsearchConn::client()
						->get($params);

				if(!$find['found']){
					$params['body'] = $this->data;
					$response = ElasticsearchConn::client()
						->index($params);
				}else{
					if($overhide){
						$params['body'] = $this->data;
						$response = ElasticsearchConn::client()
							->index($params);
					}else{
						throw new Exception("Document alread exist, change \$overhide to true for disable security mode", 1);
					}
				}
			}
		}
	}

	public function findAll(){
	}

	public function delete($id = false){
		$params = $this->getParams();

		if($this->id !== false){
			$params['id'] = $this->id;
			$params['body'] = $this->data;

			$find = ElasticsearchConn::client()
					->index($params);
		}else {
			if(!empty($id)){
				$params['id'] = $id;
				$find = ElasticsearchConn::client()
						->get($params);

				if(!$find['found']){
					$params['body'] = $this->data;
					$response = ElasticsearchConn::client()
						->index($params);
				}else{
					if($overhide){
						$params['body'] = $this->data;
						$response = ElasticsearchConn::client()
							->index($params);
					}else{
						throw new Exception("Document alread exist, change \$overhide to true for disable security mode", 1);
					}
				}
			}
		}
	}


	public function updateValues($values){
		foreach ($values as $key => $value) {
			$this->data[$key] = $value;
		}
	}

	public function setSize($size){
		// echo '<pre>'; print_r($size); echo '</pre>';
		return $size;
	}

	public function getQueries($queries){
		$queries = [];

		if(isset($this->query)) $queries['query'] = $this->query;
		if(isset($this->size)) $queries['size'] = $this->size;
		if(isset($this->from)) $queries['from'] = $this->from;
		if(isset($this->source)) $queries['_source'] = $this->source;

		return (object) [
			'query' => $this->query,
		];
	}

	public function __get($key) {

		if (array_key_exists($key, $this->data)) {
			return $this->data[$key];
		}

		if (method_exists($this, 'get'.ucfirst($key))) {
			return call_user_func(array($this, 'get'.ucfirst($key)), $value);
		}

		return $this->$key;
	}

	public function __set($key, $value) {

		$this->data[$key] = $value;

		if (method_exists($this, 'set'.ucfirst($key))) {
			return call_user_func(array($this, 'set'.ucfirst($key)), $value);
		}

		return $this;
	}

	public function __unset($key) {
		if(isset($this->data[$key])){
			unset($this->data[$key]);
		}
	}
}


// $elastic = Model::find(1);
$elastic = new Model;
$elastic->name = 'assd';
$elastic->age = '22';
$elastic->save(25);
// echo '<pre>'; print_r($elastic); echo '</pre>';
die();

// {
//     "query": {
//         "bool": {
//             "must": {
//                 "bool" : { "should": [
//                       { "match": { "title": "Elasticsearch" }},
//                       { "match": { "title": "Solr" }} ] }
//             },
//             "must": { "match": { "authors": "clinton gormely" }},
//             "must_not": { "match": {"authors": "radu gheorge" }}
//         }
//     }
// }

$elastic->query->bool->should[] = (object) [
						'match' => (object) ['title' => 'Elasticsearch']
					];
$elastic->query->bool->should[] = (object) [
						'match' => (object) ['title' => 'Solr']
					];
$elastic->query->bool->must[] = (object) [
						'match' => (object) ['authors' => 'clinton gormely']
					];
$elastic->query->bool->must_not[] = (object) [
						'match' => (object) ['authors' => 'radu gheorge']
					];

$elastic->size = 200;
// echo $elastic->size;

$elastic->name = 'Alberto Ammar';
$elastic->age = 200;
unset($elastic->age);
$elastic->book = [
	'ss' => 'sdsd'
];
$elastic->name = 'Alberto Ammar2';

echo json_encode($elastic->data);

// echo json_encode($elastic->query);
// echo json_encode($elastic->queries);

die();

include "vendor/autoload.php";
include "src/Helper.php";

// Config
\DS\Utils\Config::setValue('RDBMS_driver', 'mysql');
\DS\Utils\Config::setValue('RDBMS_host', 'host');
\DS\Utils\Config::setValue('RDBMS_dbname', 'db_test');
\DS\Utils\Config::setValue('RDBMS_user', 'root');
\DS\Utils\Config::setValue('RDBMS_pass', '123456aa');
\DS\Utils\Config::setValue('RDBMS_port', '3306');

$dbrds = new DS\CRUD\DBMongoDB('teste');

$filters_allowed = [
	'id' => [
		// 'column' => 'id', //Coluna que representa a Key - default: key
		'filter' => 'number', // Igual
		// 'number_gt': 10, // Maior que
		// 'number_lt': 10, // Menor que
		// 'number_gte': 10, // Maior que ou igual
		// 'number_lte': 10, // Menor que ou igual
		// 'int': 10, // Igual
		// 'int_gt': 10, // Maior que
		// 'int_lt': 10, // Menor que
		// 'int_gte': 10, // Maior que ou igual
		// 'int_lte': 10, // Menor que ou igual
		// 'float': 10.512, // Igual
		// 'float_gt': 10.512, // Maior que
		// 'float_lt': 10.512, // Menor que
		// 'float_gte': 10.512, // Maior que ou igual
		// 'float_lte': 10.512, // Menor que ou igual
		// 'string': 'asdasd', // Like
		// 'string_like': 'asdasd', // Menor que ou igual
		// 'array_int': 'asdasd', // Array de Ints
		// 'array_string': 'asdasd',  // Array de Strings
	],
	'id_gt' => [
		'column' => 'id', //Coluna que representa a Key - default: key
		'filter' => 'number_gt', // Maior que
		// 'number_lt': 10, // Menor que
		// 'number_gte': 10, // Maior que ou igual
		// 'number_lte': 10, // Menor que ou igual
		// 'int': 10, // Igual
		// 'int_gt': 10, // Maior que
		// 'int_lt': 10, // Menor que
		// 'int_gte': 10, // Maior que ou igual
		// 'int_lte': 10, // Menor que ou igual
		// 'float': 10.512, // Igual
		// 'float_gt': 10.512, // Maior que
		// 'float_lt': 10.512, // Menor que
		// 'float_gte': 10.512, // Maior que ou igual
		// 'float_lte': 10.512, // Menor que ou igual
		// 'string': 'asdasd', // Like
		// 'string_like': 'asdasd', // Menor que ou igual
		// 'array_int': 'asdasd', // Array de Ints
		// 'array_string': 'asdasd',  // Array de Strings
	],
	'id_lt' => [
		'column' => 'id', //Coluna que representa a Key - default: key
		'filter' => 'number_lt', // Maior que
	],
	'ids' => [
		'column' => 'id', //Coluna que representa a Key - default: key
		'filter' => 'array_int', // Maior que
	],
];

$filter = [
	'id' => 10,
	'id_lt' => 10,
	'ids' => [10,20,30]
];

$options = [
	'limit' => 1,
	'offset' => 0,
	'order' => ['id' => 'DESC'],
	'project' => [
		'id' => 1,
		'name' => 1
	]
];


$properties = DB::Mongo('properties')
				->find([
					'ref_fichas' => [ '$nin' => [ null, "", 0] ]
				], [
					'sort' => [
						'id' => 1
					]
				]);

die();
// $result = $dbrds->findAll($filter, $filters_allowed, $options);

// $dbrds->insertMany([
// 	['name' => 'alberto1', 'age' => 10, 'dead' => 10 ],
// 	['name' => 'alberto2', 'age' => 10, 'dead' => 10 ],
// 	['name' => 'alberto3', 'age' => 10, 'dead' => 10 ]
// ]);

$dbrds->deleteAll(
	['dead' => 15],
	$filters_allowed
);

// echo '<pre>'; print_r($dbrds->updateAll([
// 	'dead' => 15
// ], [], [])); echo '</pre>';

// echo '<pre>'; print_r($dbrds->findAll(['id' => 1], $filters_allowed, $options)); echo '</pre>';

die();
include "example/validator.php";

# Habilitar ou desabilitar logs
\DS\Utils\Config::setValue('disable_logs', false);

// define("DB_TYPE", "mysql");
// define("DB_HOST", "host");
// define("DB_PORT", "3306");
// define("DB_NAME", "esports");
// define("DB_USER", "root");
// define("DB_PASS", "123456aa");

// $mongoDB = new DS\Database\MongoDB;

die();

// class User extends DS\CRUD\Base
// {
// 	function __construct()
// 	{
// 		parent::__construct();

// 		$this->targetDB = 'mariadb2';

// 		// parent::$dbname = 'login';
// 		$this->filters_allowed = [
// 			'id_gt' => 'int_gt&id',
// 			'id_lt' => 'int_lt&id',
// 			'id' => 'int',
// 			'name' => 'array_string',
// 			'login' => 'string_like'
// 		];
// 	}

// 	public function search($filters)
// 	{
// 		parent::_search($filters);

// 		// $this->$filters
// 	}

// 	public function get($id)
// 	{
// 		$element = parent::_get($id);
// 		return $element;
// 	}

// 	public function create($data)
// 	{
// 		$element = parent::_create($data);
// 	}

// 	public function update($id, $data)
// 	{
// 		$element = parent::_updateOne($id, $data);
// 	}

// 	public function updateMany($id, $data)
// 	{
// 		$element = parent::_updateMany($data);
// 	}
// }


			// $db = DS\Database\DB::Table('login')
			// //
			// ->where('login', ':login')
			// ->beginBlock()
			// 	->where('primeiro', ':login')
			// 	->whereOR()
			// 	->beginBlock()
			// 		->beginBlock()
			// 			->where('name', ':login')
			// 			->where('last', ':login')
			// 		->endBlock()
			// 		->whereOR()
			// 		->where('middle', ':login')
			// 	->endBlock()
			// ->endBlock()
			// ->bindParam(':login', 'teste')
			// //
			// ->select()
			// ->getString();

			// echo $db;

$user = new User;

$user
	// ->search([
	// 	'filter' => [
	// 		'id & gt' => 1
	// 	],

	// 	'options' => [
	// 		'limit' => 2,
	// 		'offset' => 1,
	// 	],
	// ])
	->get(14)
	// ->update(20,
	// 	[
	// 		'name' => 'name',
	// 		'login' => 'name',
	// 		'password' => 'name'
	// 	]
	// )
	;

// die();

$data = file_get_contents('tests/YAMLSchema/testValidate.yaml');

$var = [
	'level1' => [
		'level1' => [
			'level3' => [
				'level3' => 'teste'
			],
		],
	]
];


$var = [
	'number' => 50,
	'string' => 'teste_string',
	'arrayString' => ['teste_string', 'teste_string1', 'teste_string2'],
	'arrayNumber' => [10, 15, 50],
	'array_arrayString' => [['asdasd','asdasd2','asdasd'],['asdasd','asdasd2','asdasd']],
	'array_arrayNumber' => [[10, 15, 50], [10, 15, 50]],
	'object' => [
		'number' => 50,
		'string' => 'teste_string',
		'arrayString' => ['teste_string', 'teste_string1', 'teste_string2'],
		'arrayNumber' => [10, 15, 50],
		'array_arrayString' => [['asdasd','asdasd2','asdasd'],['asdasd','asdasd2','asdasd']],
		'array_arrayNumber' => [[10, 15, 50], [10, 15, 50]],
	]
];

try {
	DS\YAMLSchema\Validator::validateD($data, $var);
} catch (DS\YAMLSchema\Exception $e) {
	echo '<pre>'; print_r($e->getBody()); echo '</pre>';
}

echo '<pre>'; print_r($var); echo '</pre>';

// echo '<pre>'; print_r($var); echo '</pre>';