<?php

use PHPUnit\Framework\TestCase;

class BaseMongoDBTest extends TestCase
{
	static $test;
	static $connection;


	protected function setUp(){
        self::clearCollection();
	}

	static public function clearCollection(){
    	self::$connection->getConnection()->tests->model->drop();
	}

	static public function setUpBeforeClass()
    	{
        self::$connection = new \DS\MongoORM\Database;

        self::$connection->addConnection([
            "database" => "tests",
            "host" => "mongodb",
            "port" => "27017",
            "user" => "admin",
            "password" => "QUcsn8nYG4q8HQewJqcKJcrg9VmnnrZh",
        ]);

        self::$test = new class extends \DS\MongoORM\Model {
    			public $collection = 'model';

    			protected $fillable = [
    				'name',
    				'password',
    			];
       	};

    }

	//Teste Query do DB
	public function testSearchInt()
	{
		$base = new class extends \DS\CRUD\BaseMongoDB{
            public $filters = [
                'name' => [
                    'filter' => 'int',
                    'column' => 'name'
                ]
            ];
        };

		$search = $base->__search(new self::$test, [
    			'name' => 2
    		],[],[]);

        $this->assertEquals('{"$and":[{"name":{"$eq":2}}]}', $search->toString());
	}

    public function testSearchIntGt()
    {
        $base = new class extends \DS\CRUD\BaseMongoDB{
            public $filters = [
                'name' => [
                    'filter' => 'int_gt',
                    'column' => 'name'
                ]
            ];
        };

        $search = $base->__search(new self::$test, [
                'name' => 2
            ],[],[]);

        $this->assertEquals('{"$and":[{"name":{"$gt":2}}]}', $search->toString());
    }

    public function testSearchIntGte()
    {
        $base = new class extends \DS\CRUD\BaseMongoDB{
            public $filters = [
                'name' => [
                    'filter' => 'int_gte',
                    'column' => 'name'
                ]
            ];
        };

        $search = $base->__search(new self::$test, [
                'name' => 2
            ],[],[]);

        $this->assertEquals('{"$and":[{"name":{"$gte":2}}]}', $search->toString());
    }

    public function testSearchIntlt()
    {
        $base = new class extends \DS\CRUD\BaseMongoDB{
            public $filters = [
                'name' => [
                    'filter' => 'int_lt',
                    'column' => 'name'
                ]
            ];
        };

        $search = $base->__search(new self::$test, [
                'name' => 2
            ],[],[]);

        $this->assertEquals('{"$and":[{"name":{"$lt":2}}]}', $search->toString());
    }

    public function testSearchIntlte()
    {
        $base = new class extends \DS\CRUD\BaseMongoDB{
            public $filters = [
                'name' => [
                    'filter' => 'int_lte',
                    'column' => 'name'
                ]
            ];
        };

        $search = $base->__search(new self::$test, [
                'name' => 2
            ],[],[]);

        $this->assertEquals('{"$and":[{"name":{"$lte":2}}]}', $search->toString());
    }

    public function testSearchLike()
    {
        $base = new class extends \DS\CRUD\BaseMongoDB{
            public $filters = [
                'name' => [
                    'filter' => 'string_like',
                    'column' => 'name'
                ]
            ];
        };

        $search = $base->__search(new self::$test, [
                'name' => 'Test AB'
            ],[],[]);

        $this->assertEquals('{"$and":[{"name":{"$regex":"test ab","$options":"i"}}]}', $search->toString());
    }

    public function testSearchArray()
    {
        $base = new class extends \DS\CRUD\BaseMongoDB{
            public $filters = [
                'name' => [
                    'filter' => 'array_string',
                    'column' => 'name'
                ]
            ];
        };

        $search = $base->__search(new self::$test, [
                'name' => ['T','S']
            ],[],[]);

        $this->assertEquals('{"$and":[{"name":{"$in":["T","S"]}}]}', $search->toString());

        //

        $base = new class extends \DS\CRUD\BaseMongoDB{
            public $filters = [
                'name' => [
                    'filter' => 'array_int',
                    'column' => 'name'
                ]
            ];
        };

        $search = $base->__search(new self::$test, [
                'name' => [1,2]
            ],[],[]);

        $this->assertEquals('{"$and":[{"name":{"$in":[1,2]}}]}', $search->toString());

        //

        $base = new class extends \DS\CRUD\BaseMongoDB{
            public $filters = [
                'name' => [
                    'filter' => 'array_string_like',
                    'column' => 'name'
                ]
            ];
        };

        $search = $base->__search(new self::$test, [
                'name' => [1,2]
            ],[],[]);

        $this->assertEquals('{"$and":[{"$or":[{"name":{"$regex":"1","$options":"i"}},{"name":{"$regex":"2","$options":"i"}}]}]}', $search->toString());
    }

    public function testSearchNotArray()
    {
        $base = new class extends \DS\CRUD\BaseMongoDB{
            public $filters = [
                'name' => [
                    'filter' => 'array_not',
                    'column' => 'name'
                ]
            ];
        };

        $search = $base->__search(new self::$test, [
                'name' => ['T','S']
            ],[],[]);

        $this->assertEquals('{"$and":[{"name":{"$nin":["T","S"]}}]}', $search->toString());
    }

    public function testSearchOptionsSort()
    {
        $base = new class extends \DS\CRUD\BaseMongoDB{
            public $sort = [
                'name',
                'date'
            ];
        };

        $search = $base->__search(new self::$test, [],[
                'sort' => 'name,-date',
                'limit' => 1000,
                'offset' => 5,
            ],[]);

        $this->assertEquals('{"sort":{"name":1,"date":-1},"skip":5,"limit":1000}', $search->toStringOptions());
    }

    public function testSearchBaseGet()
    {

        (new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
        (new self::$test(['name' => 'S', 'password' => 10]))->save(microtime());
        (new self::$test(['name' => 'AA', 'password' => 20]))->save(microtime());

        $base = new class extends \DS\CRUD\BaseMongoDB{
            public $sort = [
                'name',
                'date'
            ];
        };

        $resource = $base->__search(new self::$test, [],[],[]);
        $resource = $base->__getSearch($resource);

        $this->assertEquals(3, count($resource->toArray()));
    }

    public function testSearchBaseGet2()
    {
        (new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
        (new self::$test(['name' => 'S', 'password' => 10]))->save(microtime());
        (new self::$test(['name' => 'AA', 'password' => 20]))->save(microtime());

        $base = new class extends \DS\CRUD\BaseMongoDB{

            public $query = [
                'offset' => 0,
                'limit' => 2
            ];

            public $sort = [
                'name',
                'date'
            ];
        };

        $resource = $base->__search(new self::$test, [],[],[]);
        $resource = $base->__getSearch($resource);

        $this->assertEquals(2, count($resource->toArray()));
    }

    public function testSearchBaseGet3()
    {
        (new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
        (new self::$test(['name' => 'S', 'password' => 10]))->save(microtime());
        (new self::$test(['name' => 'AA', 'password' => 20]))->save(microtime());

        $base = new class extends \DS\CRUD\BaseMongoDB{

            public $sort_default = 'name';

            public $query = [
                'offset' => 0,
                'limit' => 2
            ];

            public $sort = [
                'name',
                'date'
            ];
        };

        $resource = $base->__search(new self::$test, [],[],[]);
        $resource = $base->__getSearch($resource);

        $this->assertEquals(2, count($resource->toArray()));
    }

}