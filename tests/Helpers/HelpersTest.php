<?php

use PHPUnit\Framework\TestCase;

class HelpersTest extends TestCase
{

    protected function setUp()
    {
    }

    //Teste de tipos de unidades
    public function testTrue()
    {
        //is_array_assoc
        $this->assertSame(true, \DS\Helper::is_array_assoc(['asso' => true]), 'Function Associative');
        $this->assertSame(false, \DS\Helper::is_array_assoc(['asso']), 'Function Associative');

        //is_array_assoc
        $this->assertSame(['object' => true], \DS\Helper::object_to_array((object) ['object' => true]), 'Object to Array');

        //is_array_assoc
        $this->assertEquals((object) ['object' => true], \DS\Helper::array_to_object(['object' => true]), 'Array to Object');

        //normalizeString
        $this->assertSame('ecansasd', \DS\Helper::normalizeString('éçansASD'), 'Normalize String');

        //RemoveAllKeysNoExist
        $this->assertSame(['a' => 'a'], \DS\Helper::RemoveAllKeysNoExist(['a' => 'a','b' => 'b'], ['a']), 'RemoveAllKeysNoExist');
        $this->assertEquals((object) ['a' => 'a'], \DS\Helper::RemoveAllKeysNoExist((object) ['a' => 'a','b' => 'b'], ['a']), 'RemoveAllKeysNoExist');

        //array_search_multi
        $this->assertEquals(['key' => 'value1'], \DS\Helper::array_search_multi([['key' => 'value1'], ['key' => 'value2']], 'key', 'value1'), 'array_search_multi');
        $this->assertEquals(NULL, \DS\Helper::array_search_multi([['key' => 'value1'], ['key' => 'value2']], 'key', 'value5'), 'array_search_multi');

        //Organize Array
        $this->assertEquals([
                'a' => 2,
                'b' => 2,
                'c' => 2,
                'x' => 2,
                ],
            \DS\Helper::organizeKey([
                'b' => 2,
                'x' => 2,
                'a' => 2,
                'c' => 2,
                ],
            ['a','b','c']));

        $this->assertEquals((object) [
                'a' => 2,
                'b' => 2,
                'c' => 2,
                'x' => 2,
                ],
            \DS\Helper::organizeKey( (object) [
                'b' => 2,
                'x' => 2,
                'a' => 2,
                'c' => 2,
                ],
            ['a','b','c']));

        //Converter numero em string de preçø
        $this->assertEquals('10,00', \DS\Helper::convertPrice(1000), 'array_search_multi');
        $this->assertEquals('100.000,00', \DS\Helper::convertPrice(10000000), 'array_search_multi');
        $this->assertEquals('1.000', \DS\Helper::convertPrice(1000, false), 'array_search_multi');

        //Masks CPF
        $this->assertEquals('111.111.111-11', \DS\Helper::maskCPF('11111111111'), 'mask_cpf');
        //Masks Phone
        $this->assertEquals('(44) 1111-1111', \DS\Helper::maskPhone('4411111111'), 'phone');
        $this->assertEquals('1111-1111', \DS\Helper::maskPhone('11111111'), 'phone');
        $this->assertEquals('(44) 91111-1111', \DS\Helper::maskPhone('44911111111'), 'phone');

        //Testa Hash
        // $hash = [];

        // for ($i=0; $i < 200000; $i++) {
        //     $hash[] = \DS\Helper::uniqueTokenMin();
        // }
        // $this->assertEquals(count($hash), count(array_unique($hash)));

    }
}