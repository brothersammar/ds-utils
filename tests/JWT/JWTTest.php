<?php

use PHPUnit\Framework\TestCase;

class JWTTest extends TestCase
{
    /**
     * Testar Logs
     */
    public function testJWT()
    {
	    	$payload = [
	    		'iss' => 'example.com.br',
	    		'token' => md5('token'),
	    		'account_id' => 100
	    	];

	    	/**
	    	 * Gerar JWT
	    	 */
	    	$jwt = \DS\Utils\JWT::encode($payload, 'keyexample');
		$this->assertEquals(
			'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJleGFtcGxlLmNvbS5iciIsInRva2VuIjoiOTRhMDhkYTFmZWNiYjZlOGI0Njk5MDUzOGM3YjUwYjIiLCJhY2NvdW50X2lkIjoxMDB9.DOObYrjnJysYlIauuJwn3iUFrNY-gzvFQNaG3JMazII',
			$jwt);

		/**
	    	 * Converter JWT
	    	 */
	    	$jwt = \DS\Utils\JWT::decode($jwt, 'keyexample');
	    	$jwt = (array) $jwt;
		$this->assertEquals($payload, $jwt);
    }
}