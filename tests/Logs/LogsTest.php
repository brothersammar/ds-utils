<?php

use PHPUnit\Framework\TestCase;

class LogsTest extends TestCase
{
    protected $TestData = null;

    static public function initTest()
    {
        \DS\Utils\Config::setValue('disable_logs', false);
        \DS\Utils\Config::setValue('assests_dir', ROOT.'/temps/tests');

        array_map('unlink', glob(\DS\Utils\Config::getValue('assests_dir').'/*'));
    }

    /**
     * Testar Logs
     */
    public function testLog()
    {

        self::initTest();

        $logs = [
            'DEBUG' => \DS\Utils\Logs::DEBUG,
            'INFO' => \DS\Utils\Logs::INFO,
            'NOTICE' => \DS\Utils\Logs::NOTICE,
            'WARNING' => \DS\Utils\Logs::WARNING,
            'ERROR' => \DS\Utils\Logs::ERROR,
            'CRITICAL' => \DS\Utils\Logs::CRITICAL,
            'ALERT' => \DS\Utils\Logs::ALERT,
            'EMERGENCY' => \DS\Utils\Logs::EMERGENCY,
        ];

        //Testa todos os tipos de logs
        foreach ($logs as $key => $value) {
            $message = md5($key);
            \DS\Utils\Logs::addLog($key, $message, [], $value);
            $filename = \DS\Utils\Config::getValue('assests_dir').'/logs-'.$key.'-'.date('Y-m-d').'.json';

            if(!file_exists($filename)) $this->fail('Error create log file');

            $log = file_get_contents($filename);
            $log = json_decode($log);

            $this->assertEquals($message, $log->message);
            $this->assertEquals([], $log->context);
            $this->assertEquals($value, $log->level);
            $this->assertEquals($key, $log->level_name);
            $this->assertEquals($key, $log->channel);
            $this->assertEquals([], $log->extra);
            $this->assertInternalType('string', $log->log_code);
            // $this->assertInternalType('string', $log->hash);
        }

        self::initTest();

        foreach ($logs as $key => $value) {
            $message = md5($key);
            $body = [
                'error' => md5($key)
            ];

            \DS\Utils\Config::setValue('extra_logs', $body);

            \DS\Utils\Logs::addLog($key, $message, $body, $value);
            $filename = \DS\Utils\Config::getValue('assests_dir').'/logs-'.$key.'-'.date('Y-m-d').'.json';

            if(!file_exists($filename)) $this->fail('Error create log file');

            $log = file_get_contents($filename);
            $log = json_decode($log);

            $this->assertEquals($message, $log->message);
            $this->assertEquals((object) $body, $log->context);
            $this->assertEquals($value, $log->level);
            $this->assertEquals($key, $log->level_name);
            $this->assertEquals($key, $log->channel);
            $this->assertEquals((object) $body, $log->extra);
            $this->assertInternalType('string', $log->log_code);
        }
    }
}