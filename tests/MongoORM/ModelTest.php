<?php

use PHPUnit\Framework\TestCase;

class ModelTest extends TestCase
{

    static $connection;
    static $test;

    /**
     * Setup
     */
    static public function clearCollection(){
        self::$connection->getConnection()->tests->model->drop();
    }

    protected function setUp(){
        self::clearCollection();
    }

    static public function setUpBeforeClass()
    {
        self::$connection = new \DS\MongoORM\Database;

        self::$connection->addConnection([
            "database" => "tests",
            "host" => "mongodb",
            "port" => "27017",
            "user" => "admin",
            "password" => "QUcsn8nYG4q8HQewJqcKJcrg9VmnnrZh",
        ]);

        self::clearCollection();

        self::$test = new class extends \DS\MongoORM\Model {

            public $collection = 'model';

            protected $fillable = [
                'name',
                'password',
            ];

            protected $hidden = [
                'hidden2',
                'hidden',
            ];

            /**
             * Casts
             */
            protected $casts = [
                'deleted_at' => 'time',
                'object_cast' => 'object',
                'array_cast' => 'array',
                'boolean_cast' => 'boolean',
            ];
        };

    }

    /**
     * @testdox Testar Cast Object
     */
    public function testCastObject()
    {
        $model = new self::$test;
        $model->object_cast = '{}';
        $this->assertEquals((object) [], $model->object_cast);

        $model = new self::$test;
        $model->object_cast = (object) ['name' => 'teste'];
        $this->assertEquals((object) ['name' => 'teste'], $model->object_cast);
    }

    /**
     * @testdox Testar Cast Array
     */
    public function testCastArray()
    {
        $model = new self::$test;
        $model->array_cast = '[]';
        $this->assertEquals([], $model->array_cast);

        $model = new self::$test;
        $model->array_cast = ['name' => 'teste'];
        $this->assertEquals(['name' => 'teste'], $model->array_cast);
    }

    /**
     * @testdox Testar Cast Array
     */
    public function testCastBoolean()
    {
        $model = new self::$test;
        $model->boolean_cast = 1;
        $this->assertSame(true, $model->boolean_cast);

        $model = new self::$test;
        $model->boolean_cast = 0;
        $this->assertSame(false, $model->boolean_cast);
    }

    /**
     * @testdox Testar Inicio de Cast
     */
    public function testCastInit()
    {
        $model = new class extends \DS\MongoORM\Model {

            public $collection = 'model';

            protected $attributes = [
                'object_cast' => '{}',
                'array_cast' => '[]',
                'boolean_cast' => 1,
            ];

            /**
             * Casts
             */
            protected $casts = [
                'deleted_at' => 'time',
                'object_cast' => 'object',
                'array_cast' => 'array',
                'boolean_cast' => 'boolean',
            ];
        };

        $this->assertSame(true, $model->boolean_cast);
        $this->assertEquals((object) [], $model->object_cast);
        $this->assertEquals([], $model->array_cast);

    }

    /**
     * Teste __get and __set
     */
    public function testGetSet()
    {
        $model = new self::$test;
        $model->name = 'name';
        $this->assertEquals('name', $model->name);
    }

    /**
     * Teste fillable
     */
    public function testFillable()
    {
        $model = new self::$test([
                'name' => 200,
                'password' => 200,
                'password2' => 200
            ]);

        $this->assertEquals(200, $model->name);
        $this->assertEquals(200, $model->password);
        $this->assertEquals(NULL, $model->password2);
    }

    /**
     * Teste Valores iniciais
     */
    public function testInitAttr()
    {
        $model = new class extends \DS\MongoORM\Model {
            public $collection = 'model';
            protected $attributes = [
                'enabled' => 1
            ];
        };

        $this->assertEquals(1, $model->enabled);
    }

    /**
     * Teste adiçnao de mais valores
     */
    public function testAddAttr()
    {
        $model = new class extends \DS\MongoORM\Model {
            public $collection = 'model';
            protected $fillable = [
                'name'
            ];
            protected $attributes = [];
        };

        $model->add(['name' => 'name-test']);

        $this->assertEquals('name-test', $model->name);
    }

    /**
     * Teste Hidden
     */
    public function testHidden()
    {
        $model = new self::$test([]);
        $model->hidden = 'hidden';
        $model->hidden2 = 'hidden';

        $this->assertEquals([], $model->toArray());
    }

    /**
     * Teste Save
     */
    public function testSave()
    {
        $model = new class extends \DS\MongoORM\Model {
            public $collection = 'model';
            public $incrementing = false;
        };

        $model->name = 'name-value';
        $model->name2 = 'name2-value';
        $model->name5 = 'name5-value';

        $model->save();

        $this->assertInternalType('string', $model->_id);
        $this->assertInternalType('string', $model->id);

        //Ver se está colocando o tempo certo
        $this->assertGreaterThanOrEqual(0, strtotime('now') - strtotime($model->updated_at));
        $this->assertLessThanOrEqual(5, strtotime('now') - strtotime($model->updated_at));
        $this->assertGreaterThanOrEqual(0, strtotime('now') - strtotime($model->created_at));
        $this->assertLessThanOrEqual(5, strtotime('now') - strtotime($model->created_at));
    }

    /**
     * Teste Save informando ID
     */
    public function testSaveSetID()
    {
        $model = new class extends \DS\MongoORM\Model {
            public $collection = 'model';
            public $incrementing = false;
        };

        $model->name = 'name-value';
        $model->name2 = 'name2-value';
        $model->name5 = 'name5-value';

        $model->save(102);

        $this->assertInternalType('string', $model->_id);
        $this->assertEquals(102, $model->id);
    }

    /**
     * Teste Save informando ID
     */
    public function testSaveDuplicateKey()
    {
        $this->expectException(\DS\MongoORM\Exception::class);
        $this->expectExceptionCode(\DS\MongoORM\Exception::DUPLICATE_KEY);

        (new self::$test())->createIndex('id');
        (new self::$test(['name' => 'A', 'password' => 200]))->save(1);
        (new self::$test(['name' => 200, 'password' => 200]))->save(1);
    }

    // /**
    //  * Teste Find
    //  */
    public function testFind()
    {
        (new self::$test(['name' => 'A', 'password' => 200]))->save(1);
        (new self::$test(['name' => 200, 'password' => 200]))->save(2);
        (new self::$test(['name' => 200, 'password' => 200]))->save(3);
        (new self::$test(['name' => 200, 'password' => 200]))->save(4);

        $model = self::$test::find(1);

        $this->assertEquals('A', $model->toArray()['name']);
        $this->assertEquals(200, $model->toArray()['password']);
    }

    /**
     * Teste FindMany
     */
    public function testFindMany()
    {
        (new self::$test(['name' => 'A', 'password' => 200]))->save(1);
        (new self::$test(['name' => 200, 'password' => 200]))->save(2);
        (new self::$test(['name' => 200, 'password' => 200]))->save(3);
        (new self::$test(['name' => 200, 'password' => 200]))->save(4);

        $model = self::$test::find([1,2]);

        $this->assertEquals(2, $model[0]->toArray()['id']);
        $this->assertEquals(1, $model[1]->toArray()['id']);
    }

    /**
     * Teste Search
     */
    public function testSearchGet()
    {
        (new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
        (new self::$test(['name' => 'S', 'password' => 10]))->save(microtime());
        (new self::$test(['name' => 'AA', 'password' => 20]))->save(microtime());

        $result = self::$test::equals('name','AA')
            ->get();

        $this->assertEquals(get_class($result->toArray()[0]), get_class(self::$test));
        $this->assertEquals(1, count($result->toArray()));
    }

    /**
     * Teste FindMany
     */
    public function testBoot()
    {

        $b = new class extends \DS\MongoORM\Model {

            public $collection = 'model';

            protected $fillable = [
                'name',
                'enabled',
                'password',
            ];

            protected $hidden = [
                'hidden2',
                'hidden',
            ];

            /**
             * Casts
             */
            protected $casts = [
                'deleted_at' => 'time',
                'object_cast' => 'object',
                'array_cast' => 'array',
                'boolean_cast' => 'boolean',
            ];

            protected static function boot()
            {
                parent::boot();

                static::addGlobalScope('enabled', function ($builder) {
                    $builder->eq('enabled', 1);
                });
            }
        };

        $model = (new $b(['name' => 'A', 'password' => 10, 'enabled' => 0]));
        $model->save(microtime());

        $model = $b::find($model->id);

        // addGlobalScope deve filtrar model
        $this->assertEquals(NULL, $model);

        $c = new class extends \DS\MongoORM\Model {
            public $collection = 'model';

            protected static function boot()
            {
                parent::boot();

                static::addGlobalScope('enabled', function ($builder) {
                    $builder->eq('enabled', 1);
                });
            }
        };

        $c::find(2);

        // Global Scope
        $this->assertEquals(2, count($c::$globalScope));
    }

    /**
     * Teste FindMany
     */
    public function testUpdate()
    {
        // (new self::$test(['name' => 'A', 'password' => 200]))->save(1);
        (new self::$test(['name' => ['ss' => 'sss'], 'password' => [200,200]]))->save(20);
        $model = self::$test::find(20);
        $model->name = 'name1-value';
        $model->name2 = 'name2-value';
        $model->object = ['ss' => 'sss'];
        $model->array = [200,200];
        $model->update();

        $model = self::$test::find(20);

        $this->assertEquals('name1-value', $model->name);
        $this->assertEquals('name2-value', $model->name2);
        $this->assertEquals((object) ['ss' => 'sss'], $model->object);
        $this->assertEquals([200,200], $model->array);

        //Ver se está colocando o tempo certo
        $this->assertGreaterThanOrEqual(0, strtotime('now') - strtotime($model->updated_at));
        $this->assertLessThanOrEqual(5, strtotime('now') - strtotime($model->updated_at));
    }

}

