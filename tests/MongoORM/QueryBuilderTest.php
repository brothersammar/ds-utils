<?php

use PHPUnit\Framework\TestCase;

class QueryBuilderTest extends TestCase
{
	static $test;
	static $connection;

	protected function setUp()
	{
        self::clearCollection();
	}

	static public function clearCollection()
	{
    		self::$connection->getConnection()->tests->model->drop();
	}

	static public function setUpBeforeClass()
    {
        self::$connection = new \DS\MongoORM\Database;

		self::$connection->addConnection([
			"database" => "tests",
			"host" => "mongodb",
			"port" => "27017",
			"user" => "admin",
			"password" => "QUcsn8nYG4q8HQewJqcKJcrg9VmnnrZh",
		]);

		self::$test = new class extends \DS\MongoORM\Model {
			public $collection = 'model';

			protected $fillable = [
				'name',
				'password',
			];
		};
    }

	//Teste QueryBuilder
	public function testSimpleQuery()
	{

		$query = '{"$and":[{"id":2},{"id":{"$regex":"bairro","$options":"i"}},{"id":{"$ne":[2,3]}},{"id":{"$eq":2}},{"id":{"$gt":2}},{"id":{"$gte":2}},{"id":{"$lt":2}},{"id":{"$lte":2}},{"id":{"$in":[2,3]}},{"id":{"$in":[2,3]}}]}';

		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->equals('id', 2);
		$search->like('id', 'bairro');
		$search->notEquals('id', [2, 3]);
		$search->eq('id', 2);
		$search->gt('id', 2);
		$search->gte('id', 2);
		$search->lt('id', 2);
		$search->lte('id', 2);
		$search->in('id', [2, 3]);
		$search->in('id', [2, 3]);

		$this->assertEquals($query, $search->toString());
	}

	//Teste QueryBuilder
	public function testSimpleQuery2()
	{

		$query = '{"$and":[{"id":2,"id1":{"$regex":"bairro","$options":"i"},"id2":{"$ne":[2,3]},"id3":{"$eq":2},"id4":{"$gt":2},"id5":{"$gte":2},"id6":{"$lt":2},"id7":{"$lte":2},"id8":{"$in":[2,3]}}]}';

		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->equals('id', 2);
		$search->like('id1', 'bairro', true);
		$search->notEquals('id2', [2, 3], true);
		$search->eq('id3', 2, true);
		$search->gt('id4', 2, true);
		$search->gte('id5', 2, true);
		$search->lt('id6', 2, true);
		$search->lte('id7', 2, true);
		$search->in('id8', [2, 3], true);

		$this->assertEquals($query, $search->toString());
	}

	public function testSimpleQuery3()
	{

		$query = '{"$and":[{"$or":[{"id":2,"id1":{"$regex":"bairro","$options":"i"},"id2":{"$ne":[2,3]},"id3":{"$eq":2},"id4":{"$gt":2},"id5":{"$gte":2},"id6":{"$lt":2},"id7":{"$lte":2},"id8":{"$in":[2,3]}}]}]}';

		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->or(function($q)
		{
    		$q->equals('id', 2);
    		$q->like('id1', 'bairro', true);
    		$q->notEquals('id2', [2, 3], true);
    		$q->eq('id3', 2, true);
    		$q->gt('id4', 2, true);
    		$q->gte('id5', 2, true);
    		$q->lt('id6', 2, true);
    		$q->lte('id7', 2, true);
    		$q->in('id8', [2, 3], true);
		});

		$this->assertEquals($query, $search->toString());
	}

	public function testSimpleQuery4()
	{
		$query = '{"$and":[{"id":10},{"$or":[{"id":2,"id2":2}]}]}';

		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->equals('id', 10);
		$search->or(function($q)
		{
    		$q->equals('id', 2);
    		$q->equals('id2', 2, true);
		});

		$this->assertEquals($query, $search->toString());
	}

	public function testSimpleQuery5()
	{
		$query = '{"$and":[{"id":10},{"$or":[{"id":2},{"$and":[{"id":{"$ne":2}},{"id":{"$ne":1}}]}]}]}';

		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->equals('id', 10);
		$search->or(function($q)
		{
    			$q->equals('id', 2);
    			$q->and(function($q)
			{
				$q->ne('id', 2);
				$q->ne('id', 1);
			});
		});

		$this->assertEquals($query, $search->toString());
	}

	/**
	 * @testdox Testar Busca
	 */
	public function testSearch()
	{
		(new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'S', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 20]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 30]))->save(microtime());
	    	(new self::$test(['name' => 'B', 'password' => 40]))->save(microtime());
	    	(new self::$test(['name' => 'BWEWV', 'password' => 50]))->save(microtime());
	    	(new self::$test(['name' => 'B B ', 'password' => 60]))->save(microtime());
	    	(new self::$test(['name' => 'AKSDAD', 'password' => 70]))->save(microtime());

    		//Contain
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->in('name', ['A', 'B']);
		$this->assertEquals(2, count($search->get()->toArray()));

		//Equal
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->equals('name', 'A');
		$this->assertEquals(1, count($search->get()->toArray()));

		//Like
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->like('name', 'A');
		$this->assertEquals(4, count($search->get()->toArray()));

		//Notequals
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->notEquals('name', 'A');
		$this->assertEquals(7, count($search->get()->toArray()));

		//EQ
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->eq('password', 10);
		$this->assertEquals(2, count($search->get()->toArray()));

		//gt
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->gt('password', 10);
		$this->assertEquals(6, count($search->get()->toArray()));

		//gte
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->gte('password', 10);
		$this->assertEquals(8, count($search->get()->toArray()));

		//lt
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->lt('password', 70);
		$this->assertEquals(7, count($search->get()->toArray()));

		//lte
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->lte('password', 70);
		$this->assertEquals(8, count($search->get()->toArray()));

		//Or
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->or(function($q)
    		{
    			$q->eq('password', 10);
    			$q->eq('password', 30);
    		});

		$this->assertEquals(3, count($search->get()->toArray()));

		//Complex
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');

		$search->or(function($q)
    		{
    			//
    			$q->eq('name', 'AA');
    			$q->eq('password', 20, true);
    			//
    			$q->eq('password', 60);
    		});

		$this->assertEquals(2, count($search->get()->toArray()));
	}

	/**
	 * @testdox Testar Atualização
	 */
	public function testUpdateMany()
	{
		(new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 20]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 30]))->save(microtime());

		$updateMany = DS\MongoORM\QueryBuilder::collection('tests','model');
		$updateMany->eq('name', 'A');
		$updateMany->updateMany([
			'$set' => [
				'name' => 'BB'
			],
			'$unset' => [
				'password' => 1
			],
		]);

		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$result = $search->get()->toArray();

		//Teste Resultado
		$this->assertEquals('BB', $result[0]->name);
		$this->assertEquals(false, isset($result[0]->password));
		$this->assertEquals('BB', $result[1]->name);
		$this->assertEquals(false, isset($result[1]->password));
		$this->assertEquals('AA', $result[2]->name);
		$this->assertEquals(true, isset($result[2]->password));
		$this->assertEquals('AA', $result[3]->name);
		$this->assertEquals(true, isset($result[3]->password));
	}

	/**
	 * @testdox Testar Atualização com todos os elementos
	 */
	public function testUpdateManyAll()
	{
		(new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 20]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 30]))->save(microtime());

		$updateMany = DS\MongoORM\QueryBuilder::collection('tests','model');
		$updateMany->updateMany([
			'$set' => [
				'name' => 'BB'
			],
			'$unset' => [
				'password' => 1
			],
		]);

		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$result = $search->get()->toArray();

		//Teste Resultado
		$this->assertEquals('BB', $result[0]->name);
		$this->assertEquals(false, isset($result[0]->password));
		$this->assertEquals('BB', $result[1]->name);
		$this->assertEquals(false, isset($result[1]->password));
		$this->assertEquals('BB', $result[2]->name);
		$this->assertEquals(false, isset($result[2]->password));
		$this->assertEquals('BB', $result[3]->name);
		$this->assertEquals(false, isset($result[3]->password));
	}

	/**
	 * @testdox Atualizar 1 documento
	 */
	public function testUpdateOne()
	{
		(new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 20]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 30]))->save(microtime());

		$updateMany = DS\MongoORM\QueryBuilder::collection('tests','model');
		$updateMany->eq('name', 'A');
		$updateMany->updateOne([
			'$set' => [
				'name' => 'BB'
			],
			'$unset' => [
				'password' => 1
			],
		]);

		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$result = $search->get()->toArray();

		//Teste Resultado
		$this->assertEquals('BB', $result[0]->name);
		$this->assertEquals(false, isset($result[0]->password));
		$this->assertEquals('AA', $result[1]->name);
		$this->assertEquals(true, isset($result[1]->password));
		$this->assertEquals('AA', $result[2]->name);
		$this->assertEquals(true, isset($result[2]->password));
		$this->assertEquals('AA', $result[3]->name);
		$this->assertEquals(true, isset($result[3]->password));
	}

	public function testFirst(){
		(new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'S', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 20]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 30]))->save(microtime());
	    	(new self::$test(['name' => 'B', 'password' => 40]))->save(microtime());
	    	(new self::$test(['name' => 'BWEWV', 'password' => 50]))->save(microtime());
	    	(new self::$test(['name' => 'B B ', 'password' => 60]))->save(microtime());
	    	(new self::$test(['name' => 'AKSDAD', 'password' => 70]))->save(microtime());

	    	//Contain
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->in('name', ['A', 'B']);

		$this->assertEquals('MongoDB\Model\BSONDocument', get_class($search->first()));
	}

    public function testCount(){
		(new self::$test(['name' => 'A', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'S', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'AA', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'B', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'BWEWV', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'B B ', 'password' => 10]))->save(microtime());
		(new self::$test(['name' => 'AKSDAD', 'password' => 10]))->save(microtime());

		//Contain
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->equals('password', 10);
		$search->limit(1);

		$this->assertEquals(1, count($search->get()->toArray()));
		$this->assertEquals(8, $search->get()->count());
    }

    	public function testRemove(){
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->equals('id', 2);
		$search->equals('id2', 2);
		$search->remove('id2');
		$this->assertEquals('{"$and":[{"id":2}]}', $search->toString());
    	}

	public function testRemoveOneValue(){
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->equals('id', 2);
		$search->remove('id');

		$this->assertEquals('{}', $search->toString());
	}

	public function testAggregate(){
		$search = DS\MongoORM\QueryBuilder::collection('tests','model');
		$search->equals('id', 2);
		$search->equals('id2', 10);
		$search->orderBy('id', 'desc');

		$search->setAggregate([
			[
				'$project' => [
					'ss.tes' => ['$ifNull' => ['$field', -1] ]
				]
			],
			[
				'$match' => $search->toArray()
			],
		]);

		$this->assertEquals('[{"$project":{"ss.tes":{"$ifNull":["$field",-1]}}},{"$match":{"$and":[{"id":2},{"id2":10}]}},{"$sort":{"id":-1}}]', $search->toString());
	}

}