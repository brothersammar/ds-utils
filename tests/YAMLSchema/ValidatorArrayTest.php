<?php

use PHPUnit\Framework\TestCase;


class ValidatorArrayTest extends TestCase
{
    protected $base = null;

    protected function setUp()
    {
        $this->base = [
            '$root' => [
                "type" => "object",
                "items" => [
                    'test' => [
                        'name' => 'Número',
                        'type' => 'array',
                        'items' => [
                                'type' => 'number'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     *
     * Testar Números
     *
     */
    public function testSchema(){
        /**
         * Teste com ArrayNumber
         */
            # Teste 1
            {
                $yaml = $this->base;
                $yaml['$root']["items"]['test']['items'] = [
                    'type' => 'number'
                ];
                $yaml = yaml_emit($yaml);

                $data = [
                        'test' => ['2010', '2010']
                    ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

                $this->assertSame([2010, 2010], $new_data['test']);
            }

            # Teste 2
            {
                $yaml = $this->base;
                $yaml['$root']["items"]['test']['items'] = [
                    'type' => 'number'
                ];
                $yaml = yaml_emit($yaml);

                $data = [
                        'test' => ['2010', '2010']
                    ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

                $this->assertSame([2010, 2010], $new_data['test']);
            }

        /**
         * Teste com ArrayNumber
         */
            # Teste 1
            {
                $yaml = $this->base;
                $yaml['$root']["items"]['test']['items'] = [
                    'type' => 'string'
                ];
                $yaml = yaml_emit($yaml);

                $data = [
                        'test' => ['2010', '2010']
                    ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

                $this->assertSame(['2010', '2010'], $new_data['test']);
            }

            # Teste 2
            {
                $yaml = $this->base;
                $yaml['$root']["items"]['test']['items'] = [
                    'type' => 'string'
                ];
                $yaml = yaml_emit($yaml);

                $data = [
                        'test' => ['Lorem ipsum dolor!', 'recusandae id, quod at, quos!']
                    ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

                $this->assertSame(['Lorem ipsum dolor!', 'recusandae id, quod at, quos!'], $new_data['test']);
            }

        /**
         * Teste com ArrayObject
         */

            # Teste 1
            {
                $yaml = $this->base;
                $yaml['$root']["items"]['test']['items'] = [
                    'type' => 'object',
                    'items' => [
                        'id' => [
                            'name' => 'ID da Finalidade',
                            'type' => 'number',
                        ],
                        'value' => [
                            'name' => 'Valor da Finalidade',
                            'type' => 'number',
                        ]
                    ]
                ];

                $yaml = yaml_emit($yaml);

                $data = [
                        'test' => [
                            [
                                "id" => 0,
                                "value" => 28500000
                            ]
                        ]
                    ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

                $this->assertSame(json_encode([ (object) ["id" => 0, "value" => 28500000 ]]), json_encode($new_data['test']));

        }

        /**
         * Teste ArrayDeep
         */
            # Teste 1
            {
                $yaml = $this->base;
                $yaml['$root']["items"]['test']['items'] = [
                    'type' => 'array'
                ];

                $yaml['$root']["items"]['test']['items']['items'] = [
                    'type' => 'string'
                ];

                $yaml = yaml_emit($yaml);

                $data = [
                        'test' => [['2010', '2010'],['2010', '2010']]
                    ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

                $this->assertSame([['2010', '2010'],['2010', '2010']], $new_data['test']);
            }

            # Teste 2
            {
                $yaml = $this->base;
                $yaml['$root']["items"]['test']['items'] = [
                    'type' => 'array'
                ];

                $yaml['$root']["items"]['test']['items']['items'] = [
                    'type' => 'string'
                ];

                $yaml = yaml_emit($yaml);

                $data = [
                        'test' => [['Lorem ipsum dolor!', 'recusandae id, quod at, quos!'],['Lorem ipsum dolor!', 'recusandae id, quod at, quos!']]
                    ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

                $this->assertSame([['Lorem ipsum dolor!', 'recusandae id, quod at, quos!'],['Lorem ipsum dolor!', 'recusandae id, quod at, quos!']], $new_data['test']);
            }

            # Teste 3
            {
                $yaml = $this->base;
                $yaml['$root']["items"]['test']['items'] = [
                    'type' => 'array'
                ];

                $yaml['$root']["items"]['test']['items']['items'] = [
                    'type' => 'array'
                ];

                $yaml['$root']["items"]['test']['items']['items']['items'] = [
                    'type' => 'string'
                ];

                $yaml = yaml_emit($yaml);

                $data = ['test' => [[['L', 'S']]] ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

                $this->assertSame([[['L', 'S']]], $new_data['test']);
            }

    }

    /**
     * Testar com string
     */
    public function testWithEmpty(){

//        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        // Criar YAML
        $yaml = yaml_emit($this->base);

        $data = [
                'test' => NULL
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);

        $this->assertSame([], $data);
    }

    /**
     * Testar com string
     */
    public function testWithEmptyWithDefault(){
        //Criar YAML
        $yaml = $this->base;
        $yaml['$root']["items"]['test']['default'] = '[]';
        $yaml = yaml_emit($yaml);

        $data = [
                'test' => ''
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);

        $this->assertSame([], $data['test']);
    }

    /**
     * Testar com number
     */
    public function testWithNumber(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        //Criar YAML
        $yaml = yaml_emit($this->base);

        $data = [
                'test' => 2323
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     * Testar com string
     */
    public function testWithString(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        //Criar YAML
        $yaml = yaml_emit($this->base);

        $data = [
                'test' => 'string'
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     * Testar com Range Máx
     */
    public function testWithMaxRange(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::RANGE_MAX);

        //Criar YAML
        $yaml = $this->base;
        $yaml['$root']["items"]['test']['validate'] = [
            'max-size' => 2
        ];
        $yaml = yaml_emit($yaml);

        $data = [
                'test' => ['1', '2', '3']
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     * Testar com Range Min
     */
    public function testWithMinRangeArray(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::RANGE_MIN);

        //Criar YAML
        $yaml = $this->base;
        $yaml['$root']["items"]['test']['validate'] = [
            'min-size' => 2
        ];
        $yaml = yaml_emit($yaml);

        $data = [
                'test' => ['1']
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }
}
