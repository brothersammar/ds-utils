<?php

use PHPUnit\Framework\TestCase;


class ValidatorBooleanTest extends TestCase
{
    protected $base = null;

    protected function setUp()
    {
        $this->base = [
            '$root' => [
                "type" => "object",
                "items" => [
                    'test' => [
                        'name' => 'Boolean',
                        'type' => 'bool'
                    ]
                ]
            ]
        ];
    }

    /**
     * Testar Números
     */
    public function testSchema(){

        //Criar YAML
        $yaml = yaml_emit($this->base);

        /**
         * Teste 1
         */
        $data = [
                'test' => true
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame(true, $new_data['test']);

        /**
         * Teste 2
         */
        $data = [
                'test' => '2010'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame(true, $new_data['test']);

        /**
         * Teste 3
         */
        $data = [
                'test' => '0'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame(false, $new_data['test']);

        /**
         * Teste 4
         */
        $data = [
                'test' => 0
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame(false, $new_data['test']);

        /**
         * Teste 5
         */
        $yaml = $this->base;
        $yaml['$root']["items"]['test']['tratament'] = ['number'];
        $yaml = yaml_emit($yaml);

        $data = [
                'test' => 0
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame(0, $new_data['test']);


        /**
         * Teste 5
         */
        $yaml = $this->base;
        $yaml['$root']["items"]['test']['tratament'] = ['number'];
        $yaml = yaml_emit($yaml);

        $data = [
                'test' => true
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame(1, $new_data['test']);

    }

    /**
     * Testar com string
     */
    public function testWithEmpty(){

//        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        //Criar YAML
        $yaml = yaml_emit($this->base);

        $data = [
            'test' => NULL
        ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);

        $this->assertSame([], $data);
    }


    /**
     * Testar com string
     */
    public function testWithEmptyWithDefault(){
        //Criar YAML
        $yaml = $this->base;
        $yaml['$root']["items"]['test']['default'] = 0;
        $yaml = yaml_emit($yaml);

        $data = [
                'test' => ''
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
        $this->assertSame(false, $data['test']);
    }


    /**
     * Testar com string
     */
    public function testWithString(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        //Criar YAML
        $yaml = yaml_emit($this->base);

        $data = [
                'test' => 'string'
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     * Testar com Array de Strings
     */
    public function testWithArrayString(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        //Criar YAML
        $yaml = yaml_emit($this->base);

        $data = [
                'test' => ['teste_string', 'teste_string1', 'teste_string2']
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     * Testar com Array de numeros
     */
    public function testWithArrayNumber(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        //Criar YAML
        $yaml = yaml_emit($this->base);

        $data = [
                'test' => [10, 15, 50]
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }
}
