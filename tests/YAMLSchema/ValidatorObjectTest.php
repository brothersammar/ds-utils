<?php

use PHPUnit\Framework\TestCase;


class ValidatorObjectTest extends TestCase
{
    protected $base = null;

    protected function setUp()
    {
        $this->base = [
            '$root' => [
                "type" => "object",
                "additionalProperties" => true,
                "items" => []
            ]
        ];
    }

    /**
     *
     * Testar Objetos
     *
     */
    public function testSchemaObject(){
        /**
         * Teste com additionalProperties
         */
            # Teste 1
            {
                $yaml = $this->base;
                $yaml['$root']["items"]['test'] = [
                    'type' => 'string'
                ];

                $yaml['$root']["items"]['object'] = [
                    'type' => 'object',
                    'items' => [
                            'name' => [
                                'type' => 'string'
                            ]
                        ]
                    ];

                $yaml = yaml_emit($yaml);

                $data = [
                        'test' => 'string',
                        'object' => (object) [
                            'name' => 'ss'
                        ],
                        'additionalProperties' => '2010'
                    ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateD($yaml, $new_data);
                $this->assertSame(json_encode($data), json_encode($new_data));
            }

            # Teste 2
            {
                $yaml = $this->base;
                $yaml['$root']["items"]['test'] = [
                    'type' => 'string'
                ];

                $yaml['$root']["items"]['object'] = [
                    'type' => 'object',
                    'items' => [
                            'name' => [
                                'type' => 'string'
                            ]
                        ]
                    ];

                $yaml = yaml_emit($yaml);

                $data = [
                        'test' => 'string',
                        'object' => [
                            'name' => 'ss',
                            'name2' => 'ss',
                        ],
                        'additionalProperties' => '2010'
                    ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateD($yaml, $new_data);
                $this->assertNotSame($data, $new_data);
            }

             # Teste 3
            {
                $yaml = $this->base;
                $yaml['$root']["items"]['test'] = [
                    'type' => 'string'
                ];

                $yaml['$root']["items"]['object'] = [
                    'type' => 'object',
                    'additionalProperties' => true,
                    'items' => [
                            'name' => [
                                'type' => 'string'
                            ]
                        ]
                    ];

                $yaml = yaml_emit($yaml);

                $data = [
                        'test' => 'string',
                        'object' => [
                            'name' => 'ss',
                            'name2' => 3423,
                        ],
                        'additionalProperties' => '2010'
                    ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateD($yaml, $new_data);
                $this->assertSame(json_encode($data), json_encode($new_data));
            }

            # Teste 2
            {
                $yaml = $this->base;
                $yaml['$root']["items"]["propertie_search"] = [
                    'type' => 'array',
                    'items' => [
                        'type' => 'object',
                        'items' => [
                            'location' => [
                                'type' => 'object',
                                'items' => [
                                    'estate' => [
                                        'type' => 'string',
                                    ],
                                    'neighborhood' => [
                                        'type' => 'string',
                                    ],
                                    'city' => [
                                        'type' => 'string',
                                    ],
                                ]
                            ]
                        ]
                    ]
                ];

                $yaml['$root']["items"]["composition"] = [
                    'type' => 'array',
                    'items' => [
                        'type' => 'object',
                        'items' => [
                            'id' => [
                                'type' => 'number'
                            ],
                            'value' => [
                                'type' => 'string'
                            ]
                        ]
                    ]
                ];

                $yaml = yaml_emit($yaml);

                $data = [
                        'propertie_search' => [
                            [
                                'location' => [
                                    'estate' => 'asdsad',
                                    'neighborhood' => 'asdsad',
                                    'city' => 'asdsad',
                                ]
                            ]
                        ],
                        "composition" => [
                            [
                                "id" => 1,
                                "value" => '2'
                            ],
                            [
                                "id" => 2,
                                "value" => '1'
                            ],
                            [
                                "id" => 3,
                                "value" => '1'
                            ],
                            [
                                "id" => 11,
                                "value" => '2'
                            ],
                            [
                                "id" => 21,
                                "value" => '1'
                            ],
                            [
                                "id" => 22,
                                "value" => '1'
                            ]
                        ]
                    ];

                $new_data = $data;
                \DS\YAMLSchema\Validator::validateF($yaml, $new_data, ['set_all_required' => true]);
                // \DS\YAMLSchema\Validator::validateF($yaml, $new_data, ['set_all_required' => true]);

               $this->assertSame(json_encode($data), json_encode($new_data));
            }


    }

    /**
     *
     * Testar Objetos
     *
     */
    public function testSchemaObjec2tDeep(){
        /**
         * Teste deep
         */
        {
            $yaml = $this->base;

            $yaml['$root']["items"]['array_object'] = [
                'type' => 'array',
                'items' => [
                    'type' => 'object',
                    'additionalProperties' => false,
                    'items' => [
                            'name' => [
                                'type' => 'string'
                            ]
                        ]
                    ]
                ];

            $yaml = yaml_emit($yaml);

            $data = [
                    'array_object' => [
                        (object) [
                            'name' => 'NAME',
                            'name2' => 3423,
                        ]
                    ]
                ];

            $expect = [
                    'array_object' => [
                        (object) [
                            'name' => 'NAME'
                        ]
                    ]
                ];

            $new_data = $data;
            \DS\YAMLSchema\Validator::validateD($yaml, $new_data);
            $this->assertEquals($expect, $new_data);
        }

        /**
         * Teste deep additionalProperties
         */
        {
            $yaml = $this->base;

            $yaml['$root']["items"]['array_object'] = [
                'type' => 'array',
                'items' => [
                    'type' => 'object',
                    'additionalProperties' => true,
                    'items' => [
                            'name' => [
                                'type' => 'string'
                            ]
                        ]
                    ]
                ];

            $yaml = yaml_emit($yaml);

            $data = [
                    'array_object' => [
                        (object) [
                            'name' => 'NAME',
                            'name2' => 3423,
                        ]
                    ]
                ];

            $expect = [
                    'array_object' => [
                        (object) [
                            'name' => 'NAME',
                            'name2' => 3423,
                        ]
                    ]
                ];

            $new_data = $data;
            \DS\YAMLSchema\Validator::validateD($yaml, $new_data);
            $this->assertEquals($expect, $new_data);
        }
    }

    /**
     * Testar com string
     */
    public function testWithEmptyObject(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        $yaml = $this->base;

        $yaml['$root']["items"]['object'] = [
                    'type' => 'object',
                    'items' => [
                            'name' => [
                                'type' => 'string'
                            ]
                        ]
                    ];

        $yaml = yaml_emit($yaml);

        $data = [
                'object' => ''
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     * Testar com string
     */
    public function testWithEmptyWithDefault(){
        $yaml = $this->base;

        $yaml['$root']["items"]['object'] = [
                    'type' => 'object',
                    'default' => '{}',
                    'items' => [
                            'name' => [
                                'type' => 'string'
                            ]
                        ]
                    ];

        $yaml = yaml_emit($yaml);

        $data = [
                'object' => ''
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);

        $this->assertSame(json_encode((object) []), json_encode($data['object']));
    }

    /**
     * Testar com number
     */
    public function testWithNumber(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        $yaml = $this->base;

        $yaml['$root']["items"]['object'] = [
                    'type' => 'object',
                    'items' => [
                            'name' => [
                                'type' => 'string'
                            ]
                        ]
                    ];

        $yaml = yaml_emit($yaml);

        $data = [
                'object' => 234234
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     * Testar com array
     */
    public function testWithArray(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        $yaml = $this->base;

        $yaml['$root']["items"]['object'] = [
                    'type' => 'object',
                    'items' => [
                            'name' => [
                                'type' => 'string'
                            ]
                        ]
                    ];

        $yaml = yaml_emit($yaml);

        $data = [
                'object' => [2323,23123]
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     * Testar com string
     */
    public function testWithString(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        //Criar YAML
        $yaml = $this->base;

        $yaml['$root']["items"]['object'] = [
                    'type' => 'object',
                    'items' => [
                            'name' => [
                                'type' => 'string'
                            ]
                        ]
                    ];

        $yaml = yaml_emit($yaml);

        $data = [
                'object' => 'asdsadasd'
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }
}