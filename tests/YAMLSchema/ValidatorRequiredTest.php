<?php

use PHPUnit\Framework\TestCase;


class ValidatorRequiredTest extends TestCase
{
    protected $base = null;

    protected function setUp()
    {
        $this->base = [
            '$root' => [
                "type" => "object",
                "items" => [
                    'test' => [
                        'name' => 'String',
                        'type' => 'string'
                    ]
                ]
            ]
        ];
    }

    /**
     * Testar obrigatórios
     */
    public function testRequired(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::ERROR_REQUIRED);

        $yaml = $this->base;

        $yaml['$root']["required"] = [
            'string2'
        ];

        $yaml['$root']["items"] = [
            'string1' => [
                'type' => 'string'
            ],
            'string2' => [
                'type' => 'string'
            ]
        ];
        $yaml = yaml_emit($yaml);

        $data = [
                'string1' => '2010'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);
    }


    public function testRequired2(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::ERROR_REQUIRED);

        $yaml = [
            '$root' => [
                'type' => 'object',
                'additionalProperties' => false,
                'required' => [
                    'status',
                    'finality',
                    'type',
                    'estate',
                    'city',
                    'neighborhood',
                    'images',
                ],
                'items' => [
                    # Código
                    'cod' => [
                        'name' => 'Código',
                        'type' => 'string',
                        'validate' => [
                            'max-size' => 100,
                        ]
                    ],

                    # Destaque Página inicial
                    'featured_home' => [
                        'name' => 'Destaque Página inicial',
                        'type' => 'number',
                        'validate' => [
                            'max-size' => 1,
                        ]
                    ],

                    # Destaque Página de bairros
                    'featured_neighborhood_page' => [
                        'name' => 'Destaque Página de bairros',
                        'type' => 'number',
                        'validate' => [
                            'max-size' => 1,
                        ]
                    ],

                    # Status => Disponivel, Vendido...
                    'status' => [
                        'name' => 'Status',
                        'type' => 'number',
                    ],
                ]
            ]
        ];

        $yaml = yaml_emit($yaml);
        $new_data = [];

        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);
    }

    /**
     * Testar obrigatórios ignorado
     */
    public function testRequiredIgnored(){

        $yaml = $this->base;

        $yaml['$root']["required"] = ['string2'];

        $yaml['$root']["items"] = [
            'string1' => [
                'type' => 'string'
            ],
            'string2' => [
                'type' => 'string'
            ]
        ];
        $yaml = yaml_emit($yaml);

        $data = [
                'string1' => '2010'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data, ['ignore_required' => true]);
        $this->assertEquals(true, true);
    }

    /**
     * Testar define todos como obrigatórios
     */
    public function testRequiredSetAll(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::ERROR_REQUIRED);

        $yaml = $this->base;

        $yaml['$root']["items"] = [
            'string1' => [
                'type' => 'string'
            ],
            'string2' => [
                'type' => 'string'
            ]
        ];
        $yaml = yaml_emit($yaml);

        $data = [];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data, ['set_all_required' => true]);
    }

    /**
     * Testar define todos como obrigatórios
     */
    public function testRequiredSetAllArray(){

        $yaml = $this->base;

        $yaml['$root']["items"] = [
            'array_object' => [
                'type' => 'array',
                'items' => [
                    'type' => 'object',
                    'additionalProperties' => true,
                    'items' => [
                            'name' => [
                                'type' => 'string'
                            ]
                        ]
                    ]
            ]
        ];
        $yaml = yaml_emit($yaml);

        $data = [
            'array_object' => [
                (object) [
                    'name' => 'asdasd'
                ]
            ]
        ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data, ['set_all_required' => true]);

        $this->assertEquals(true, true);
    }


}