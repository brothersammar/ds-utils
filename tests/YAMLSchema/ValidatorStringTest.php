<?php

use PHPUnit\Framework\TestCase;

class ValidatorStringTest extends TestCase
{
    protected $base = null;

    protected function setUp()
    {
        $this->base = [
            '$root' => [
                "type" => "object",
                "items" => [
                    'test' => [
                        'name' => 'String',
                        'type' => 'string'
                    ]
                ]
            ]
        ];
    }

    /**
     *
     * Testar Números
     *
     */
    public function testSchema(){

        //Criar YAML
        $yaml = yaml_emit($this->base);

        /**
         * Teste 1
         */
        $data = [
                'test' => 2010
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame('2010', $new_data['test']);

        /**
         * Teste 2
         */
        $data = [
                'test' => '2010'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame('2010', $new_data['test']);

        /**
         * Teste 3
         */
        $data = [
                'test' => 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame('Lorem ipsum dolor sit amet, consectetur adipisicing elit.', $new_data['test']);
    }

    /**
     * Testar com Array de Strings
     */
    public function testWithArrayString(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        //Criar YAML
        $yaml = yaml_emit($this->base);

        $data = [
                'test' => ['teste_string', 'teste_string1', 'teste_string2']
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     * Testar com Array de numeros
     */
    public function testWithArrayNumber(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::TYPE_ERROR);

        //Criar YAML
        $yaml = yaml_emit($this->base);

        $data = [
                'test' => [10, 15, 50]
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     * Testar com Range Máx
     */
    public function testWithMaxRange(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::RANGE_MAX);

        //Criar YAML
        $yaml = $this->base;
        $yaml['$root']["items"]['test']['validate'] = [
            'max-size' => 8
        ];
        $yaml = yaml_emit($yaml);

        $data = [
                'test' => '123456789'
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     * Testar com Range Min
     */
    public function testWithMinRange(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::RANGE_MIN);

        //Criar YAML
        $yaml = $this->base;
        $yaml['$root']["items"]['test']['validate'] = [
            'min-size' => 4
        ];
        $yaml = yaml_emit($yaml);

        $data = [
                'test' => '2'
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }
}