<?php

use PHPUnit\Framework\TestCase;

class ValidatorTramentTest extends TestCase
{
    protected $base = null;

    protected function setUp()
    {
        $this->base = [
            '$root' => [
                "type" => "object",
                "items" => [
                    'test' => [
                        'name' => 'String',
                        'type' => 'string'
                    ]
                ]
            ]
        ];
    }

    /**
     *
     * Testar Maiúsculo
     *
     */
    public function testUppercase(){

        $yaml = $this->base;
        $yaml['$root']["items"]['test']['tratament'] = [
            'uppercase'
        ];

        $yaml = yaml_emit($yaml);

        $data = [
                'test' => 'name'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame('NAME', $new_data['test']);
    }

    /**
     *
     * Testar Minúsculo
     *
     */
    public function testLowercase(){

        $yaml = $this->base;
        $yaml['$root']["items"]['test']['tratament'] = [
            'lowercase'
        ];

        $yaml = yaml_emit($yaml);

        $data = [
                'test' => 'NAME'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame('name', $new_data['test']);
    }

    /**
     *
     * Testar SHAE1
     *
     */
    public function testSHAE1(){

        $yaml = $this->base;
        $yaml['$root']["items"]['test']['tratament'] = [
            'sha1'
        ];

        $yaml = yaml_emit($yaml);

        $data = [
                'test' => 'name'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame(sha1('name'), $new_data['test']);
    }

    /**
     *
     * Testar MD5
     *
     */
    public function testMD5(){

        $yaml = $this->base;
        $yaml['$root']["items"]['test']['tratament'] = [
            'md5'
        ];

        $yaml = yaml_emit($yaml);

        $data = [
                'test' => 'name'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame(md5('name'), $new_data['test']);
    }

}