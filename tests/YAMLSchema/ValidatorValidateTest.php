<?php

use PHPUnit\Framework\TestCase;

class ValidatorValidateTest extends TestCase
{
    protected $base = null;

    protected function setUp()
    {
        $this->base = [
            '$root' => [
                "type" => "object",
                "items" => [
                    'test' => [
                        'name' => 'String',
                        'type' => 'string'
                    ]
                ]
            ]
        ];
    }

    /**
     *
     * Testar CPF
     *
     */
    public function testCPF(){

        $yaml = $this->base;
        $yaml['$root']["items"]['test']['validate'] = [
            'cpf' => true
        ];

        $yaml = yaml_emit($yaml);

        $data = [
                'test' => '11438374798'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame('11438374798', $new_data['test']);
    }

    /**
     *
     * Testar CPF Error
     *
     */
    public function testCPFError(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::INVALID_CPF);

        $yaml = $this->base;
        $yaml['$root']["items"]['test']['validate'] = [
            'cpf' => true
        ];
        $yaml = yaml_emit($yaml);

        $data = [
                'test' => '11111111122'
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     *
     * Testar Email
     *
     */
    public function testEmail(){

        $yaml = $this->base;
        $yaml['$root']["items"]['test']['validate'] = [
            'email' => true
        ];

        $yaml = yaml_emit($yaml);

        $data = [
                'test' => 'example@example.com'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame('example@example.com', $new_data['test']);
    }

    /**
     *
     * Testar Email Error
     *
     */
    public function testEmailError(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::INVALID_EMAIL);

        $yaml = $this->base;
        $yaml['$root']["items"]['test']['validate'] = [
            'email' => true
        ];
        $yaml = yaml_emit($yaml);

        $data = [
                'test' => 'example'
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }


    /**
     *
     * Testar CEP
     *
     */
    public function testCEP(){

        $yaml = $this->base;
        $yaml['$root']["items"]['test']['validate'] = [
            'cep' => true
        ];

        $yaml = yaml_emit($yaml);

        $data = [
                'test' => '12345678'
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame('12345678', $new_data['test']);
    }

    /**
     *
     * Testar CEP Error
     *
     */
    public function testCEPError(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::INVALID_CEP);

        $yaml = $this->base;
        $yaml['$root']["items"]['test']['validate'] = [
            'cep' => true
        ];
        $yaml = yaml_emit($yaml);

        $data = [
                'test' => '12345'
            ];

        \DS\YAMLSchema\Validator::validateD($yaml, $data);
    }

    /**
     *
     * Testar Same
     *
     */
    public function testSame(){

        $yaml = $this->base;
        $yaml['$root']["items"]['test_same'] = ['type' => 'string'];

        $yaml['$root']["items"]['test']['validate'] = [
            'same' => 'test_same'
        ];

        $yaml = yaml_emit($yaml);

        $data = [
                'test_same' => 'string',
                'test' => 'string',
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame('string', $new_data['test']);
    }

    /**
     *
     * Testar Same Error
     *
     */
    // public function testSameError(){

    //     $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::INVALID_SAME);

    //     $yaml = $this->base;
    //     $yaml['$root']["items"]['test_same'] = ['type' => 'string'];

    //     $yaml['$root']["items"]['test']['validate'] = [
    //         'same' => 'test_same'
    //     ];

    //     $yaml = yaml_emit($yaml);

    //     $data = [
    //             'test_same' => 'string',
    //             'test' => 'string2',
    //         ];

    //     $new_data = $data;
    //     \DS\YAMLSchema\Validator::validateD($yaml, $new_data);
    // }

    /**
     *
     * Testar Allowed
     *
     */
    public function testAllowed(){
        $yaml = $this->base;
        $yaml['$root']["items"]['test']['allowed'] = [
            'allowed',
            'allowed2',
        ];

        $yaml = yaml_emit($yaml);

        $data = [
                'test' => 'allowed2',
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);

        $this->assertSame('allowed2', $new_data['test']);
    }

    /**
     *
     * Testar Allowed
     *
     */
    public function testAllowedError(){

        $this->expectExceptionCode(DS\YAMLSchema\ErrorCode::NOT_ALLOWED_DATA);

        $yaml = $this->base;
        $yaml['$root']["items"]['test']['allowed'] = [
            'allowed',
            'allowed2',
        ];

        $yaml = yaml_emit($yaml);

        $data = [
                'test' => 'allowed3',
            ];

        $new_data = $data;
        \DS\YAMLSchema\Validator::validateD($yaml, $new_data);
    }


}